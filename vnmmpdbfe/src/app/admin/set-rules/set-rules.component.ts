import {Component, OnInit} from '@angular/core';
import {RecordGroupService} from '../../services/record-group.service';
import {RuleService} from '../../services/rule.service';
import {debounceTime, first} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TableService} from '../../services/table.service';
import {UtilService} from '../../services/util.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';
import {UserService} from '../../services/user.service';
import {RoleService} from '../../services/role.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-set-rules',
  templateUrl: './set-rules.component.html',
  styleUrls: ['./set-rules.component.scss']
})
export class SetRulesComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private recordGroupService: RecordGroupService,
    private ruleService: RuleService,
    private tableService: TableService,
    private utilService: UtilService,
    private spinnerOverlayService: SpinnerOverlayService,
    private userService: UserService,
    private roleService: RoleService
  ) { }

  listFormula = [
    'abs', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atanh', 'avg', 'avgFields', 'ceil', 'clearField', 'concat', 'cos', 'cosh', 'countFields', 'currentDate',
    'currentDateTime', 'dayOfMonth', 'decimals', 'exp', 'floor', 'formLink', 'getDays', 'getRecordName', 'getFieldLabel', 'getUserMail', 'hasRole', 'hour', 'if', 'lastFieldModifiedDate',
    'lastFieldModifiedName', 'lastFieldModifiedUserName', 'lastRecordModifiedDate', 'lastRecordModifiedName', 'lastRecordModifiedUserName', 'length', 'ln', 'log', 'match', 'max',
    'maxFields', 'median', 'medianFields', 'min', 'minFields', 'minute', 'mod', 'month', 'padLeft', 'padRight', 'pathValue', 'rand', 'relatedValue', 'replace', 'round', 'second',
    'sin', 'sinh', 'sqrt', 'str', 'subString', 'sum', 'sumFields', 'tan', 'tanh', 'toDatePattern', 'toDateString', 'toDatetimeString', 'toLower', 'toUpper', 'week', 'year', 'day',
  ];

  staticAddressForm: FormGroup;
  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  modalReference: any;

  selectedTriggerTable: any;
  selectedTargetTable: any = [];

  listTable: any;
  listFieldTrigger: any = [];
  listFieldTarget: any = [];
  listRules: any = [];
  listRuleGroups: any = [];
  selectedRuleGroup: any;

  listConditions: any = [];
  listActions: any = [];
  listRecipients: any = [];

  listRecordGroup: any = [];
  popUpRule: any = {};
  popUpId: number;
  page = 1;
  pageSize = 20;

  // send mail
  recipients: any = [];

  listUsersOrigin: any = [];
  listRolesOrigin: any = [];
  popUpAction: any = {};
  popUpActionId: number;

  listUsers: any = [];
  listRoles: any = [];
  staticAddress: any = {
    type: 'Static Address'
  };
  activeRecipient = 0;
  checkAll = false;

  selectedFormula: string;
  selectedField: string;

  // tslint:disable:forin prefer-for-of
  async ngOnInit() {
    this.staticAddressForm = this.formBuilder.group({
        email: ['', Validators.required],
      });

    this.spinnerOverlayService.show();
    this.setUpMessage();
    this.tableService.getAllTable()
      .pipe(first())
      .subscribe(
        data => {
          this.listTable = data;
        });

    this.recordGroupService.getAllRecordGroup()
      .pipe(first())
      .subscribe(
        data => {
          this.listRecordGroup = data;
        });

    this.userService.getAllUser()
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.listUsersOrigin = [];
          for (let i = 0; i < data.length; i++) {
            this.listUsersOrigin.push({
              email: data[i].email,
              name: data[i].name,
              type: 'User'
            });
          }
        });

    this.roleService.getAllRole()
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.listRoles = [];
          for (let i = 0; i < data.length; i++) {
            this.listRolesOrigin.push({
              name: data[i].name,
              type: 'Role'
            });
          }
        });

    this.ruleService.getAllRuleGroups()
      .pipe(first())
      .subscribe(
        data => {
          this.listRuleGroups = data;
          this.spinnerOverlayService.hide();
        });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  onSelectRuleGroup(event: any) {
    if (event) {
      this.spinnerOverlayService.show();
      const json: any = {};
      json.rule_group_id = this.selectedRuleGroup._id;
      this.ruleService.getRulesWithCondition(json)
        .pipe(first())
        .subscribe(
          data => {
            this.listRules = data;
            this.listRules.sort((a, b) => {
              return b.sort_id - a.sort_id;
            });
            this.listRules.reverse();
            this.spinnerOverlayService.hide();
          });
    } else {
      this.listRules = [];
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.listRules, event.previousIndex, event.currentIndex);
  }

  onSelectSortId(previousIndex, event) {
    moveItemInArray(this.listRules, previousIndex, event.target.value - 1);
  }

  onSaveSortId() {
    this.spinnerOverlayService.show();
    for (let i = 0; i < this.listRules.length; i++) {
      this.listRules[i].sort_id = i;
      const saveRule: any = {};
      saveRule._id = this.listRules[i]._id;
      saveRule.sort_id = i;
      this.ruleService.updateRule(saveRule)
        .pipe(first())
        .subscribe(
          data => {
            this.spinnerOverlayService.hide();
          });
    }
  }

  onSelectTriggerTable(event: any) {
    this.listConditions = [];
    if (event) {
      this.selectedTriggerTable = event;
      this.getTriggerFieldName();
    } else {
      this.selectedTriggerTable = null;
    }
  }

  getTriggerFieldName() {
    this.listFieldTrigger = [];
    for (const a in this.selectedTriggerTable.metadata) {
      this.listFieldTrigger.push([a, this.selectedTriggerTable.metadata[a]]);
    }

    if (this.listFieldTrigger.length === 0) {
      return;
    }

    this.listFieldTrigger.sort((a, b) => {
      return  b[1].sort_id - a[1].sort_id;
    });
    this.listFieldTrigger.reverse();

    for (let i = 0; i < this.listFieldTrigger.length; i++) {
      if (this.listFieldTrigger[i][1].type === 'File') {
        this.listFieldTrigger.splice(i, 1);
        i--;
      } else {
        this.listFieldTrigger[i] = {
          field_id: this.listFieldTrigger[i][0],
          field_name: this.listFieldTrigger[i][1].field_name
        };
      }
    }
  }

  onSelectTargetTable(event: any, id: any) {
    if (event) {
      this.selectedTargetTable[id] = event;
      this.getTargetFieldName(id);
    } else {
      this.selectedTargetTable[id] = null;
    }
  }

  getTargetFieldName(id: any) {
    this.listFieldTarget[id] = [];
    for (const a in this.selectedTargetTable[id].metadata) {
      this.listFieldTarget[id].push([a, this.selectedTargetTable[id].metadata[a]]);
    }

    if (this.listFieldTarget[id].length === 0) {
      return;
    }

    this.listFieldTarget[id].sort((a, b) => {
      return  b[1].sort_id - a[1].sort_id;
    });
    this.listFieldTarget[id].reverse();

    for (let i = 0; i < this.listFieldTarget[id].length; i++) {
      if (this.listFieldTarget[id][i][1].type === 'File') {
        this.listFieldTarget[id].splice(i, 1);
        i--;
      } else {
        this.listFieldTarget[id][i] = {
          field_id: this.listFieldTarget[id][i][0],
          field_name: this.listFieldTarget[id][i][1].field_name
        };
      }
    }
    this.listFieldTarget[id].unshift({
      field_name: 'record_group',
      field_id: 'record_group'
    });
  }

  onPopUp(content: any, modalBig: boolean) {
    if (modalBig) {
      this.modalReference = this.modalService.open(content, { windowClass : 'customModalClassBig'});
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    } else {
      this.modalReference = this.modalService.open(content);
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    }
  }

  onAddCondition() {
    const newCondition: any = {};
    this.listConditions.push(newCondition);
  }

  onDeleteCondition(id: any) {
    this.listConditions.splice(id, 1);
  }

  onAddAction() {
    const newAction: any = {};
    this.listActions.push(newAction);
  }

  onDeleteAction(id: any) {
    this.listActions.splice(id, 1);
  }

  onPopUpAddRule(content: any, modalBig: boolean) {
    this.popUpRule = {};
    this.selectedTriggerTable = null;
    this.selectedTargetTable = [];
    this.listActions = [];
    this.listConditions = [];
    this.onPopUp(content, modalBig);
  }

  onPopUpRule(content: any, id: any, modalBig: any) {
    this.popUpRule = this.utilService.deepCopy(this.listRules[id]);
    this.popUpId = id;

    this.selectedTriggerTable = null;
    for (let i = 0; i < this.listTable.length; i++) {
      if (this.popUpRule.trigger_table === this.listTable[i].name) {
        this.selectedTriggerTable = this.listTable[i];
        break;
      }
    }
    this.getTriggerFieldName();

    this.selectedTargetTable = [];
    if (this.popUpRule.action) {
      for (let i = 0; i < this.popUpRule.action.length; i++) {
        if (this.popUpRule.action[i].target_table) {
          for (let j = 0; j < this.listTable.length; j++) {
            if (this.popUpRule.action[i].target_table === this.listTable[j].name) {
              this.selectedTargetTable[i] = this.listTable[j];
              break;
            }
          }
          this.getTargetFieldName(i);
        }
      }
    }

    // trigger
    this.listConditions = [];
    if (this.popUpRule.condition) {
      for (let i = 0; i < this.popUpRule.condition.length; i++) {
        if (this.popUpRule.condition[i].or) {
          for (let j = 0; j < this.popUpRule.condition[i].or.length; j++) {
            const condition: any = {};
            condition.compare_type = this.popUpRule.condition[i].or[j].compare_type;
            condition.field_id = this.popUpRule.condition[i].or[j].field_id;
            condition.operator = this.popUpRule.condition[i].or[j].operator;
            condition.value = this.popUpRule.condition[i].or[j].value;
            if (j === 0) {
              condition.condition = 'AND';
            } else {
              condition.condition = 'OR';
            }
            this.listConditions.push(condition);
          }
        } else {
          const condition: any = {};
          condition.compare_type = this.popUpRule.condition[i].compare_type;
          condition.field_id = this.popUpRule.condition[i].field_id;
          condition.operator = this.popUpRule.condition[i].operator;
          condition.value = this.popUpRule.condition[i].value;
          condition.condition = 'AND';
          this.listConditions.push(condition);
        }
      }
    }

    // target
    this.listActions = [];
    if (this.popUpRule.action) {
      for (let i = 0; i < this.popUpRule.action.length; i++) {
        const action: any = {};
        action.action_type = this.popUpRule.action[i].action_type;
        if (action.action_type === 'Set Value' || action.action_type === 'Set Value by Function') {
          action.target_table = this.popUpRule.action[i].target_table;
          action.target_field = this.popUpRule.action[i].target_field;
          action.value = this.popUpRule.action[i].value;
        } else if (action.action_type === 'Send Mail') {
          action.recipients = this.popUpRule.action[i].recipients;
          action.subject = this.popUpRule.action[i].subject;
          action.content = this.popUpRule.action[i].content;
        }
        this.listActions.push(action);
      }
    }
    this.displayAllRecipient();
    this.onPopUp(content, modalBig);
  }

  onSaveRule() {
    // if (!this.onCheckInformation()) {
    //   return;
    // }
    this.spinnerOverlayService.show();
    // trigger convert
    const triggerField: any = [];
    const conditions: any = [];
    for (let i = 0; i < this.listConditions.length; i++) {
      if (this.listConditions[i].condition === 'AND' || i === 0) {
        let newCondition: any = {};
        if (i + 1 < this.listConditions.length) {
          if (this.listConditions[i + 1].condition === 'OR') {
            newCondition = {
              or: []
            };
          }
        }
        conditions.push(newCondition);
      }
      if (this.listConditions[i].condition || i === 0) {
        triggerField.push(this.listConditions[i].field_id);
        const condition: any = {};
        condition.compare_type = this.listConditions[i].compare_type;
        condition.value = this.listConditions[i].value;
        if (condition.compare_type === 'Keyword') {
          condition.field_id = this.listConditions[i].field_id;
        } else if (condition.compare_type !== 'Record Keyword') {
          condition.field_id = this.listConditions[i].field_id;
          condition.operator = this.listConditions[i].operator;
        }
        if (conditions[conditions.length - 1].or) {
          conditions[conditions.length - 1].or.push(condition);
        } else {
          conditions[conditions.length - 1] = condition;
        }
      }
    }
    this.popUpRule.condition = conditions;
    this.popUpRule.trigger_field = triggerField;

    // target convert
    const actions: any = [];
    for (let i = 0; i < this.listActions.length; i++) {
      const action: any = {};
      action.action_type = this.listActions[i].action_type;
      if (action.action_type === 'Set Value' || action.action_type === 'Set Value by Function') {
        action.target_table = this.listActions[i].target_table;
        action.target_field = this.listActions[i].target_field;
        action.value = this.listActions[i].value;
      } else if (action.action_type === 'Send Mail') {
        action.recipients = this.listActions[i].recipients;
        action.subject = this.listActions[i].subject;
        action.content = this.listActions[i].content;
      }
      actions.push(action);
    }
    this.popUpRule.action = actions;

    const saveRule = this.utilService.deepCopy(this.popUpRule);

    // convert field code
    // if (this.active === 1) {
    //   saveRule.subject_field = [];
    //   saveRule.content_field = [];
    //   if (saveRule.subject.includes('VALUE(')) {
    //     for (let i = 0; i < saveRule.subject.length; i++) {
    //       if (i + 6 >= saveRule.subject.length) {
    //         break;
    //       }
    //       if (saveRule.subject.substring(i, i + 6) === 'VALUE(') {
    //         for (let j = i + 6; j < saveRule.subject.length; j++) {
    //           if (saveRule.subject.charAt(j) === ')') {
    //             saveRule.subject_field.push(saveRule.subject.substring(i + 6, j));
    //             saveRule.subject = saveRule.subject.substring(0, i) + '%s' + saveRule.subject.substring(j + 1, saveRule.subject.length);
    //             break;
    //           }
    //         }
    //       }
    //     }
    //   }
    //   if (saveRule.content.includes('VALUE(')) {
    //     for (let i = 0; i < saveRule.content.length; i++) {
    //       if (i + 6 >= saveRule.content.length) {
    //         break;
    //       }
    //       if (saveRule.content.substring(i, i + 6) === 'VALUE(') {
    //         for (let j = i + 6; j < saveRule.content.length; j++) {
    //           if (saveRule.content.charAt(j) === ')') {
    //             saveRule.content_field.push(saveRule.content.substring(i + 6, j));
    //             saveRule.content = saveRule.content.substring(0, i) + '%s' + saveRule.content.substring(j + 1, saveRule.content.length);
    //             break;
    //           }
    //         }
    //       }
    //     }
    //   }
    //   saveRule.content = '<p>' + saveRule.content.replace(/\n\r?/g, '<br>') + '</p>';
    // }
    console.log(saveRule);
    if (this.popUpRule._id) {
      this.ruleService.updateRule(saveRule)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.spinnerOverlayService.hide();
            this.listRules[this.popUpId] = this.popUpRule;
            this.modalReference.close();
          });
    } else {
      saveRule.rule_group_id = this.selectedRuleGroup._id;
      this.ruleService.addNewRule(saveRule)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.popUpRule._id = data._id;
            this.popUpRule.rule_group_id = saveRule.rule_group_id;
            this.listRules.push(this.popUpRule);
            this.spinnerOverlayService.hide();
            this.modalReference.close();
          });
    }
  }

  onDeleteRule() {
    this.spinnerOverlayService.show();
    this.ruleService.deleteRule(this.popUpRule._id)
      .pipe(first())
      .subscribe(
        data => {
          this.listRules.splice(this.popUpId, 1);
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  getListRules() {
    return this.listRules
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  // send mail
  onPopUpRecipient(content: any, modalBig: boolean, id: any) {
    this.activeRecipient = 0;
    this.popUpActionId = id;

    if (!this.listActions[id].recipients) {
      this.listRecipients = [];
    } else {
      this.listRecipients = this.utilService.deepCopy(this.listActions[id].recipients);
    }

    this.listUsers  = this.utilService.deepCopy(this.listUsersOrigin).filter(user => {
      return this.listRecipients.filter(recipient => {
        return recipient.name === user.name;
      }).length === 0;
    });

    this.listRoles = this.utilService.deepCopy(this.listRolesOrigin).filter(user => {
      return this.listRecipients.filter(recipient => {
        return recipient.name === user.name;
      }).length === 0;
    });

    this.onPopUp(content, modalBig);
  }

  onPopUpAction(content: any, modalBig: boolean, id: any) {
    this.popUpAction = this.utilService.deepCopy(this.listActions[id]);
    this.popUpActionId = id;
    this.selectedFormula = null;
    this.selectedField = null;
    this.onPopUp(content, modalBig);
  }

  insertAtCaret(areaId, text) {
    const txtarea = document.getElementById(areaId) as HTMLInputElement;
    const scrollPos = txtarea.scrollTop;
    let strPos = 0;
    // @ts-ignore
    const br = ((txtarea.selectionStart || txtarea.selectionStart === 0) ? 'ff' : (document.selection ? 'ie' : false ) );
    if (br === 'ie') {
      txtarea.focus();
      // @ts-ignore
      const range = document.selection.createRange();
      range.moveStart ('character', -txtarea.value.length);
      strPos = range.text.length;
    } else if (br === 'ff') { strPos = txtarea.selectionStart; }

    const front = (txtarea.value).substring(0, strPos);
    const back = (txtarea.value).substring(strPos, txtarea.value.length);
    return front + text + back;
  }

  onAddFormula() {
    if (this.selectedFormula) {
      this.popUpAction.value = this.insertAtCaret('valueFunction', this.selectedFormula + '()');
    }
  }

  onAddFieldCode() {
    if (this.selectedField) {
      this.popUpAction.value = this.insertAtCaret('valueFunction', `value('` + this.selectedField + `')`);
    }
  }

  onAddFieldCodeMail() {
    if (this.selectedField) {
      this.popUpAction.content = this.insertAtCaret('contentMail', `value('` + this.selectedField + `')`);
    }
  }

  onSaveAction() {
    this.listActions[this.popUpActionId] = this.popUpAction;
    this.modalReference.close();
  }

  onCheckAll() {
    if (this.activeRecipient === 0) {
      for (let i = 0; i < this.listUsers.length; i++) {
        this.listUsers[i].checked = this.checkAll;
      }
    } else if (this.activeRecipient === 1) {
      for (let i = 0; i < this.listRoles.length; i++) {
        this.listRoles[i].checked = this.checkAll;
      }
    }
  }

  onAddRecipients() {
    if (this.activeRecipient === 0) {
      for (let i = 0; i < this.listUsers.length; i++) {
        if (this.listUsers[i].checked) {
          delete this.listUsers[i].checked;
          this.listRecipients.push(this.listUsers[i]);
          this.listUsers.splice(i, 1);
          i--;
        }
      }
    } else if (this.activeRecipient === 1) {
      for (let i = 0; i < this.listRoles.length; i++) {
        if (this.listRoles[i].checked) {
          delete this.listRoles[i].checked;
          this.listRecipients.push(this.listRoles[i]);
          this.listRoles.splice(i, 1);
          i--;
        }
      }
    } else if (this.activeRecipient === 2) {
    }
  }

  onDeleteRecipient(id: any) {
    if (this.listRecipients[id].type === 'User') {
      this.listUsers.push(this.listRecipients[id]);
    } else if (this.listRecipients[id].type === 'Role') {
      this.listRoles.push(this.listRecipients[id]);
    }
    this.listRecipients.splice(id, 1);
  }

  onAddStaticAddress() {
    if (this.staticAddressForm.invalid) {
      return;
    }
    this.staticAddress.name = this.staticAddressForm.get('email').value;
    this.listRecipients.push(this.staticAddress);
    this.staticAddress = {
      type: 'Static Address'
    };
  }

  onSaveRecipients() {
    this.displayRecipient();
    this.listActions[this.popUpActionId].recipients = this.listRecipients;
    this.modalReference.close();
  }

  displayRecipient() {
    this.recipients[this.popUpActionId] = '';
    for (let i = 0; i < this.listRecipients.length; i++) {
      if (i + 1 === this.listRecipients.length) {
        this.recipients[this.popUpActionId] += this.listRecipients[i].name;
      } else {
        this.recipients[this.popUpActionId] += this.listRecipients[i].name + ', ';
      }
    }
  }

  displayAllRecipient() {
    this.recipients = [];
    for (let i = 0; i < this.listActions.length; i++) {
      if (this.listActions[i].recipients) {
        this.recipients[i] = '';
        for (let j = 0; j < this.listActions[i].recipients.length; j++) {
          if (j + 1 === this.listActions[i].recipients.length) {
            this.recipients[i] += this.listActions[i].recipients[j].name;
          } else {
            this.recipients[i] += this.listActions[i].recipients[j].name + ', ';
          }
        }
      }
    }
  }

  getCurrentId(id: any) {
    return (id + (this.page - 1) * this.pageSize);
  }
}
