import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UtilService} from '../../services/util.service';
import {Subject} from 'rxjs';
import {debounceTime, first} from 'rxjs/operators';
import {RoleService} from '../../services/role.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';

@Component({
  selector: 'app-manage-roles',
  templateUrl: './manage-roles.component.html',
  styleUrls: ['./manage-roles.component.scss']
})
export class ManageRolesComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private roleService: RoleService,
    private utilService: UtilService,
    private spinnerOverlayService: SpinnerOverlayService,
  ) { }

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  modalReference: any;

  listRoles: any = [];
  listFilteredRole: any = [];

  newRole: any = {};
  popUpId: number;
  popUpRole: any;
  listSearch: any = {};
  page = 1;
  pageSize = 20;

  ngOnInit() {
    this.setUpMessage();
    this.setListSearch();
    this.roleService.getAllRole()
      .pipe(first())
      .subscribe(
        data => {
          this.listRoles = data;
          this.utilService.sortStringAsc(this.listRoles, 'name');
          this.listFilteredRole = this.listRoles;
        });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  setListSearch() {
    this.listSearch.name = '';
    this.listSearch.type = '';
  }

  performFilter() {
    this.listFilteredRole = this.listRoles.filter((field: any) =>
      field.name.toLocaleLowerCase().indexOf(this.listSearch.name.toLocaleLowerCase()) !== -1
    );
  }

  onPopUp(content: any) {
    this.modalReference = this.modalService.open(content);
    this.modalReference.result.then((result) => {
    }, (reason) => {
    });
  }

  onPopUpDataRecordGroup(content: any, id: any) {
    this.popUpRole = this.utilService.deepCopy(this.listFilteredRole[id]);
    this.popUpId = id;
    this.onPopUp(content);
  }

  onSaveAddRole() {
    this.newRole = this.utilService.trimJson(this.newRole);
    if (!this.checkInformation(this.newRole, false)) {
      return;
    }
    this.spinnerOverlayService.show();
    this.roleService.addNewRole(this.newRole)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.newRole._id = data._id;
          this.listRoles.push(this.newRole);
          this.performFilter();
          this.newRole = {};
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onSaveEdit() {
    this.spinnerOverlayService.show();
    this.popUpRole = this.utilService.trimJson(this.popUpRole);
    if (!this.checkInformation(this.popUpRole, true)) {
      return;
    }
    this.roleService.updateRole(this.popUpRole)
      .pipe(first())
      .subscribe(
        data => {
          const index = this.listRoles.indexOf(this.listFilteredRole[this.popUpId]);
          this.listRoles[index] = this.utilService.deepCopy(this.popUpRole);
          this.performFilter();
          this.spinnerOverlayService.hide();
        });
  }

  onDelete(id: any) {
    this.spinnerOverlayService.show();
    this.roleService.deleteRole(this.listFilteredRole[id]._id)
      .pipe(first())
      .subscribe(
        data => {
          this.listRoles = this.listRoles.filter((field: any) =>
            field._id !== this.listFilteredRole[id]._id
          );
          this.performFilter();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  checkInformation(recordGroup: any, isEdit: boolean) {
    if (!this.utilService.checkValue(recordGroup.name)) {
      this.message.next('Project Name must not be blanked');
      return false;
    }
    return true;
  }

  getListRoles() {
    return this.listFilteredRole
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  getCurrentId(id: any) {
    return (id + (this.page - 1) * this.pageSize);
  }
}
