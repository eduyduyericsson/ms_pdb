import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProjectService} from '../../services/project.service';
import {UtilService} from '../../services/util.service';
import {Subject} from 'rxjs';
import {debounceTime, first} from 'rxjs/operators';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {TableService} from '../../services/table.service';
import {FormService} from '../../services/form.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';

@Component({
  selector: 'app-manage-forms',
  templateUrl: './manage-forms.component.html',
  styleUrls: ['./manage-forms.component.scss']
})
export class ManageFormsComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private projectService: ProjectService,
    private utilService: UtilService,
    private tableService: TableService,
    private formService: FormService,
    private spinnerOverlayService: SpinnerOverlayService,
  ) { }

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  active = 0;
  activeForm = 0;
  activeEditSection = 0;
  activeMasterDetail = 0;
  modalReference: any;
  newForm: any = {};
  newSection: any = {
    metadata: {}
  };
  listProject: any;
  listTable: any;
  listDetailTable: any;
  selectedProject: any;
  selectedForm: any;

  tableIndex: any;
  masterTableIndex: any;
  detailTableIndex: any;

  listForm: any = [];
  selectedIndex: number;
  listSelect: any = [];
  numCol: any;
  numRow: any;

  listField: any = [];
  listFieldSelect: any = [];
  listFieldSection: any = [];
  listFieldEditSection: any = [];

  listFilteredField: any = [];
  listFilteredFieldSection: any = [];
  listFilteredFieldSelect: any = [];

  listSearch: any = {};
  listSearchEditField: any = {};
  listSearchSelect: any = {};

  checkAll = false;
  dropDownTitle = '';

  checkAllViewOnly = false;
  checkAllMandatory = false;

  // tslint:disable:forin prefer-for-of
    ngOnInit() {
    this.spinnerOverlayService.show();
    this.setUpMessage();
    this.projectService.getAllProject()
      .pipe(first())
      .subscribe(
        data => {
          this.listProject = data;
          this.spinnerOverlayService.hide();
        });

    this.tableService.getAllTable()
      .pipe(first())
      .subscribe(
        data => {
          this.listTable = data;
        });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  setListSearch() {
    this.listSearch.name = '';
    this.listSearch.type = '';
  }

  setListSearchEditField() {
    this.listSearchEditField.name = '';
    this.listSearchEditField.type = '';

    this.listSearchSelect.name = '';
    this.listSearchSelect.type = '';
  }

  performFilter() {
    this.listFilteredField = this.listFieldSection[this.active].filter((field: any) =>
      field.field_name.toLocaleLowerCase().indexOf(this.listSearch.name.toLocaleLowerCase()) !== -1 &&
      field.type.toLocaleLowerCase().indexOf(this.listSearch.type.toLocaleLowerCase()) !== -1
    );
  }

  performFilterFieldSection() {
    this.listFilteredFieldSection = this.listFieldEditSection[this.activeEditSection].filter((field: any) =>
      field.field_name.toLocaleLowerCase().indexOf(this.listSearchEditField.name.toLocaleLowerCase()) !== -1 &&
      field.type.toLocaleLowerCase().indexOf(this.listSearchEditField.type.toLocaleLowerCase()) !== -1
    );
    this.performFilterFieldSelect();
  }

  performFilterFieldSelect() {
    this.listFilteredFieldSelect = this.listFieldSelect.filter((field: any) =>
      field.field_name.toLocaleLowerCase().indexOf(this.listSearchSelect.name.toLocaleLowerCase()) !== -1 &&
      field.type.toLocaleLowerCase().indexOf(this.listSearchSelect.type.toLocaleLowerCase()) !== -1
    );
  }

  checkInformation(form: any) {
    if (!this.utilService.checkValue(form.name)) {
      this.message.next('Form Name must not be blanked');
      return false;
    }
    for (let i = 0; i < this.listForm.length; i++) {
      if (this.listForm[i] === form.name) {
        this.message.next('Form Name existed');
        return false;
      }
    }

    if (!this.utilService.checkValue(form.form_type)) {
      this.message.next('Form Type must not be blanked');
      return false;
    } else {
      if (form.form_type === 'Multi Record View') {
        if (!this.utilService.checkValue(form.table_id)) {
          this.message.next('Table must not be blanked');
          return false;
        }
      } else if (form.form_type === 'Master Detail View') {
        if (!this.utilService.checkValue(form.master_table_id)) {
          this.message.next('Master Table must not be blanked');
          return false;
        }
        if (!this.utilService.checkValue(form.detail_table_id)) {
          this.message.next('Detail Table must not be blanked');
          return false;
        }
      }
    }
    return true;
  }

  drop(event: CdkDragDrop<string[]>) {
    if (this.listSearchEditField.name !== '') {
      return;
    }
    moveItemInArray(this.listFieldEditSection[this.activeEditSection], event.previousIndex, event.currentIndex);
    this.performFilterFieldSection();
  }

  onSelectSortId(previousIndex: any, event: any) {
    moveItemInArray(this.listFieldEditSection[this.activeEditSection], previousIndex, event.target.value - 1);
    this.performFilterFieldSection();
  }

  onSelectSectionOrder(previousIndex: any, event: any) {
    moveItemInArray(this.selectedForm.sections, previousIndex, event.target.value - 1);
    this.activeEditSection = event.target.value - 1;
  }

  onSelectSectionOrderMaster(previousIndex: any, event: any) {
    moveItemInArray(this.selectedForm.master_table_sections, previousIndex, event.target.value - 1);
    this.activeEditSection = event.target.value - 1;
  }

  onSelectSectionOrderDetail(previousIndex: any, event: any) {
    moveItemInArray(this.selectedForm.detail_table_sections, previousIndex, event.target.value - 1);
    this.activeEditSection = event.target.value - 1;
  }

  onSelectOrderForm(previousIndex: any, event: any) {
    moveItemInArray(this.listForm, previousIndex, event.target.value - 1);
    this.activeForm = event.target.value - 1;
  }

  onSaveOrderForm() {
    this.spinnerOverlayService.show();
    for (let i = 0; i < this.listForm.length; i++) {
      const saveForm: any = {};
      saveForm._id = this.listForm[i]._id;
      saveForm.sort_id = i;
      this.formService.updateForm(saveForm)
        .pipe(first())
        .subscribe(
          async data => {
            this.listForm[i].sort_id = i;
            this.spinnerOverlayService.hide();
          });
    }
  }

  trackBy(index: any, item: any) {
    return index;
  }

  onPopUp(content: any, modalBig: boolean) {
    if (modalBig) {
      this.modalReference = this.modalService.open(content, { windowClass : 'customModalClassBig'});
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    } else {
      this.modalReference = this.modalService.open(content);
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    }
  }

  onPopUpEditSections(content: any, modalBig: boolean) {
    this.setListSearchEditField();
    if (this.selectedForm.form_type === 'Multi Record View') {
      this.onChangeTableMultiRecord();
    } else if (this.selectedForm.form_type === 'Master Detail View') {
      this.onChangeTableMasterDetail();
    }
    this.listFieldEditSection = this.utilService.deepCopy(this.listFieldSection);
    this.activeEditSection = 0;
    this.performFilterFieldSection();
    this.onPopUp(content, modalBig);
  }

  onPopUpDropdownTable(content: any, id: any, isListSelect) {
    this.selectedIndex = id;
    this.listSelect = [];
    if (isListSelect) {
      if (this.utilService.checkValue(this.listFilteredFieldSelect[id].values)) {
        this.listSelect = this.listFilteredFieldSelect[id].values;
      } else {
        this.listSelect = [];
      }
      this.dropDownTitle = this.listFilteredFieldSelect[id].field_name;
    } else {
      if (this.utilService.checkValue(this.listFilteredFieldSection[id].values)) {
        this.listSelect = this.listFilteredFieldSection[id].values;
      } else {
        this.listSelect = [];
      }
      this.dropDownTitle = this.listFilteredFieldSection[id].field_name;
    }
    this.onPopUp(content, false);
  }

  async onSelectProject(event: any) {
    if (event) {
      this.spinnerOverlayService.show();
      this.selectedProject = event;
      await this.updateListFormName();
      this.onChangeForm();
      this.spinnerOverlayService.hide();
    }
  }

  onAddForm() {
    if (!this.checkInformation(this.newForm)) {
      return;
    }
    this.spinnerOverlayService.show();
    const newForm: any = {};
    newForm.name = this.newForm.name;
    newForm.form_type = this.newForm.form_type;
    newForm.project_id = this.selectedProject._id;
    if (this.newForm.form_type === 'Multi Record View') {
      newForm.table_id = this.newForm.table_id;
      newForm.metadata = {};
      newForm.sections = [
        {
          name: 'none_section',
          metadata: {}
        }
      ];
    } else if (this.newForm.form_type === 'Master Detail View') {
      newForm.master_table_id = this.newForm.master_table_id;
      newForm.detail_table_id = this.newForm.detail_table_id;
      newForm.master_table_metadata = {};
      newForm.detail_table_metadata = {};
      newForm.master_table_sections = [
        {
          name: 'none_section',
          metadata: {}
        }
      ];
      newForm.detail_table_sections = [
        {
          name: 'none_section',
          metadata: {}
        }
      ];
    }
    this.formService.addNewForm(newForm)
      .pipe(first())
      .subscribe(
        (data: any) => {
          newForm._id = data._id;
          newForm.sort_id = this.listForm.length;
          this.listForm.push(newForm);
          if (this.listForm.length === 1) {
            this.onChangeForm();
          }
          this.newForm = {};
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onSaveForm() {
    if (!this.checkInformation(this.selectedForm)) {
      return;
    }
    this.spinnerOverlayService.show();
    this.formService.updateForm(this.selectedForm)
      .pipe(first())
      .subscribe(
        data => {
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  updateListFormName(): Promise<void> {
    return new Promise<void>(resolve => {
      this.selectedForm = null;
      const json: any = {};
      json.project_id = this.selectedProject._id;
      this.formService.getFormWithCondition(json)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.listForm = data;

            this.listForm.sort((a, b) => {
              return b.sort_id - a.sort_id;
            });
            this.listForm.reverse();

            resolve();
          });
    });
  }

  onChangeForm() {
    this.setListSearch();
    this.selectedForm = this.listForm[this.activeForm];

    this.tableIndex = null;
    this.masterTableIndex = null;
    this.detailTableIndex = null;
    if (!this.selectedForm) {
      return;
    }
    if (this.selectedForm.form_type === 'Multi Record View') {
      for (let i = 0; i < this.listTable.length; i++) {
        if (this.selectedForm.table_id === this.listTable[i]._id) {
          this.tableIndex = i;
          break;
        }
      }
      this.onChangeTableMultiRecord();
    } else if (this.selectedForm.form_type === 'Master Detail View') {
      for (let i = 0; i < this.listTable.length; i++) {
        if (this.selectedForm.master_table_id === this.listTable[i]._id) {
          this.masterTableIndex = i;
          continue;
        }
        if (this.selectedForm.detail_table_id === this.listTable[i]._id) {
          this.detailTableIndex = i;
        }
        if (this.masterTableIndex && this.detailTableIndex) {
          break;
        }
      }
      this.onChangeTableMasterDetail();
    }
  }

  onDeleteForm() {
    this.spinnerOverlayService.show();
    this.formService.deleteForm(this.selectedForm._id)
      .pipe(first())
      .subscribe(
        data => {
          this.listForm = this.listForm.filter((form: any) =>
            form._id !== this.selectedForm._id
          );
          this.activeForm = 0;
          this.onChangeForm();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onAddSection() {
    if (!this.newSection.name) {
      return;
    }
    if (this.selectedForm.form_type === 'Multi Record View') {
      this.selectedForm.sections.push(this.newSection);
    } else if (this.selectedForm.form_type === 'Master Detail View') {
      if (this.activeMasterDetail === 0) {
        this.selectedForm.master_table_sections.push(this.newSection);
      } else if (this.activeMasterDetail === 1) {
        this.selectedForm.detail_table_sections.push(this.newSection);
      }
    }
    this.listFieldEditSection.push([]);
    this.newSection = {
      metadata: {}
    };
    this.modalReference.close();
  }

  onAddBlank() {
    this.listFieldEditSection[this.activeEditSection].unshift({
      name: 'Blank',
      type: 'Blank',
    });
    this.performFilterFieldSection();
  }

  onDeleteField(id: any) {
    if (this.listFilteredFieldSection[id].type !== 'Blank') {
      this.listFilteredFieldSection[id].view_only = false;
      this.listFilteredFieldSection[id].mandatory = false;
      this.listFieldSelect.push(this.listFilteredFieldSection[id]);
    }
    for (let i = 0; i < this.listFieldEditSection[this.activeEditSection].length; i++) {
      if (this.listFieldEditSection[this.activeEditSection][i]._id === this.listFilteredFieldSection[id]._id) {
        this.listFieldEditSection[this.activeEditSection].splice(i, 1);
        i--;
      } else {
        this.listFieldEditSection[this.activeEditSection][i]._id = i;
      }
    }
    this.performFilterFieldSection();
  }

  onAddFields() {
    for (let i = 0; i < this.listFieldSelect.length; i++) {
      if (this.listFieldSelect[i].checked) {
        this.listFieldSelect[i].checked = undefined;
        this.listFieldSelect[i]._id = this.listFieldSection[this.activeEditSection].length;
        this.listFieldEditSection[this.activeEditSection].push(this.listFieldSelect[i]);
        this.listFieldSelect.splice(i, 1);
        i--;
      }
    }
    this.performFilterFieldSection();
  }

  onSaveEditSections() {
    this.spinnerOverlayService.show();
    this.checkAll = false;
    let count = 0;
    if (this.selectedForm.form_type === 'Multi Record View') {
      this.selectedForm.metadata = {};
      for (let i = 0; i < this.listFieldEditSection.length; i++) {
        if (this.listFieldEditSection[i]) {
          if (!this.selectedForm.sections[i].column) {
            this.selectedForm.sections[i].column = 2;
          }
          this.selectedForm.sections[i].metadata = {};
          for (let j = 0; j < this.listFieldEditSection[i].length; j++) {
            if (this.listFieldEditSection[i][j].field_name === 'Blank') {
              this.selectedForm.sections[i].metadata[this.listFieldEditSection[i][j].field_name] = {};
              this.selectedForm.sections[i].metadata[this.listFieldEditSection[i][j].field_name].sort_id = j;
              this.selectedForm.sections[i].metadata[this.listFieldEditSection[i][j].field_name].view_only = this.listFieldEditSection[i][j].view_only;
              this.selectedForm.sections[i].metadata[this.listFieldEditSection[i][j].field_name].mandatory = this.listFieldEditSection[i][j].mandatory;
            } else {
              this.selectedForm.sections[i].metadata[this.listFieldEditSection[i][j].field_id] = {};
              this.selectedForm.sections[i].metadata[this.listFieldEditSection[i][j].field_id].sort_id = j;
              this.selectedForm.sections[i].metadata[this.listFieldEditSection[i][j].field_id].view_only = this.listFieldEditSection[i][j].view_only;
              this.selectedForm.sections[i].metadata[this.listFieldEditSection[i][j].field_id].mandatory = this.listFieldEditSection[i][j].mandatory;

              this.selectedForm.metadata[this.listFieldEditSection[i][j].field_id] = {};
              this.selectedForm.metadata[this.listFieldEditSection[i][j].field_id].sort_id = count;
              this.selectedForm.metadata[this.listFieldEditSection[i][j].field_id].view_only = this.listFieldEditSection[i][j].view_only;
              this.selectedForm.metadata[this.listFieldEditSection[i][j].field_id].mandatory = this.listFieldEditSection[i][j].mandatory;

              if (this.listFieldEditSection[i][j].display_name && this.listFieldEditSection[i][j].display_name.trim() !== '') {
                this.selectedForm.sections[i].metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].display_name.trim();
                this.selectedForm.metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].display_name.trim();
              } else {
                this.selectedForm.sections[i].metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].field_name;
                this.selectedForm.metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].field_name;
                this.listFieldEditSection[i][j].display_name = this.listFieldEditSection[i][j].field_name;
              }
              count++;
            }
          }
        }
      }
    } else if (this.selectedForm.form_type === 'Master Detail View') {
      if (this.activeMasterDetail === 0) {
        this.selectedForm.master_table_metadata = {};
        for (let i = 0; i < this.listFieldEditSection.length; i++) {
          if (this.listFieldEditSection[i]) {
            if (!this.selectedForm.master_table_sections[i].column) {
              this.selectedForm.master_table_sections[i].column = 2;
            }
            this.selectedForm.master_table_sections[i].metadata = {};
            for (let j = 0; j < this.listFieldEditSection[i].length; j++) {
              if (this.listFieldEditSection[i][j].field_name === 'Blank') {
                this.selectedForm.master_table_sections[i].metadata[this.listFieldEditSection[i][j].field_name] = {};
                this.selectedForm.master_table_sections[i].metadata[this.listFieldEditSection[i][j].field_name].sort_id = j;
                this.selectedForm.master_table_sections[i].metadata[this.listFieldEditSection[i][j].field_name].view_only = this.listFieldEditSection[i][j].view_only;
                this.selectedForm.master_table_sections[i].metadata[this.listFieldEditSection[i][j].field_name].mandatory = this.listFieldEditSection[i][j].mandatory;
              } else {
                this.selectedForm.master_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id] = {};
                this.selectedForm.master_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id].sort_id = j;
                this.selectedForm.master_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id].view_only = this.listFieldEditSection[i][j].view_only;
                this.selectedForm.master_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id].mandatory = this.listFieldEditSection[i][j].mandatory;

                this.selectedForm.master_table_metadata[this.listFieldEditSection[i][j].field_id] = {};
                this.selectedForm.master_table_metadata[this.listFieldEditSection[i][j].field_id].sort_id = count;
                this.selectedForm.master_table_metadata[this.listFieldEditSection[i][j].field_id].view_only = this.listFieldEditSection[i][j].view_only;
                this.selectedForm.master_table_metadata[this.listFieldEditSection[i][j].field_id].mandatory = this.listFieldEditSection[i][j].mandatory;

                if (this.listFieldEditSection[i][j].display_name && this.listFieldEditSection[i][j].display_name.trim() !== '') {
                  this.selectedForm.master_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].display_name.trim();
                  this.selectedForm.master_table_metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].display_name.trim();
                } else {
                  this.selectedForm.master_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].field_name;
                  this.selectedForm.master_table_metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].field_name;
                  this.listFieldEditSection[i][j].display_name = this.listFieldEditSection[i][j].field_name;
                }
                count++;
              }
            }
          }
        }
      } else if (this.activeMasterDetail === 1) {
        this.selectedForm.detail_table_metadata = {};
        for (let i = 0; i < this.listFieldEditSection.length; i++) {
          if (this.listFieldEditSection[i]) {
            if (!this.selectedForm.detail_table_sections[i].column) {
              this.selectedForm.detail_table_sections[i].column = 2;
            }
            this.selectedForm.detail_table_sections[i].metadata = {};
            for (let j = 0; j < this.listFieldEditSection[i].length; j++) {
              if (this.listFieldEditSection[i][j].field_name === 'Blank') {
                this.selectedForm.detail_table_sections[i].metadata[this.listFieldEditSection[i][j].field_name] = {};
                this.selectedForm.detail_table_sections[i].metadata[this.listFieldEditSection[i][j].field_name].sort_id = j;
                this.selectedForm.detail_table_sections[i].metadata[this.listFieldEditSection[i][j].field_name].view_only = this.listFieldEditSection[i][j].view_only;
                this.selectedForm.detail_table_sections[i].metadata[this.listFieldEditSection[i][j].field_name].mandatory = this.listFieldEditSection[i][j].mandatory;
              } else {
                this.selectedForm.detail_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id] = {};
                this.selectedForm.detail_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id].sort_id = j;
                this.selectedForm.detail_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id].view_only = this.listFieldEditSection[i][j].view_only;
                this.selectedForm.detail_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id].mandatory = this.listFieldEditSection[i][j].mandatory;

                this.selectedForm.detail_table_metadata[this.listFieldEditSection[i][j].field_id] = {};
                this.selectedForm.detail_table_metadata[this.listFieldEditSection[i][j].field_id].sort_id = count;
                this.selectedForm.detail_table_metadata[this.listFieldEditSection[i][j].field_id].view_only = this.listFieldEditSection[i][j].view_only;
                this.selectedForm.detail_table_metadata[this.listFieldEditSection[i][j].field_id].mandatory = this.listFieldEditSection[i][j].mandatory;

                if (this.listFieldEditSection[i][j].display_name && this.listFieldEditSection[i][j].display_name.trim() !== '') {
                  this.selectedForm.detail_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].display_name.trim();
                  this.selectedForm.detail_table_metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].display_name.trim();
                } else {
                  this.selectedForm.detail_table_sections[i].metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].field_name;
                  this.selectedForm.detail_table_metadata[this.listFieldEditSection[i][j].field_id].display_name = this.listFieldEditSection[i][j].field_name;
                  this.listFieldEditSection[i][j].display_name = this.listFieldEditSection[i][j].field_name;
                }
                count++;
              }
            }
          }
        }
      }
    }
    this.formService.updateForm(this.selectedForm)
      .pipe(first())
      .subscribe(
        async data => {
          if (this.selectedForm.form_type === 'Multi Record View') {
            this.onChangeTableMultiRecord();
          } else if (this.selectedForm.form_type === 'Master Detail View') {
            this.onChangeTableMasterDetail();
          }
          this.performFilterFieldSection();
          this.spinnerOverlayService.hide();
        });
  }

  onChangeTableMultiRecord() {
    this.spinnerOverlayService.show();
    const listFieldName = [];
    this.listField = [];
    for (const a in this.selectedForm.metadata) {
      listFieldName.push([a, this.selectedForm.metadata[a]]);
    }

    if (listFieldName.length !== 0) {
      listFieldName.sort((a, b) => {
        return  b[1].sort_id - a[1].sort_id;
      });
      listFieldName.reverse();

      for (let i = 0; i < listFieldName.length; i++) {
        this.listField[i] = {};
        this.listField[i].field_name = this.listTable[this.tableIndex].metadata[listFieldName[i][0]].field_name;
      }
    }

    // set list field table
    this.listFieldSelect = [];
    const listFieldNameTable = [];
    for (const a in this.listTable[this.tableIndex].metadata) {
      listFieldNameTable.push([a, this.listTable[this.tableIndex].metadata[a]]);
    }

    if (listFieldNameTable.length !== 0) {
      listFieldNameTable.sort((a, b) => {
        return  b[1].sort_id - a[1].sort_id;
      });
      listFieldNameTable.reverse();

      for (let i = 0; i < listFieldNameTable.length; i++) {
        this.listFieldSelect[i] = {};
        this.listFieldSelect[i].field_id = listFieldNameTable[i][0];
        this.listFieldSelect[i].field_name = listFieldNameTable[i][1].field_name;
        this.listFieldSelect[i].type = listFieldNameTable[i][1].type;
        this.listFieldSelect[i].values = listFieldNameTable[i][1].values;
        for (let j = 0; j < this.listField.length; j++) {
          if (this.listFieldSelect[i].field_name === this.listField[j].field_name) {
            this.listFieldSelect.splice(i, 1);
            listFieldNameTable.splice(i, 1);
            i--;
            break;
          }
        }
      }
    }

    // set list field section
    this.listFieldSection = [];
    const listFieldNameTableSection = [];
    for (let i = 0; i < this.selectedForm.sections.length; i++) {
      if (this.selectedForm.sections[i]) {
        listFieldNameTableSection[i] = [];
        for (const a in this.selectedForm.sections[i].metadata) {
          listFieldNameTableSection[i].push([a, this.listTable[this.tableIndex].metadata[a], this.selectedForm.sections[i].metadata[a]]);
        }
      }
    }

    for (let i = 0; i < listFieldNameTableSection.length; i++) {
      this.listFieldSection[i] = [];
      if (listFieldNameTableSection[i].length !== 0) {
        listFieldNameTableSection[i].sort((a, b) => {
          return  b[2].sort_id - a[2].sort_id;
        });
        listFieldNameTableSection[i].reverse();

        for (let j = 0; j < listFieldNameTableSection[i].length; j++) {
          this.listFieldSection[i][j] = {};
          if (listFieldNameTableSection[i][j][0] === 'Blank') {
            this.listFieldSection[i][j].field_name = 'Blank';
            this.listFieldSection[i][j].type = 'Blank';
          } else {
            this.listFieldSection[i][j].field_id = listFieldNameTableSection[i][j][0];
            this.listFieldSection[i][j].field_name = listFieldNameTableSection[i][j][1].field_name;
            this.listFieldSection[i][j].type = listFieldNameTableSection[i][j][1].type;
            this.listFieldSection[i][j].values = listFieldNameTableSection[i][j][1].values;
            this.listFieldSection[i][j].display_name = listFieldNameTableSection[i][j][2].display_name;
            this.listFieldSection[i][j].view_only = listFieldNameTableSection[i][j][2].view_only;
            this.listFieldSection[i][j].mandatory = listFieldNameTableSection[i][j][2].mandatory;
          }
          this.listFieldSection[i][j]._id = j;
        }
      }
    }

    this.performFilter();
    this.spinnerOverlayService.hide();
  }

  onChangeTableMasterDetail() {
    this.spinnerOverlayService.show();
    this.setListSearch();
    this.active = 0;
    const listFieldName = [];
    this.listField = [];

    if (this.activeMasterDetail === 0) {
      for (const a in this.selectedForm.master_table_metadata) {
        listFieldName.push([a, this.selectedForm.master_table_metadata[a]]);
      }
    } else if (this.activeMasterDetail === 1) {
      for (const a in this.selectedForm.detail_table_metadata) {
        listFieldName.push([a, this.selectedForm.detail_table_metadata[a]]);
      }
    }

    if (listFieldName.length !== 0) {
      listFieldName.sort((a, b) => {
        return  b[1].sort_id - a[1].sort_id;
      });
      listFieldName.reverse();

      for (let i = 0; i < listFieldName.length; i++) {
        this.listField[i] = {};
        this.listField[i].field_name = listFieldName[i][1].field_name;
      }
    }

    this.listFieldSelect = [];
    const listFieldNameTable = [];
    if (this.activeMasterDetail === 0) {
      // set list field table
      for (const a in this.listTable[this.masterTableIndex].metadata) {
        listFieldNameTable.push([a, this.listTable[this.masterTableIndex].metadata[a]]);
      }

      if (listFieldNameTable.length !== 0) {
        listFieldNameTable.sort((a, b) => {
          return  b[1].sort_id - a[1].sort_id;
        });
        listFieldNameTable.reverse();

        for (let i = 0; i < listFieldNameTable.length; i++) {
          this.listFieldSelect[i] = {};
          this.listFieldSelect[i].field_id = listFieldNameTable[i][0];
          this.listFieldSelect[i].field_name = listFieldNameTable[i][1].field_name;
          this.listFieldSelect[i].type = listFieldNameTable[i][1].type;
          this.listFieldSelect[i].values = listFieldNameTable[i][1].values;
          for (let j = 0; j < this.listField.length; j++) {
            if (this.listFieldSelect[i].field_name === this.listField[j].field_name) {
              this.listFieldSelect.splice(i, 1);
              listFieldNameTable.splice(i, 1);
              i--;
              break;
            }
          }
        }
      }

      // set list field section
      this.listFieldSection = [];
      const listFieldNameTableSection = [];
      for (let i = 0; i < this.selectedForm.master_table_sections.length; i++) {
        if (this.selectedForm.master_table_sections[i]) {
          listFieldNameTableSection[i] = [];
          for (const a in this.selectedForm.master_table_sections[i].metadata) {
            listFieldNameTableSection[i].push([a, this.listTable[this.masterTableIndex].metadata[a], this.selectedForm.master_table_sections[i].metadata[a]]);
          }
        }
      }

      for (let i = 0; i < listFieldNameTableSection.length; i++) {
        this.listFieldSection[i] = [];
        if (listFieldNameTableSection[i].length !== 0) {
          listFieldNameTableSection[i].sort((a, b) => {
            return  b[1].sort_id - a[1].sort_id;
          });
          listFieldNameTableSection[i].reverse();

          for (let j = 0; j < listFieldNameTableSection[i].length; j++) {
            this.listFieldSection[i][j] = {};
            if (listFieldNameTableSection[i][j][0] === 'Blank') {
              this.listFieldSection[i][j].field_name = 'Blank';
              this.listFieldSection[i][j].type = 'Blank';
            } else {
              this.listFieldSection[i][j].field_id = listFieldNameTableSection[i][j][0];
              this.listFieldSection[i][j].field_name = listFieldNameTableSection[i][j][1].field_name;
              this.listFieldSection[i][j].type = listFieldNameTableSection[i][j][1].type;
              this.listFieldSection[i][j].values = listFieldNameTableSection[i][j][1].values;
              this.listFieldSection[i][j].display_name = listFieldNameTableSection[i][j][2].display_name;
              this.listFieldSection[i][j].view_only = listFieldNameTableSection[i][j][2].view_only;
              this.listFieldSection[i][j].mandatory = listFieldNameTableSection[i][j][2].mandatory;
            }
            this.listFieldSection[i][j]._id = j;
          }
        }
      }

      this.listFilteredField = this.listFieldSection[this.activeEditSection];
    } else if (this.activeMasterDetail === 1) {
      // set list field table
      for (const a in this.listTable[this.detailTableIndex].metadata) {
        listFieldNameTable.push([a, this.listTable[this.detailTableIndex].metadata[a]]);
      }

      if (listFieldNameTable.length !== 0) {
        listFieldNameTable.sort((a, b) => {
          return  b[1].sort_id - a[1].sort_id;
        });
        listFieldNameTable.reverse();

        for (let i = 0; i < listFieldNameTable.length; i++) {
          this.listFieldSelect[i] = {};
          this.listFieldSelect[i].field_id = listFieldNameTable[i][0];
          this.listFieldSelect[i].field_name = listFieldNameTable[i][1].field_name;
          this.listFieldSelect[i].type = listFieldNameTable[i][1].type;
          this.listFieldSelect[i].values = listFieldNameTable[i][1].values;
          for (let j = 0; j < this.listField.length; j++) {
            if (this.listFieldSelect[i].field_name === this.listField[j].field_name) {
              this.listFieldSelect.splice(i, 1);
              listFieldNameTable.splice(i, 1);
              i--;
              break;
            }
          }
        }
      }

      // set list field section
      this.listFieldSection = [];
      const listFieldNameTableSection = [];
      for (let i = 0; i < this.selectedForm.detail_table_sections.length; i++) {
        if (this.selectedForm.detail_table_sections[i]) {
          listFieldNameTableSection[i] = [];
          for (const a in this.selectedForm.detail_table_sections[i].metadata) {
            listFieldNameTableSection[i].push([a, this.listTable[this.detailTableIndex].metadata[a], this.selectedForm.detail_table_sections[i].metadata[a]]);
          }
        }
      }

      for (let i = 0; i < listFieldNameTableSection.length; i++) {
        this.listFieldSection[i] = [];
        if (listFieldNameTableSection[i].length !== 0) {
          listFieldNameTableSection[i].sort((a, b) => {
            return  b[1].sort_id - a[1].sort_id;
          });
          listFieldNameTableSection[i].reverse();

          for (let j = 0; j < listFieldNameTableSection[i].length; j++) {
            this.listFieldSection[i][j] = {};
            if (listFieldNameTableSection[i][j][0] === 'Blank') {
              this.listFieldSection[i][j].field_name = 'Blank';
              this.listFieldSection[i][j].type = 'Blank';
            } else {
              this.listFieldSection[i][j].field_id = listFieldNameTableSection[i][j][0];
              this.listFieldSection[i][j].field_name = listFieldNameTableSection[i][j][1].field_name;
              this.listFieldSection[i][j].type = listFieldNameTableSection[i][j][1].type;
              this.listFieldSection[i][j].values = listFieldNameTableSection[i][j][1].values;
              this.listFieldSection[i][j].display_name = listFieldNameTableSection[i][j][2].display_name;
              this.listFieldSection[i][j].view_only = listFieldNameTableSection[i][j][2].view_only;
              this.listFieldSection[i][j].mandatory = listFieldNameTableSection[i][j][2].mandatory;
            }
            this.listFieldSection[i][j]._id = j;
          }
        }
      }
      this.performFilter();
    }
    this.spinnerOverlayService.hide();
  }

  onSelectMasterTableAddData() {
    this.listDetailTable = [];
    const json: any = {};
    json.master_table_id = this.newForm.master_table_id;
    this.tableService.getDetailTable(json)
      .pipe(first())
      .subscribe(
        data => {
          this.listDetailTable = data;
        });
  }

  onChangeSection(isEdit) {
    if (isEdit) {
      this.setListSearchEditField();
      this.performFilterFieldSection();
    } else {
      this.setListSearch();
      this.performFilter();
    }
  }

  onRemoveAllField() {
    for (let i = 0; i < this.listFieldEditSection[this.activeEditSection].length; i++) {
      this.listFieldEditSection[this.activeEditSection][i].view_only = false;
      this.listFieldEditSection[this.activeEditSection][i].mandatory = false;
      this.listFieldSelect.push(this.listFieldEditSection[this.activeEditSection][i]);
    }
    this.listFieldEditSection[this.activeEditSection] = [];
    this.performFilterFieldSection();
  }

  onDeleteSection() {
    this.onRemoveAllField();
    this.listFieldEditSection.splice(this.activeEditSection, 1);
    if (this.selectedForm.form_type === 'Multi Record View') {
      this.selectedForm.sections.splice(this.activeEditSection, 1);
    } else if (this.selectedForm.form_type === 'Master Detail View') {
      if (this.activeMasterDetail === 0) {
        this.selectedForm.master_table_sections.splice(this.activeEditSection, 1);
      } else if (this.activeMasterDetail === 1) {
        this.selectedForm.detail_table_sections.splice(this.activeEditSection, 1);
      }
    }
    this.activeEditSection = 0;
    this.performFilterFieldSection();
    this.modalReference.close();
  }

  onCheckAll() {
    for (let i = 0; i < this.listFilteredFieldSelect.length; i++) {
      this.listFilteredFieldSelect[i].checked = this.checkAll;
    }
    this.performFilterFieldSelect();
  }

  onCheckAllViewOnly() {
    for (let i = 0; i < this.listFilteredFieldSection.length; i++) {
      if (this.listFilteredFieldSection[i].field_name !== 'Blank') {
        this.listFilteredFieldSection[i].view_only = this.checkAllViewOnly;
      }
    }
    this.performFilterFieldSelect();
  }

  onCheckAllMandatory() {
    for (let i = 0; i < this.listFilteredFieldSection.length; i++) {
      if (this.listFilteredFieldSection[i].field_name !== 'Blank') {
        this.listFilteredFieldSection[i].mandatory = this.checkAllMandatory;
      }
    }
    this.performFilterFieldSelect();
  }

  floorNumber(num: number) {
    return Math.floor(num);
  }
}
