import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProjectService} from '../../services/project.service';
import {DataService} from '../../services/data.service';
import {UtilService} from '../../services/util.service';
import {Subject} from 'rxjs';
import {debounceTime, first} from 'rxjs/operators';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {TableService} from '../../services/table.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';

@Component({
  selector: 'app-manage-tables',
  templateUrl: './manage-tables.component.html',
  styleUrls: ['./manage-tables.component.scss']
})
export class ManageTablesComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private projectService: ProjectService,
    private dataService: DataService,
    private utilService: UtilService,
    private tableService: TableService,
    private spinnerOverlayService: SpinnerOverlayService
  ) { }
  @ViewChild('importBtn', {static: false}) importBtn: ElementRef;
  selectedFile: File;

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  modalReference: any;
  newTable: any = {
    default_primary_key: true
  };
  listTable: any = [];
  selectedTable: any;
  listFieldName: any = [];
  listFieldRelation: any = [];
  listField: any = [];
  selectedIndex: number;
  listSelect: any = [];
  numCol: any;
  numRow: any;
  listSearch: any = {};
  listFilteredField: any = [];
  oldNameField = '';
  newField: any = {};

  // tslint:disable:forin prefer-for-of
  ngOnInit() {
    this.setUpMessage();
    this.tableService.getAllTable()
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.listTable = [];
          for (let i = 0; i < data.length; i++) {
            this.listTable.push(
              {
                name: data[i].name,
                _id: data[i]._id
              }
            );
          }
        });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  setListSearch() {
    this.listSearch.name = '';
    this.listSearch.type = '';
  }

  performFilter() {
    this.listFilteredField = this.listField.filter((field: any) =>
      field.field_name.toLocaleLowerCase().indexOf(this.listSearch.name.toLocaleLowerCase()) !== -1 &&
      field.type.toLocaleLowerCase().indexOf(this.listSearch.type.toLocaleLowerCase()) !== -1
    );
  }

  checkInformation(table: any) {
    if (!this.utilService.checkValue(table.name)) {
      this.message.next('Table Name must not be blanked');
      return false;
    }
    for (let i = 0; i < this.listTable.length; i++) {
      if (this.listTable[i].name === table.name) {
        this.message.next('Table Name existed');
        return false;
      }
    }
    if (!table.default_primary_key) {
      if (!this.utilService.checkValue(table.primary_key_name)) {
        this.message.next('Primary Key must not be blanked');
        return false;
      }
      if (!this.utilService.checkValue(table.primary_key_type)) {
        this.message.next('Data Type must not be blanked');
        return false;
      }
    }
    return true;
  }

  drop(event: CdkDragDrop<string[]>) {
    if (this.listSearch.name !== '' || this.listSearch.type !== '') {
      return;
    }
    moveItemInArray(this.listField, event.previousIndex, event.currentIndex);
    this.listFilteredField = this.listField;
  }

  trackBy(index: any, item: any) {
    return index;
  }

  onPopUp(content: any, modalBig: boolean) {
    if (modalBig) {
      this.modalReference = this.modalService.open(content, { windowClass : 'customModalClassBig'});
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    } else {
      this.modalReference = this.modalService.open(content);
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    }
  }

  onSelectFieldType() {
    this.listFilteredField[this.selectedIndex].values = undefined;
    if (this.listFilteredField[this.selectedIndex].type === 'Relation') {
      this.listFilteredField[this.selectedIndex].values = {};
    } else if (this.listFilteredField[this.selectedIndex].type === 'Dropdown') {
      this.listFilteredField[this.selectedIndex].values = [];
    }
  }

  onSelectNewFieldType() {
    if (this.newField.type === 'Relation') {
      this.newField.values = {};
    } else if (this.newField.type === 'Dropdown') {
      this.newField.values = [];
    }
  }

  onPopUpDropdown(content: any, id: any) {
    this.selectedIndex = id;
    this.listSelect = [];
    if (this.utilService.checkValue(this.listFilteredField[id].values)) {
      this.listSelect = this.listFilteredField[id].values;
      this.listFilteredField[id].values = [...this.listFilteredField[id].values];
    }
    this.onPopUp(content, false);
  }

  onPopUpDropdownAddField(content: any, modalBig: boolean) {
    this.listSelect = [];
    if (this.utilService.checkValue(this.newField.values)) {
      this.listSelect = this.newField.values;
      this.newField.values = [...this.newField.values];
    } else {
      this.newField.values = [];
    }
    this.onPopUp(content, modalBig);
  }

  onPopUpSelectedIndex(content: any, id: any) {
    this.selectedIndex = id;
    this.onPopUp(content, false);
  }

  onPopUpEditField(content: any, modalBig: any, id: any) {
    this.selectedIndex = id;
    this.oldNameField = this.listFilteredField[this.selectedIndex].field_name;
    if (this.listFilteredField[this.selectedIndex].type === 'Relation') {
      if (this.listFilteredField[this.selectedIndex].values.table_id) {
        this.onSelectTableRelation(false, false);
      }
    }
    this.onPopUp(content, modalBig);
  }

  onAddSelectValue() {
    const newRow = '';
    this.listSelect.push(newRow);
  }

  onDeleteSelect(id: any) {
    this.listSelect.splice(id, 1);
  }

  onSelectTableRelation(isAdd: boolean, isResetFieldName: boolean) {
    this.spinnerOverlayService.show();
    this.listFieldRelation = [];
    let tableId: string;
    if (isAdd) {
      if (isResetFieldName) {
        this.newField.values.field_name = undefined;
      }
      tableId = this.newField.values.table_id;
    } else {
      if (isResetFieldName) {
        this.listFilteredField[this.selectedIndex].values.field_name = undefined;
      }
      tableId = this.listFilteredField[this.selectedIndex].values.table_id;
    }
    this.tableService.getTableWithId(tableId)
      .pipe(first())
      .subscribe(
        (data: any) => {
          for (const a in data.metadata) {
            if (data.metadata[a].is_unique) {
              this.listFieldRelation.push([a, data.metadata[a]]);
            }
          }

          if (this.listFieldRelation.length !== 0) {
            this.listFieldRelation.sort((a, b) => {
              return b[1].sort_id - a[1].sort_id;
            });
            this.listFieldRelation.reverse();
          }
          for (let i = 0; i < this.listFieldRelation.length; i++) {
            this.listFieldRelation[i] = {
              field_id: this.listFieldRelation[i][0],
              field_name: this.listFieldRelation[i][1].field_name
            };
          }

          this.spinnerOverlayService.hide();
        });
  }

  onAddTable() {
    if (!this.checkInformation(this.newTable)) {
      return;
    }
    this.spinnerOverlayService.show();
    const newTable: any = {};
    newTable.name = this.newTable.name;
    if (this.newTable.default_primary_key) {
      newTable.primary_key = 'row_id';
      newTable.use_default_primary_key = true;
      newTable.metadata = {
        row_id: {
          sort_id: 0,
          type: 'Auto Increment',
          is_unique: true
        }
      };
    } else {
      newTable.primary_key = this.newTable.primary_key_name;
      newTable.use_default_primary_key = false;
      newTable.metadata = {
        [this.newTable.primary_key_name]: {
          sort_id: 0,
          type: this.newTable.primary_key_type,
          is_unique: true
        }
      };
    }
    this.tableService.addNewTable(newTable)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.listTable = [...this.listTable, {
            name: this.newTable.name,
            _id: data._id
          }];
          this.newTable = {
            default_primary_key: true
          };
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  getTableInfo(id: any): Promise<void> {
    return new Promise<void>(resolve => {
      this.tableService.getTableWithId(id)
        .pipe(first())
        .subscribe(
          (data1: any) => {
            this.selectedTable = data1;
            resolve();
          });
    });
  }

  async onSelectTable(event: any) {
    if (event) {
      this.spinnerOverlayService.show();
      this.setListSearch();
      await this.getTableInfo(event._id);
      this.listFieldName = [];
      this.listField = [];
      this.listFilteredField = [];
      for (const fieldId in this.selectedTable.metadata) {
        this.listFieldName.push([fieldId, this.selectedTable.metadata[fieldId]]);
      }

      if (this.listFieldName.length === 0) {
        this.spinnerOverlayService.hide();
        return;
      }

      this.listFieldName.sort((a, b) => {
        return b[1].sort_id - a[1].sort_id;
      });
      this.listFieldName.reverse();

      for (let i = 0; i < this.listFieldName.length; i++) {
        this.listField[i] = this.listFieldName[i][1];
        this.listField[i].field_id = this.listFieldName[i][0];
      }
      this.listFilteredField = this.listField;
      this.spinnerOverlayService.hide();
    }
  }

  onDeleteTable() {
    this.spinnerOverlayService.show();
    this.tableService.deleteTable(this.selectedTable._id)
      .pipe(first())
      .subscribe(
        data => {
          for (let i = 0 ; i < this.listTable.length; i++) {
            if (this.listTable[i]._id === this.selectedTable._id) {
              this.listTable.splice(i, 1);
              this.listTable = [...this.listTable];
              break;
            }
          }
          this.selectedTable = null;
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onAddField() {
    this.spinnerOverlayService.show();
    const json: any = {};
    json._id = this.selectedTable._id;
    json.field_name = this.newField.field_name;
    json.field_info = {
      sort_id: this.listField.length,
      type: this.newField.type,
      values: this.newField.values,
      is_unique: this.newField.is_unique
    };
    this.tableService.addField(json)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.newField._id = data.inserted_id;
          this.listField.push(this.newField);
          this.newField = {};
          this.performFilter();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onSelectSortId(previousIndex: any, event: any) {
    moveItemInArray(this.listField, previousIndex, event.target.value - 1);
    this.performFilter();
  }

  onSaveField() {
    this.spinnerOverlayService.show();
    const json: any = [];
    for (let i = 0; i < this.listField.length; i++) {
      this.listField[i].sort_id = i;
      json[i] = {};
      json[i]._id = this.listField[i]._id;
      json[i].field_name = this.listField[i].field_name;
      json[i].sort_id = i;
    }
    this.tableService.updateManyFields(json)
      .pipe(first())
      .subscribe(
        data => {
          this.spinnerOverlayService.hide();
        });
  }

  onSaveEditField() {
    this.spinnerOverlayService.show();
    const json: any = {};
    json._id = this.listFilteredField[this.selectedIndex]._id;
    json.field_info = this.utilService.deepCopy(this.listFilteredField[this.selectedIndex]);
    delete json.field_info._id;
    this.tableService.updateField(json)
      .pipe(first())
      .subscribe(
        data => {
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onSaveValues() {
    this.listFilteredField[this.selectedIndex].values = this.listSelect;
    this.modalReference.close();
  }

  onSaveAddValues() {
    this.newField.values = this.listSelect;
    this.modalReference.close();
  }

  onDeleteField() {
    this.spinnerOverlayService.show();
    this.tableService.deleteField(this.listFilteredField[this.selectedIndex]._id)
      .pipe(first())
      .subscribe(
        data => {
          this.listField.splice(this.listField.indexOf(this.listFilteredField[this.selectedIndex]), 1);
          this.performFilter();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onFileChangedImportTable(event) {
    this.selectedFile = event.target.files[0];
    this.onUpload();
  }

  onUpload() {
    this.spinnerOverlayService.show();
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    this.tableService.onUploadImportTable(uploadData, this.selectedTable._id)
      .pipe(first())
      .subscribe(
        (data1: any) => {
          this.onSelectTable(this.selectedTable);
          this.spinnerOverlayService.hide();
        });
    this.importBtn.nativeElement.value = '';
  }

  onDownloadTemplate() {
    this.tableService.onDownloadTableTemplate()
      .pipe(first())
      .subscribe(
        (data: any) => {
        });
  }
}
