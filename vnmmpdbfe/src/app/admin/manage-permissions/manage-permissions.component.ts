import { Component, OnInit } from '@angular/core';
import {RecordGroupService} from '../../services/record-group.service';
import {RoleService} from '../../services/role.service';
import {UtilService} from '../../services/util.service';
import {RecordGroupPermissionService} from '../../services/record-group-permission.service';
import {FieldPermissionService} from '../../services/field-permission.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';
import {TableService} from '../../services/table.service';
import {Subject} from 'rxjs';
import {debounceTime, first} from 'rxjs/operators';
import {ProjectService} from '../../services/project.service';
import {FormService} from '../../services/form.service';
import {FormPermissionService} from '../../services/form-permission.service';

@Component({
  selector: 'app-manage-permissions',
  templateUrl: './manage-permissions.component.html',
  styleUrls: ['./manage-permissions.component.scss']
})
export class ManagePermissionsComponent implements OnInit {

  constructor(
    private recordGroupService: RecordGroupService,
    private roleService: RoleService,
    private utilService: UtilService,
    private recordGroupPermissionService: RecordGroupPermissionService,
    private fieldPermissionService: FieldPermissionService,
    private spinnerOverlayService: SpinnerOverlayService,
    private tableService: TableService,
    private projectService: ProjectService,
    private formService: FormService,
    private formPermissionService: FormPermissionService
  ) { }

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  active = 0;

  listTable: any = [];
  selectedTable: any;

  listProject: any = [];
  selectedProject: any;
  listForm: any = [];
  listSearchForm = '';
  listSetRoleForm: any = {};
  listFilteredForm: any = [];

  listRecordGroups: any = [];
  listSetRoleRecordGroup: any = {};
  listSearchRg = '';
  listFilteredRg: any = [];

  listRoles: any = [];
  listSetRoleField: any = {};
  listSearchField = '';
  listFilteredField: any = [];

  listField: any;
  listRecordGroupPermission: any = [];
  listFieldPermission: any = [];
  listFormPermission: any = [];

  page = 1;
  pageSize = 5;

  listCheckAllRgs: any = [];
  listCheckAllFields: any = [];
  listCheckAllForms: any = [];

  listSearchRole = '';
  listFilteredRole: any = [];

  // tslint:disable:prefer-for-of forin
  async ngOnInit() {
    this.spinnerOverlayService.show();
    this.setUpMessage();

    this.tableService.getAllTable()
      .pipe(first())
      .subscribe(
        data => {
          this.listTable = data;
        });

    this.recordGroupService.getAllRecordGroup()
      .pipe(first())
      .subscribe(
        data => {
          this.listRecordGroups = data;
          this.utilService.sortStringAsc(this.listRecordGroups, 'name');
        });

    await this.getAllProjects();
    await this.getAllRoles();
    this.spinnerOverlayService.hide();
    this.onChangeForm();
  }

  getAllProjects() {
    return new Promise<void>(resolve => {
      this.projectService.getAllProject()
        .pipe(first())
        .subscribe(
          data => {
            this.listProject = data;
            this.utilService.sortStringAsc(this.listProject, 'Project Name');
            resolve();
          });
    });
  }

  getAllRoles(): Promise<void> {
    return new Promise<void>(resolve => {
      this.roleService.getAllRole()
        .pipe(first())
        .subscribe(
          data => {
            this.utilService.sortStringAsc(this.listRoles, 'name');
            this.listRoles = data;
            resolve();
          });
    });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  setListSearchRg() {
    this.listSearchRg = '';
    this.listSearchRole = '';
  }

  setListSearchField() {
    this.listSearchField = '';
    this.listSearchRole = '';
  }

  setListSearchForm() {
    this.listSearchRole = '';
    this.listSearchForm = '';
  }

  performFilterRg() {
    this.listFilteredRg = this.listRecordGroups.filter((field: any) =>
      field.name.toLocaleLowerCase().indexOf(this.listSearchRg.toLocaleLowerCase()) !== -1
    );
  }

  performFilterField() {
    this.listFilteredField = this.listField.filter((field: any) =>
      field.field_name.toLocaleLowerCase().indexOf(this.listSearchField.toLocaleLowerCase()) !== -1
    );
  }

  performFilterRole() {
    this.listFilteredRole = this.listRoles.filter((field: any) =>
      field.name.toLocaleLowerCase().indexOf(this.listSearchRole.toLocaleLowerCase()) !== -1
    );
  }

  performFilterForm() {
    this.listFilteredForm = this.listForm.filter((field: any) =>
      field.name.toLocaleLowerCase().indexOf(this.listSearchForm.toLocaleLowerCase()) !== -1
    );
  }

  onCheckAllRgs(idRow: any) {
    for (let i = 0; i < this.listFilteredRg.length; i++) {
      this.listSetRoleRecordGroup[this.listFilteredRole[idRow].name][this.listFilteredRg[i].name].S = this.listCheckAllRgs[idRow];
      this.listSetRoleRecordGroup[this.listFilteredRole[idRow].name][this.listFilteredRg[i].name].U = this.listCheckAllRgs[idRow];
      this.listSetRoleRecordGroup[this.listFilteredRole[idRow].name][this.listFilteredRg[i].name].I = this.listCheckAllRgs[idRow];
      this.listSetRoleRecordGroup[this.listFilteredRole[idRow].name][this.listFilteredRg[i].name].D = this.listCheckAllRgs[idRow];
    }
  }

  onCheckAllFields(idRow: any) {
    for (let i = 0; i < this.listFilteredField.length; i++) {
      this.listSetRoleField[this.listFilteredRole[idRow].name][this.listFilteredField[i].field_id].S = this.listCheckAllFields[idRow];
      this.listSetRoleField[this.listFilteredRole[idRow].name][this.listFilteredField[i].field_id].U = this.listCheckAllFields[idRow];
    }
  }

  onCheckAllForms(idRow: any) {
    for (let i = 0; i < this.listFilteredForm.length; i++) {
      this.listSetRoleForm[this.listFilteredRole[idRow].name][this.listFilteredForm[i].name] = this.listCheckAllForms[idRow];
    }
  }

  onChangeForm() {
    this.spinnerOverlayService.show();
    this.listFilteredRole = this.listRoles;
    if (this.active === 0) {
      this.selectedProject = null;
      this.listField = null;
      this.spinnerOverlayService.hide();
    } else if (this.active === 1) {
      this.setListSearchRg();
      this.listFilteredRg = this.listRecordGroups;
      this.listSetRoleRecordGroup = {};
      for (let i = 0; i < this.listRoles.length; i++) {
        this.listSetRoleRecordGroup[this.listRoles[i].name] = {};
        for (let j = 0; j < this.listRecordGroups.length; j++) {
          this.listSetRoleRecordGroup[this.listRoles[i].name][this.listRecordGroups[j].name] = {};
        }
      }
      this.recordGroupPermissionService.getAllRecordGroupPermission()
        .pipe(first())
        .subscribe(
          data => {
            this.listRecordGroupPermission = data;
            // tslint:disable:prefer-for-of
            for (let k = 0; k < this.listRecordGroupPermission.length; k++) {
              for (let i = 0; i < this.listRoles.length; i ++) {
                if (this.listRecordGroupPermission[k].role_id === this.listRoles[i]._id) {
                  for (let j = 0; j < this.listRecordGroups.length; j++) {
                    if (this.listRecordGroupPermission[k].record_group_id === this.listRecordGroups[j]._id) {
                      this.listSetRoleRecordGroup[this.listRoles[i].name][this.listRecordGroups[j].name].S = this.listRecordGroupPermission[k].S;
                      this.listSetRoleRecordGroup[this.listRoles[i].name][this.listRecordGroups[j].name].U = this.listRecordGroupPermission[k].U;
                      this.listSetRoleRecordGroup[this.listRoles[i].name][this.listRecordGroups[j].name].I = this.listRecordGroupPermission[k].I;
                      this.listSetRoleRecordGroup[this.listRoles[i].name][this.listRecordGroups[j].name].D = this.listRecordGroupPermission[k].D;
                      break;
                    }
                  }
                  break;
                }
              }
            }
            this.spinnerOverlayService.hide();
          });
    } else if (this.active === 2) {
      this.selectedTable = null;
      this.listField = null;
      this.spinnerOverlayService.hide();
    }
  }

  onSaveRolesRecordGroup() {
    this.spinnerOverlayService.show();
    for (let i = 0; i < this.listRoles.length; i++) {
      for (let j = 0; j < this.listRecordGroups.length; j++) {
        const recordGroupPermission: any = {};
        let check = false;
        recordGroupPermission.role_id = this.listRoles[i]._id;
        recordGroupPermission.record_group_id = this.listRecordGroups[j]._id;
        recordGroupPermission.S = this.listSetRoleRecordGroup[this.listRoles[i].name][this.listRecordGroups[j].name].S;
        recordGroupPermission.U = this.listSetRoleRecordGroup[this.listRoles[i].name][this.listRecordGroups[j].name].U;
        recordGroupPermission.I = this.listSetRoleRecordGroup[this.listRoles[i].name][this.listRecordGroups[j].name].I;
        recordGroupPermission.D = this.listSetRoleRecordGroup[this.listRoles[i].name][this.listRecordGroups[j].name].D;
        if (recordGroupPermission.U || recordGroupPermission.D) {
          recordGroupPermission.S = true;
        }
        if (!recordGroupPermission.S) {
          recordGroupPermission.S = false;
        }
        if (!recordGroupPermission.U) {
          recordGroupPermission.U = false;
        }
        if (!recordGroupPermission.I) {
          recordGroupPermission.I = false;
        }
        if (!recordGroupPermission.D) {
          recordGroupPermission.D = false;
        }
        for (let k = 0; k < this.listRecordGroupPermission.length; k++) {
          if (recordGroupPermission.role_id === this.listRecordGroupPermission[k].role_id
            && recordGroupPermission.record_group_id === this.listRecordGroupPermission[k].record_group_id) {
            check = true;
            recordGroupPermission._id = this.listRecordGroupPermission[k]._id;
            this.listRecordGroupPermission[k] = recordGroupPermission;
            break;
          }
        }
        if (check) {
        } else {
          this.listRecordGroupPermission.push(recordGroupPermission);
        }
      }
    }
    this.recordGroupPermissionService.updateListRecordGroupsPermission(this.listRecordGroupPermission)
      .pipe(first())
      .subscribe(
        data => {
          this.spinnerOverlayService.hide();
          this.onChangeForm();
        });
  }

  onSaveRolesField() {
    this.spinnerOverlayService.show();
    for (let i = 0; i < this.listRoles.length; i++) {
      for (let j = 0; j < this.listField.length; j++) {
        const fieldPermission: any = {};
        let check = false;
        fieldPermission.table_id = this.selectedTable._id;
        fieldPermission.role_id = this.listRoles[i]._id;
        fieldPermission.field_id = this.listField[j].field_id;
        fieldPermission.field_permission = {};
        fieldPermission.field_permission.S = this.listSetRoleField[this.listRoles[i].name][this.listField[j].field_id].S;
        fieldPermission.field_permission.U = this.listSetRoleField[this.listRoles[i].name][this.listField[j].field_id].U;
        if (!fieldPermission.field_permission.S) {
          fieldPermission.field_permission.S = false;
        }
        if (!fieldPermission.field_permission.U) {
          fieldPermission.field_permission.U = false;
        }
        if (fieldPermission.field_permission.U) {
          fieldPermission.field_permission.S = true;
        }
        for (let k = 0; k < this.listFieldPermission.length; k++) {
          if (fieldPermission.role_id === this.listFieldPermission[k].role_id
            && this.listFieldPermission[k].table_id === this.selectedTable._id
            && fieldPermission.field_id === this.listFieldPermission[k].field_id) {
            check = true;
            fieldPermission._id = this.listFieldPermission[k]._id;
            this.listFieldPermission[k] = fieldPermission;
            break;
          }
        }
        if (check) {
        } else {
          this.listFieldPermission.push(fieldPermission);
        }
      }
    }
    this.fieldPermissionService.updateListFieldsPermission(this.listFieldPermission)
      .pipe(first())
      .subscribe(
        data => {
          this.onSelectTable(this.selectedTable);
          this.spinnerOverlayService.hide();
        });
  }

  onSelectTable(event: any) {
    if (event) {
      this.setListSearchField();
      this.spinnerOverlayService.show();
      this.selectedTable = event;
      this.listField = [];
      for (const a in this.selectedTable.metadata) {
        this.listField.push({
          field_id: a,
          field_name: this.selectedTable.metadata[a].field_name
        });
      }
      this.listFilteredField = this.listField;

      this.listSetRoleField = {};
      for (let i = 0; i < this.listRoles.length; i++) {
        this.listSetRoleField[this.listRoles[i].name] = {};
        for (let j = 0; j < this.listField.length; j++) {
          this.listSetRoleField[this.listRoles[i].name][this.listField[j].field_id] = {};
        }
      }

      const json: any = {};
      json.table_id = this.selectedTable._id;
      this.fieldPermissionService.getFieldPermissionWithCondition(json)
        .pipe(first())
        .subscribe(
          data => {
            this.listFieldPermission = data;
            for (let k = 0; k < this.listFieldPermission.length; k++) {
              for (let i = 0; i < this.listRoles.length; i ++) {
                if (this.listFieldPermission[k].role_id === this.listRoles[i]._id) {
                  for (let j = 0; j < this.listField.length; j++) {
                    if (this.listFieldPermission[k].field_id === this.listField[j].field_id) {
                      this.listSetRoleField[this.listRoles[i].name][this.listField[j].field_id].S = this.listFieldPermission[k].field_permission.S;
                      this.listSetRoleField[this.listRoles[i].name][this.listField[j].field_id].U = this.listFieldPermission[k].field_permission.U;
                      break;
                    }
                  }
                  break;
                }
              }
            }
            this.spinnerOverlayService.hide();
          });
    } else {
      this.selectedTable = null;
    }
  }

  onSelectProject(event: any) {
    if (event) {
      this.spinnerOverlayService.show();
      this.setListSearchForm();

      this.listSetRoleForm = {};
      for (let i = 0; i < this.listRoles.length; i++) {
        this.listSetRoleForm[this.listRoles[i].name] = {};
      }

      this.selectedProject = event;
      const json: any = {};
      json.project_id = this.selectedProject._id;
      this.formService.getFormWithCondition(json)
        .pipe(first())
        .subscribe(
          data => {
            this.listForm = data;
            this.listFilteredForm = this.listForm;

            this.formPermissionService.getFormPermissionWithCondition(json)
              .pipe(first())
              .subscribe(
                data1 => {
                  this.listFormPermission = data1;
                  for (let k = 0; k < this.listFormPermission.length; k++) {
                    for (let i = 0; i < this.listRoles.length; i ++) {
                      if (this.listFormPermission[k].role_id === this.listRoles[i]._id) {
                        for (let j = 0; j < this.listForm.length; j++) {
                          if (this.listFormPermission[k].form_id === this.listForm[j]._id) {
                            this.listSetRoleForm[this.listRoles[i].name][this.listForm[j].name] = this.listFormPermission[k].form_permission;
                            break;
                          }
                        }
                        break;
                      }
                    }
                  }
                  this.spinnerOverlayService.hide();
                });
          });
    } else {
      this.selectedProject = null;
    }
  }

  onSaveRolesForm() {
    this.spinnerOverlayService.show();
    for (let i = 0; i < this.listRoles.length; i++) {
      for (let j = 0; j < this.listForm.length; j++) {
        const formPermission: any = {};
        let check = false;
        formPermission.project_id = this.selectedProject._id;
        formPermission.role_id = this.listRoles[i]._id;
        formPermission.form_id = this.listForm[j]._id;
        if (!this.listSetRoleForm[this.listRoles[i].name][this.listForm[j].name]) {
          this.listSetRoleForm[this.listRoles[i].name][this.listForm[j].name] = false;
        }
        formPermission.form_permission = this.listSetRoleForm[this.listRoles[i].name][this.listForm[j].name];
        for (let k = 0; k < this.listFormPermission.length; k++) {
          if (formPermission.role_id === this.listFormPermission[k].role_id
            && this.listFormPermission[k].project_id === this.selectedProject._id
            && formPermission.form_id === this.listFormPermission[k].form_id) {
            check = true;
            formPermission._id = this.listFormPermission[k]._id;
            this.listFormPermission[k] = formPermission;
            break;
          }
        }
        if (!check) {
          this.listFormPermission.push(formPermission);
        }
      }
    }
    this.formPermissionService.updateListFormPermission(this.listFormPermission)
      .pipe(first())
      .subscribe(
        data => {
          this.spinnerOverlayService.hide();
          this.onSelectProject(this.selectedProject);
        });
  }

  getListRoles() {
    return this.listFilteredRole
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  getNumberColRecordGroup() {
    return new Array(this.listFilteredRg.length * 4);
  }

  getNumberColField() {
    return new Array(this.listFilteredField.length * 2);
  }

  getCurrentColRg(id: any) {
    return this.listFilteredRg[Math.floor(id / 4)].name;
  }

  getCurrentColField(id: any) {
    return this.listFilteredField[Math.floor(id / 2)].field_id;
  }

  getCurrentId(id: any) {
    return (id + (this.page - 1) * this.pageSize);
  }
}
