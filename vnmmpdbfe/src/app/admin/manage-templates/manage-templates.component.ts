import { Component, OnInit } from '@angular/core';
import {TableService} from '../../services/table.service';
import {RoleService} from '../../services/role.service';
import {debounceTime, first} from 'rxjs/operators';
import {FieldPermissionService} from '../../services/field-permission.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Subject} from 'rxjs';
import {DataService} from '../../services/data.service';
import {ExportService} from '../../services/export.service';
import {UtilService} from '../../services/util.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-manage-templates',
  templateUrl: './manage-templates.component.html',
  styleUrls: ['./manage-templates.component.scss']
})
export class ManageTemplatesComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private tableService: TableService,
    private roleService: RoleService,
    private fieldPermissionService: FieldPermissionService,
    private spinnerOverlayService: SpinnerOverlayService,
    private dataService: DataService,
    private exportService: ExportService,
    private utilService: UtilService,
  ) { }

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  modalReference: any;

  selectedFile: File;

  active = 0;
  page = 1;
  pageSize = 5;

  listTable: any = [];
  listRole: any = [];

  listTemplate: any = [];
  listFilteredTemplate: any = [];

  popUpTemplate: any;
  popUpTemplateId: number;

  listSearch: any = {};
  listSearchSelect: any = {};
  listSearchTemplate: any = {};

  selectedTable: any;
  selectedRole: any;

  listFieldSelect: any = [];
  listFieldTemplate: any = [];

  listFilteredFieldSelect: any = [];
  listFilteredFieldTemplate: any = [];

  listDropdownValue: any = [];
  dropdownTitle = '';
  selectedIndex: number;

  fieldPermission: any;

  checkAll = false;

  // tslint:disable:forin prefer-for-of
  async ngOnInit() {
    this.spinnerOverlayService.show();
    this.setUpMessage();
    this.setListSearch();

    await this.getInfo();

    await this.onChangeTemplate();
    this.spinnerOverlayService.hide();
  }

  getInfo() {
    return new Promise<void>(resolve => {
      this.tableService.getAllTable()
        .pipe(first())
        .subscribe(
          data => {
            this.listTable = data;
            for (let i = 0; i < this.listTable.length; i++) {
              this.listTable[i].index = i;
            }

            this.roleService.getAllRole()
              .pipe(first())
              .subscribe(
                data1 => {
                  this.listRole = data1;
                  for (let i = 0; i < this.listRole.length; i++) {
                    this.listRole[i].index = i;
                  }
                  resolve();
                });
          });
    });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  setListSearch() {
    this.listSearch.name = '';
    this.listSearch.table_name = '';
    this.listSearch.role_name = '';
  }

  setListSearchSelect() {
    this.listSearchSelect.name = '';
    this.listSearchSelect.type = '';
  }

  setListSearchTemplate() {
    this.listSearchTemplate.name = '';
    this.listSearchTemplate.type = '';
  }

  performFilter() {
    this.listFilteredTemplate = this.listTemplate.filter((field: any) =>
      field.name.toLocaleLowerCase().indexOf(this.listSearch.name.toLocaleLowerCase()) !== -1 &&
      this.listTable[field.table_index].name.toLocaleLowerCase().indexOf(this.listSearch.table_name.toLocaleLowerCase()) !== -1 &&
      this.listRole[field.role_index].name.toLocaleLowerCase().indexOf(this.listSearch.role_name.toLocaleLowerCase()) !== -1
    );
  }

  performFilterFieldSelect() {
    this.listFilteredFieldSelect = this.listFieldSelect.filter((field: any) =>
      field[1].field_name.toLocaleLowerCase().indexOf(this.listSearchSelect.name.toLocaleLowerCase()) !== -1 &&
      field[1].type.toLocaleLowerCase().indexOf(this.listSearchSelect.type.toLocaleLowerCase()) !== -1
    );
  }

  performFilterFieldTemplate() {
    this.listFilteredFieldTemplate = this.listFieldTemplate.filter((field: any) =>
      field[1].field_name.toLocaleLowerCase().indexOf(this.listSearchTemplate.name.toLocaleLowerCase()) !== -1 &&
      field[1].type.toLocaleLowerCase().indexOf(this.listSearchTemplate.type.toLocaleLowerCase()) !== -1
    );
    this.performFilterFieldSelect();
  }

  async onChangeTemplate() {
    this.spinnerOverlayService.show();
    this.listTemplate = [];
    if (this.active === 0) {
      await this.getAllImportTemplate();
    } else if (this.active === 1) {
      await this.getAllExportTemplate();
    }
    this.spinnerOverlayService.hide();
  }

  getAllImportTemplate(): Promise<void> {
    return new Promise<void>(resolve => {
      this.dataService.getAllImportTemplate()
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.listTemplate = data;
            this.getTableAndRoleName();
            this.listFilteredTemplate = this.listTemplate;
            resolve();
          });
    });
  }

  getAllExportTemplate(): Promise<void> {
    return new Promise<void>(resolve => {
      this.exportService.getAllExportTemplate()
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.listTemplate = data;
            this.getTableAndRoleName();
            this.listFilteredTemplate = this.listTemplate;
            resolve();
          });
    });
  }

  getTableAndRoleName() {
    for (let i = 0; i < this.listTemplate.length; i++) {
      for (let j = 0; j < this.listTable.length; j++) {
        if (this.listTemplate[i].table_id === this.listTable[j]._id) {
          this.listTemplate[i].table_index = j;
        }
      }

      for (let j = 0; j < this.listRole.length; j++) {
        if (this.listTemplate[i].role_id === this.listRole[j]._id) {
          this.listTemplate[i].role_index = j;
        }
      }
    }
  }

  onPopUp(content: any, modalBig: boolean) {
    if (modalBig) {
      this.modalReference = this.modalService.open(content, { windowClass : 'customModalClassBig'});
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    } else {
      this.modalReference = this.modalService.open(content);
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    }
  }

  async onPopUpTemplate(content: any, modalBig: boolean, id: number) {
    this.spinnerOverlayService.show();
    this.setListSearchSelect();
    this.setListSearchTemplate();
    this.popUpTemplate = this.utilService.deepCopy(this.listFilteredTemplate[id]);
    this.popUpTemplateId = id;

    this.selectedTable = this.listTable[this.popUpTemplate.table_index];
    this.selectedRole = this.listRole[this.popUpTemplate.role_index];

    await this.getFieldPermission();

    const listFieldName = [];

    for (let i = 0; i < this.fieldPermission.length; i++) {
      if (this.fieldPermission[i].field_permission.U) {
        listFieldName.push([this.fieldPermission[i].field_id, this.selectedTable.metadata[this.fieldPermission[i].field_id]]);
      }
    }

    if (listFieldName.length !== 0) {
      listFieldName.sort((a, b) => {
        return b[1].sort_id - a[1].sort_id;
      });
      listFieldName.reverse();
    }

    this.listFieldSelect = [];
    if (this.popUpTemplate.fields) {
      this.listFieldTemplate = this.popUpTemplate.fields;
    } else {
      this.listFieldTemplate = [];
    }
    for (let i = 0; i < listFieldName.length; i++) {
      let check = false;
      for (let j = 0; j < this.listFieldTemplate.length; j++) {
        if (listFieldName[i][0] === this.listFieldTemplate[j]) {
          this.listFieldTemplate[j] = listFieldName[i];
          check = true;
          break;
        }
      }
      if (!check) {
        this.listFieldSelect.push(listFieldName[i]);
      }
    }
    this.listFilteredFieldSelect = this.listFieldSelect;
    this.listFilteredFieldTemplate = this.listFieldTemplate;

    this.spinnerOverlayService.hide();
    this.onPopUp(content, modalBig);
  }

  onPopUpAddTemplate(content: any, modalBig: boolean) {
    this.popUpTemplate = {};
    this.setListSearchSelect();
    this.setListSearchTemplate();

    this.onPopUp(content, modalBig);
  }

  onPopUpDeleteTemplate(content: any, modalBig: boolean, id: number) {
    this.popUpTemplate = this.listFilteredTemplate[id];
    this.popUpTemplateId = id;

    this.onPopUp(content, modalBig);
  }

  onPopUpDropdownTable(content: any, modalBig: boolean, id: number, isListSelect: boolean) {
    this.selectedIndex = id;
    this.listDropdownValue = [];
    if (isListSelect) {
      if (this.listFilteredFieldSelect[id][1].values) {
        this.listDropdownValue = this.listFilteredFieldSelect[id][1].values;
      } else {
        this.listDropdownValue = [];
      }
      this.dropdownTitle = this.listFilteredFieldSelect[id][1].field_name;
    } else {
      if (this.utilService.checkValue(this.listFilteredFieldTemplate[id][1].values)) {
        this.listDropdownValue = this.listFilteredFieldTemplate[id][1].values;
      } else {
        this.listDropdownValue = [];
      }
      this.dropdownTitle = this.listFilteredFieldTemplate[id][1].field_name;
    }
    this.onPopUp(content, false);
  }

  trackBy(index: any, item: any) {
    return index;
  }

  async onChangeTableRole() {
    if (this.popUpTemplate.table_id && this.popUpTemplate.role_id) {
      this.spinnerOverlayService.show();

      for (let i = 0; i < this.listTable.length; i++) {
        if (this.popUpTemplate.table_id === this.listTable[i]._id) {
          this.selectedTable = this.listTable[i];
          this.popUpTemplate.table_index = i;
          break;
        }
      }

      for (let i = 0; i < this.listRole.length; i++) {
        if (this.popUpTemplate.role_id === this.listRole[i]._id) {
          this.selectedRole = this.listRole[i];
          this.popUpTemplate.role_index = i;
          break;
        }
      }

      await this.getFieldPermission();

      const listFieldName = [];

      for (let i = 0; i < this.fieldPermission.length; i++) {
        if (this.fieldPermission[i].field_permission.U) {
          listFieldName.push([this.fieldPermission[i].field_id, this.selectedTable.metadata[this.fieldPermission[i].field_id]]);
        }
      }

      if (listFieldName.length !== 0) {
        listFieldName.sort((a, b) => {
          return b[1].sort_id - a[1].sort_id;
        });
        listFieldName.reverse();
      }

      this.listFieldSelect = [];
      this.listFieldTemplate = [];
      for (let i = 0; i < listFieldName.length; i++) {
        this.listFieldSelect.push(listFieldName[i]);
      }
      this.listFilteredFieldSelect = this.listFieldSelect;
      this.listFilteredFieldTemplate = this.listFieldTemplate;

      this.spinnerOverlayService.hide();
    }
  }

  onAddFields() {
    for (let i = 0; i < this.listFieldSelect.length; i++) {
      if (this.listFieldSelect[i].checked) {
        this.listFieldSelect[i].checked = undefined;
        this.listFieldTemplate.push(this.listFieldSelect[i]);
        this.listFieldSelect.splice(i, 1);
        i--;
      }
    }
    this.performFilterFieldTemplate();
  }

  onDeleteField(id: any) {
    this.listFieldSelect.push(this.listFieldTemplate[id]);
    this.listFieldTemplate.splice(id, 1);
    this.performFilterFieldTemplate();
  }

  getFieldPermission(): Promise<void> {
    return new Promise<void>(resolve => {
      const json: any = {};
      json.table_id = this.popUpTemplate.table_id;
      json.role_id = this.popUpTemplate.role_id;
      this.fieldPermissionService.getFieldPermissionWithCondition(json)
        .pipe(first())
        .subscribe(
          data => {
            this.fieldPermission = data;
            resolve();
          });
    });
  }

  onCheckAll() {
    for (let i = 0; i < this.listFilteredFieldSelect.length; i++) {
      this.listFilteredFieldSelect[i].checked = this.checkAll;
    }
  }

  async onAddTemplate() {
    this.spinnerOverlayService.show();
    const json: any = {};
    json.name = this.popUpTemplate.name;
    json.table_id = this.popUpTemplate.table_id;
    json.role_id = this.popUpTemplate.role_id;
    json.fields = [];
    for (let i = 0; i < this.listFieldTemplate.length; i++) {
      json.fields.push(this.listFieldTemplate[i][0]);
    }

    // import template
    if (this.active === 0) {
      this.dataService.createImportTemplate(json)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.popUpTemplate._id = data.inserted_id;
            this.popUpTemplate.fields = json.fields;

            this.listTemplate.push(this.popUpTemplate);
            this.performFilter();

            this.spinnerOverlayService.hide();
            this.modalReference.close();
          });

      // export template
    } else if (this.active === 1) {
      if (this.popUpTemplate.use_template) {
        if (this.selectedFile) {
          await this.onUploadData();
        }
        json.use_template = true;
        json.file_template_id = this.popUpTemplate.file_template_id;
        json.file_template_name = this.popUpTemplate.file_template_name;
      } else {
        json.use_template = false;
      }
      this.exportService.addNewExportTemplate(json)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.popUpTemplate._id = data._id;
            this.popUpTemplate.fields = json.fields;

            this.listTemplate.push(this.popUpTemplate);
            this.performFilter();

            this.spinnerOverlayService.hide();
            this.modalReference.close();
          });
    }
  }

  async onSaveTemplate() {
    this.spinnerOverlayService.show();
    const json: any = {};
    json._id = this.popUpTemplate._id;
    json.name = this.popUpTemplate.name;
    json.table_id = this.popUpTemplate.table_id;
    json.role_id = this.popUpTemplate.role_id;
    json.fields = [];
    for (let i = 0; i < this.listFieldTemplate.length; i++) {
      json.fields.push(this.listFieldTemplate[i][0]);
    }
    // import template
    if (this.active === 0) {
      this.dataService.updateImportTemplate(json)
        .pipe(first())
        .subscribe(
          data => {
            this.listFilteredTemplate[this.popUpTemplateId].name = json.name;
            this.listFilteredTemplate[this.popUpTemplateId].fields = json.fields;
            this.spinnerOverlayService.hide();
          });
      // export template
    } else if (this.active === 1) {
      if (this.popUpTemplate.use_template) {
        if (this.selectedFile) {
          await this.onUploadData();
        }
        json.use_template = true;
        json.file_template_id = this.popUpTemplate.file_template_id;
        json.file_template_name = this.popUpTemplate.file_template_name;
      } else {
        json.use_template = false;
      }
      this.exportService.updateExportTemplate(json)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.listFilteredTemplate[this.popUpTemplateId].name = json.name;
            this.listFilteredTemplate[this.popUpTemplateId].fields = json.fields;
            this.spinnerOverlayService.hide();
          });
    }

  }

  onDeleteImportTemplate() {
    this.spinnerOverlayService.show();
    this.dataService.deleteImportTemplate(this.popUpTemplate._id)
      .pipe(first())
      .subscribe(
        async data => {
          await this.onChangeTemplate();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onDeleteExportTemplate() {
    this.spinnerOverlayService.show();
    if (this.popUpTemplate.file_template_id) {
      this.dataService.onDeleteFileData(this.popUpTemplate.file_template_id)
        .pipe(first())
        .subscribe(
          (data: any) => {
          });
    }
    this.exportService.deleteExportTemplate(this.popUpTemplate._id)
      .pipe(first())
      .subscribe(
        async data => {
          await this.onChangeTemplate();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.popUpTemplate.file_template_name = this.selectedFile.name;
  }

  onUploadData() {
    return new Promise<void>(resolve => {
      const uploadData = new FormData();
      uploadData.append('file', this.selectedFile, this.selectedFile.name);
      this.dataService.onUploadFileData(uploadData)
        .pipe(first())
        .subscribe(
          async (data: any) => {
            if (this.popUpTemplate.file_template_id) {
              this.dataService.onDeleteFileData(this.popUpTemplate.file_template_id)
                .pipe(first())
                .subscribe(
                  (data1: any) => {
                    resolve();
                  });

              this.popUpTemplate.file_template_id = data.file_id;
              this.selectedFile = null;
            } else {
              this.popUpTemplate.file_template_id = data.file_id;
              this.selectedFile = null;
              resolve();
            }
          });
    });
  }

  onDownloadData(fileId: any) {
    this.spinnerOverlayService.show();
    this.dataService.onDownloadFileData(fileId)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.spinnerOverlayService.hide();
        });
  }

  getListTemplate() {
    return this.listFilteredTemplate
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  getCurrentId(id: any) {
    return (id + (this.page - 1) * this.pageSize);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (this.listSearchTemplate.name !== '' || this.listSearchTemplate.type !== '') {
      return;
    }
    moveItemInArray(this.listFieldTemplate, event.previousIndex, event.currentIndex);
    this.performFilterFieldTemplate();
  }

  onSelectSortId(previousIndex: any, event: any) {
    moveItemInArray(this.listFieldTemplate, previousIndex, event.target.value - 1);
    this.performFilterFieldTemplate();
  }
}
