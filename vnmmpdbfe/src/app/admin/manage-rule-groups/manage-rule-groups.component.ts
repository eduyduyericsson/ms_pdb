import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UtilService} from '../../services/util.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';
import {Subject} from 'rxjs';
import {debounceTime, first} from 'rxjs/operators';
import {RuleService} from '../../services/rule.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-manage-rule-groups',
  templateUrl: './manage-rule-groups.component.html',
  styleUrls: ['./manage-rule-groups.component.scss']
})
export class ManageRuleGroupsComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private utilService: UtilService,
    private spinnerOverlayService: SpinnerOverlayService,
    private ruleService: RuleService
  ) { }

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  modalReference: any;

  listRuleGroups: any = [];
  listFilteredRuleGroups: any = [];

  newRuleGroups: any = {};
  popUpId: number;
  popUpRuleGroup: any;
  listSearch: any = {};
  page = 1;
  pageSize = 20;

  ngOnInit() {
    this.setUpMessage();
    this.setListSearch();
    this.ruleService.getAllRuleGroups()
      .pipe(first())
      .subscribe(
        data => {
          this.listRuleGroups = data;
          this.listRuleGroups.sort((a, b) => {
            return  b.sort_id - a.sort_id;
          });
          this.listRuleGroups.reverse();
          this.listFilteredRuleGroups = this.listRuleGroups;
        });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  setListSearch() {
    this.listSearch.name = '';
    this.listSearch.type = '';
  }

  drop(event: CdkDragDrop<string[]>) {
    if (this.listSearch.name !== '' || this.listSearch.type !== '') {
      return;
    }
    moveItemInArray(this.listRuleGroups, event.previousIndex, event.currentIndex);
  }

  onSelectSortId(previousIndex, event) {
    moveItemInArray(this.listRuleGroups, previousIndex, event.target.value - 1);
    this.performFilter();
  }

  onSaveSortId() {
    this.spinnerOverlayService.show();
    for (let i = 0; i < this.listRuleGroups.length; i++) {
      this.listRuleGroups[i].sort_id = i;
      const saveRuleGroup: any = {};
      saveRuleGroup._id = this.listRuleGroups[i]._id;
      saveRuleGroup.sort_id = i;
      this.ruleService.updateRuleGroup(saveRuleGroup)
        .pipe(first())
        .subscribe(
          data => {
            this.spinnerOverlayService.hide();
          });
    }
  }

  performFilter() {
    this.listFilteredRuleGroups = this.listRuleGroups.filter((field: any) =>
      field.name.toLocaleLowerCase().indexOf(this.listSearch.name.toLocaleLowerCase()) !== -1
    );
  }

  onPopUp(content: any) {
    this.modalReference = this.modalService.open(content);
    this.modalReference.result.then((result) => {
    }, (reason) => {
    });
  }

  onPopUpDataRuleGroup(content: any, id: any) {
    this.popUpRuleGroup = this.utilService.deepCopy(this.listFilteredRuleGroups[id]);
    this.popUpId = id;
    this.onPopUp(content);
  }

  onSaveAdd() {
    this.newRuleGroups = this.utilService.trimJson(this.newRuleGroups);
    if (!this.checkInformation(this.newRuleGroups, false)) {
      return;
    }
    this.spinnerOverlayService.show();
    this.ruleService.addNewRuleGroup(this.newRuleGroups)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.newRuleGroups._id = data._id;
          this.listRuleGroups.push(this.newRuleGroups);
          this.performFilter();
          this.newRuleGroups = {};
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onSaveEdit() {
    this.popUpRuleGroup = this.utilService.trimJson(this.popUpRuleGroup);
    if (!this.checkInformation(this.popUpRuleGroup, true)) {
      return;
    }
    this.spinnerOverlayService.show();
    this.ruleService.updateRuleGroup(this.popUpRuleGroup)
      .pipe(first())
      .subscribe(
        data => {
          const index = this.listRuleGroups.indexOf(this.listFilteredRuleGroups[this.popUpId]);
          this.listRuleGroups[index] = this.utilService.deepCopy(this.popUpRuleGroup);
          this.performFilter();
          this.spinnerOverlayService.hide();
        });
  }

  onDelete(id: any) {
    this.spinnerOverlayService.show();
    this.ruleService.deleteRuleGroup(this.listFilteredRuleGroups[id]._id)
      .pipe(first())
      .subscribe(
        data => {
          this.listRuleGroups = this.listRuleGroups.filter((field: any) =>
            field._id !== this.listFilteredRuleGroups[id]._id
          );
          this.performFilter();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  checkInformation(recordGroup: any, isEdit: boolean) {
    if (!this.utilService.checkValue(recordGroup.name)) {
      this.message.next('Record Group Name must not be blanked');
      return false;
    }
    return true;
  }

  getListRuleGroups() {
    return this.listFilteredRuleGroups
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  getCurrentId(id: any) {
    return (id + (this.page - 1) * this.pageSize);
  }
}
