import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRuleGroupsComponent } from './manage-rule-groups.component';

describe('ManageRuleGroupsComponent', () => {
  let component: ManageRuleGroupsComponent;
  let fixture: ComponentFixture<ManageRuleGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageRuleGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRuleGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
