import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRecordGroupsComponent } from './manage-record-groups.component';

describe('ManageRecordGroupsComponent', () => {
  let component: ManageRecordGroupsComponent;
  let fixture: ComponentFixture<ManageRecordGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageRecordGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRecordGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
