import {Component, OnInit} from '@angular/core';
import {CookieService} from './services/cookie.service';
import {UserService} from './services/user.service';
import {first} from 'rxjs/operators';
import {JWTTokenService} from './services/jwt-token.service';
import {SharedDataService} from './services/sharedData.service';
import {SpinnerOverlayService} from './services/spinner-overlay.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: 'nav-bar.html'
})
export class NavBarComponent implements OnInit {

  constructor(
    private router: Router,
    public cookieService: CookieService,
    private userService: UserService,
    public jwtTokenService: JWTTokenService,
    public sharedDataService: SharedDataService,
    private spinnerOverlayService: SpinnerOverlayService,
  ) { }

  isNavbarCollapsed = true;
  currentUser: any = {};
  isAdmin: boolean;

  ngOnInit() {
    this.spinnerOverlayService.show();
    if (this.cookieService.get('access_token')) {
      this.userService.getUser()
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.currentUser = data;
            let isAdmin = false;
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < this.currentUser.roles.length; i++) {
              if (this.currentUser.roles[i] === 'admin') {
                isAdmin = true;
                break;
              }
            }
            this.sharedDataService.setIsAdmin(isAdmin);
            this.isAdmin = this.sharedDataService.getIsAdmin();
            this.spinnerOverlayService.hide();
          });
    } else {
      this.spinnerOverlayService.hide();
    }
  }

  onLogout() {
    this.spinnerOverlayService.show();
    this.userService.logout()
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.cookieService.remove('access_token');
          this.cookieService.remove('refresh_token');
          window.location.reload();
          this.router.navigate(['/dashboard']);
        });
  }
}
