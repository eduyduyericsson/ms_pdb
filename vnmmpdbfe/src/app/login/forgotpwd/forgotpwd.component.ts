import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-forgotpwd',
  templateUrl: './forgotpwd.component.html',
  styleUrls: ['./forgotpwd.component.scss']
})
export class ForgotpwdComponent implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService,
  ) { }


  public check_email: any;
  public company_email: any;

  ngOnInit(): void {

  }
  forgotPwdForm = new FormGroup
    (
      {
      info_signnum: new FormControl(''),
      info_email: new FormControl(''),
      }
    );
  
  onForgotPwd() {
    this.check_email = this.forgotPwdForm.get('info_email').value
    this.company_email = this.check_email.slice(this.check_email.indexOf('@')+1)
    if (this.company_email == "ericsson.com")
      {
        this.userService
            .forgotPwd(this.forgotPwdForm.value)
            .subscribe(data => console.log(data));
      }
  }
  onBackLogin() {
    this.router.navigate(['../../login']);
  }

}
