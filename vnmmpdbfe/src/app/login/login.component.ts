import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {debounceTime, first} from 'rxjs/operators';
import {UserService} from '../services/user.service';
import {CookieService} from '../services/cookie.service';
import {JWTTokenService} from '../services/jwt-token.service';
import {Router} from '@angular/router';
import {SharedDataService} from '../services/sharedData.service';
import {SpinnerOverlayService} from "../services/spinner-overlay.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private sharedDataService: SharedDataService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private cookieService: CookieService,
    private jwtTokenService: JWTTokenService,
    private spinnerOverlayService: SpinnerOverlayService,
  ) { }

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  loginForm: FormGroup;
  account: any = {};

  ngOnInit() {
    if (this.cookieService.get('access_token')) {
      this.router.navigate(['/dashboard']);
    }
    this.setUpMessage();
    this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
      });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  onLogin() {
    this.account.username = this.loginForm.get('username').value;
    this.account.password = this.loginForm.get('password').value;
    this.userService.login(this.account)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.cookieService.set('access_token', data.access_token);
          this.cookieService.set('refresh_token', data.refresh_token);
          const roles = data.info.roles;
          let isAdmin = false;
          // tslint:disable:prefer-for-of
          for (let i = 0; i < roles.length; i++) {
            if (roles[i] === 'admin') {
              isAdmin = true;
              break;
            }
          }
          this.sharedDataService.setIsAdmin(isAdmin);
          window.location.reload();
        });
  }
}
