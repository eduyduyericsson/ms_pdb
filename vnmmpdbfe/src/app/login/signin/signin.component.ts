import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService,
  ) { }

  ngOnInit(): void {

  }

  registerForm= new FormGroup 
    (
      {
      info_signnum: new FormControl(''),
      info_email: new FormControl(''),
      info_pwd: new FormControl(''),
      info_pwd_repeat: new FormControl(''),
      }
    );

  onRegister() {
      this.userService
      .register(this.registerForm.value)
      .subscribe(data => console.log(data))
  }
  onBackLogin() {
    this.router.navigate(['../../login']);
  }

}
