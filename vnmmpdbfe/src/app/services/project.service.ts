import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({ providedIn: 'root' })
export class ProjectService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl + '/project';

  getAllProject() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getProjectWithId(id: any) {
    return this.http.get(this.baseUrl + `/get/${id}`);
  }

  addNewProject(json: any) {
    return this.http.post(this.baseUrl + '/add', json);
  }

  updateProject(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  deleteProject(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }
}
