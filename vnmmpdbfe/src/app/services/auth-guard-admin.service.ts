import { Injectable } from '@angular/core';
import {CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import {CookieService} from './cookie.service';
import {JWTTokenService} from './jwt-token.service';
import {AuthGuardService} from './auth-guard.service';
import {SharedDataService} from './sharedData.service';
import {delay, first, tap} from 'rxjs/operators';
import {UserService} from './user.service';
import {SpinnerOverlayService} from './spinner-overlay.service';

@Injectable()
export class AuthGuardAdminService implements CanActivate {

  constructor(
    private router: Router,
    private cookieService: CookieService,
    private jwtTokenService: JWTTokenService,
    private authGuardService: AuthGuardService,
    private sharedDataService: SharedDataService,
    private userService: UserService,
    private spinnerOverlayService: SpinnerOverlayService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return this.authGuardService.canActivate(route, state).then((auth: boolean) => {
      this.spinnerOverlayService.show();
      if (!auth) {
        this.spinnerOverlayService.hide();
        return false;
      } else {
        return new Promise<boolean>(resolve => {
          this.userService.getUser()
            .pipe(first())
            .subscribe(
              (data: any) => {
                let isAdmin = false;
                // tslint:disable-next-line:prefer-for-of
                for (let i = 0; i < data.roles.length; i++) {
                  if (data.roles[i] === 'admin') {
                    isAdmin = true;
                    break;
                  }
                }
                if (isAdmin) {
                  this.spinnerOverlayService.hide();
                  return resolve(true);
                } else {
                  this.router.navigate(['/dashboard']);
                  this.spinnerOverlayService.hide();
                  return resolve(false);
                }
              });
        });
      }
    });
  }
}
