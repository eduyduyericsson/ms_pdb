import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class SharedDataService {
  isAdmin: boolean;

  constructor() { }

  getIsAdmin() {
    return this.isAdmin;
  }

  setIsAdmin(value: any) {
    this.isAdmin = value;
  }
}
