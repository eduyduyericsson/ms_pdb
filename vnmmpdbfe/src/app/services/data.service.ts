import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';
import {tap} from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class DataService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl + '/data';

  getAllData() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getDataWithId(json: any) {
    return this.http.post(this.baseUrl + '/get', json);
  }

  getDataFromTable(json: any) {
    return this.http.post(this.baseUrl + '/get-all-from-table', json);
  }

  getDataWithCondition(json: any) {
    return this.http.post(this.baseUrl + '/facet-search', json);
  }

  getDataFromForm(json: any) {
    return this.http.post(this.baseUrl + '/get-form-data', json);
  }

  addNewData(json: any) {
    return this.http.post(this.baseUrl + '/add', json);
  }

  updateData(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  updateManyData(json: any) {
    return this.http.put(this.baseUrl + '/update-many', json);
  }

  deleteData(json: any) {
    return this.http.post(this.baseUrl + '/delete', json);
  }

  deleteManyData(json: any) {
    return this.http.post(this.baseUrl + '/delete-many', json);
  }

  downloadImportTemplate(json: any) {
    return this.http.post(this.baseUrl + '/download-import-template', json, { responseType: 'blob'})
      .pipe(tap((data: any) => {
        const blob = new Blob([data], { type: 'application/octet-stream' });
        const downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(blob);
        downloadLink.download = 'import-template.xlsx';

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      }));
  }

  onExportAll(json: any) {
    return this.http.post(this.baseUrl + '/export', json, { responseType: 'blob'})
      .pipe(tap((data: any) => {
        const blob = new Blob([data], { type: 'application/octet-stream' });
        const downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(blob);
        downloadLink.download = 'export-all.xlsx';

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      }));
  }

  onExportSelected(json: any) {
    return this.http.post(this.baseUrl + '/download-many', json, { responseType: 'blob'})
      .pipe(tap((data: any) => {
        const blob = new Blob([data], { type: 'application/octet-stream' });
        const downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(blob);
        downloadLink.download = 'export-selected.xlsx';

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      }));
  }

  getAllImportTemplate() {
    return this.http.get(this.baseUrl + '/get-all-import-template');
  }

  getImportTemplateWithId(id: any) {
    return this.http.get(this.baseUrl + `/get-import-template/${id}`);
  }

  searchImportTemplate(json: any) {
    return this.http.post(this.baseUrl + '/search-import-template', json);
  }

  createImportTemplate(json: any) {
    return this.http.post(this.baseUrl + '/create-import-template', json);
  }

  updateImportTemplate(json: any) {
    return this.http.post(this.baseUrl + '/update-import-template', json);
  }

  deleteImportTemplate(id: any) {
    return this.http.delete(this.baseUrl + `/delete-import-template/${id}`);
  }

  downloadNewImportTemplate(id: any, fileName: any) {
    return this.http.get(this.baseUrl + `/download-import-template-new/${id}`, { responseType: 'blob'})
      .pipe(tap((data: any) => {
        const blob = new Blob([data], { type: 'application/octet-stream' });
        const downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(blob);
        downloadLink.download = fileName + '.xlsx';

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      }));
  }

  getAllLog() {
    return this.http.get(this.baseUrl + '/get-all-log');
  }

  downloadDeleteTemplate(json: any) {
    return this.http.post(this.baseUrl + '/download-delete-template', json, { responseType: 'blob'})
      .pipe(tap((data: any) => {
        const blob = new Blob([data], { type: 'application/octet-stream' });
        const downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(blob);
        downloadLink.download = 'delete-template.xlsx';

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      }));
  }

  onBulkDeleteData(file: FormData, tableId: any) {
    const json = {
      table_id: tableId,
    };

    const blobOverrides = new Blob([JSON.stringify(json)], {
      type: 'application/json',
    });

    file.append('json', blobOverrides);
    return this.http.post(this.baseUrl + '/bulk-delete-by-xlsx', file);
  }

  onDownloadFileData(id: any) {
    return this.http.get(this.baseUrl + `/download-file/${id}`, { responseType: 'blob', observe: 'response'})
      .pipe(tap((data: any) => {
        const blob = new Blob([data.body], { type: data.headers.get('Content-type') });
        const contentDisposition = data.headers.get('content-disposition');
        const fileName = contentDisposition.substring(12, contentDisposition.length);
        const downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(blob);
        downloadLink.download = fileName;

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      }));
  }

  onUploadData(file: FormData, tableId: any) {
    const json = {
      table_id: tableId,
    };

    const blobOverrides = new Blob([JSON.stringify(json)], {
      type: 'application/json',
    });

    file.append('json', blobOverrides);
    return this.http.post(this.baseUrl + '/bulk-upload-by-xlsx', file);
  }

  onUpdateData(file: FormData, tableId: any, skipBlank) {
    const json = {
      table_id: tableId,
      skip_blank: skipBlank
    };

    const blobOverrides = new Blob([JSON.stringify(json)], {
      type: 'application/json',
    });

    file.append('json', blobOverrides);
    return this.http.post(this.baseUrl + '/bulk-update-by-xlsx', file);
  }

  onDeleteData(file: FormData, tableId: any) {
    const json = {
      table_id: tableId,
    };

    const blobOverrides = new Blob([JSON.stringify(json)], {
      type: 'application/json',
    });

    file.append('json', blobOverrides);
    return this.http.post(this.baseUrl + '/bulk-delete-by-xlsx', file);
  }

  onUploadFileData(file: FormData) {
    return this.http.post(this.baseUrl + '/upload-file', file);
  }

  onDeleteFileData(id: any) {
    return this.http.delete(this.baseUrl + `/delete-file/${id}`);
  }
}
