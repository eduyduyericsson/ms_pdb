import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';
import {tap} from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ExportService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl + '/export';

  getAllExportTemplate() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getExportTemplateWithCondition(json: any) {
    return this.http.post(this.baseUrl + '/facet-search', json);
  }

  getExportTemplateWithId(id: any) {
    return this.http.get(this.baseUrl + `/get/${id}`);
  }

  addNewExportTemplate(json: any) {
    return this.http.post(this.baseUrl + '/add', json);
  }

  updateExportTemplate(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  deleteExportTemplate(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }

  runExportTemplate(json: any) {
    return this.http.post(this.baseUrl + '/run', json, { responseType: 'blob', observe: 'response'})
      .pipe(tap((data: any) => {
        const blob = new Blob([data.body], { type: data.headers.get('Content-type') });
        const contentDisposition = data.headers.get('content-disposition');
        const fileName = contentDisposition.substring(21, contentDisposition.length);
        const downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(blob);
        downloadLink.download = fileName;

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      }));
  }
}
