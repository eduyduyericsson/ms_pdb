import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';

import { first, catchError, switchMap, filter, take } from 'rxjs/operators';
import {CookieService} from './cookie.service';
import {SpinnerOverlayService} from './spinner-overlay.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
    private cookieService: CookieService,
    private spinnerOverlayService: SpinnerOverlayService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.cookieService.get('access_token')) {
      request = this.addToken(request, this.cookieService.get('access_token'));
    }

    return next.handle(request)
      .pipe(catchError((error: HttpErrorResponse) => {
          console.log(error);
          alert(error.error.msg);
          this.spinnerOverlayService.hide();
          return throwError(error);
      }));
  }

  private addToken(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`
      }
    });
  }
}
