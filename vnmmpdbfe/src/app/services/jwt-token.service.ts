import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class JWTTokenService {

  decodedToken: { [key: string]: string };

  constructor() {
  }

  decodeToken(jwtToken: any) {
    if (jwtToken) {
      this.decodedToken = jwt_decode(jwtToken);
    }
  }

  getDecodeToken(jwtToken: any) {
    return jwt_decode(jwtToken);
  }

  getUser(jwtToken: any) {
    this.decodeToken(jwtToken);
    return this.decodedToken ? this.decodedToken.displayname : null;
  }

  getEmailId(jwtToken: any) {
    this.decodeToken(jwtToken);
    return this.decodedToken ? this.decodedToken.email : null;
  }

  getExpiryTime(jwtToken: any) {
    this.decodeToken(jwtToken);
    return this.decodedToken ? this.decodedToken.exp : null;
  }

  isTokenExpired(jwtToken: any): boolean {
    // tslint:disable-next-line:radix
    const expiryTime: number = parseInt(this.getExpiryTime(jwtToken));
    if (expiryTime) {
      return ((1000 * expiryTime) - (new Date()).getTime()) < 5000;
    } else {
      return false;
    }
  }
}
