import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({ providedIn: 'root' })
export class FormService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl + '/form';

  getAllForm() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getFormWithId(id: any) {
    return this.http.get(this.baseUrl + `/get/${id}`);
  }

  getFormWithCondition(json: any) {
    return this.http.post(this.baseUrl + '/facet-search', json);
  }

  addNewForm(json: any) {
    return this.http.post(this.baseUrl + '/add', json);
  }

  updateForm(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  deleteForm(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }
}
