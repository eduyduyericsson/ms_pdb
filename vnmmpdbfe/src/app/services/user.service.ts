import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class UserService {

  constructor(private http: HttpClient) {}
  baseUrl = environment.baseUrl + '/user';

  getAllUser() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getUser() {
    return this.http.get(this.baseUrl + '/get');
  }

  register(json: any) {
    return this.http.post(this.baseUrl + '/register', json);
  }

  forgotPwd(json: any) {
    return this.http.post(this.baseUrl + '/forgotpwd', json);
  }

  update(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  login(json: any) {
    return this.http.post(this.baseUrl + '/login', json);
  }

  logout() {
    return this.http.get(this.baseUrl + '/logout');
  }

  delete(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
