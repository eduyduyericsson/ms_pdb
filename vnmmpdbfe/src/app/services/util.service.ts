import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class UtilService {

  constructor() { }

  checkValue(value: any) {
    if (value === undefined || value === null || value === '') {
      return false;
    }
    return true;
  }

  checkEmail(search: string): boolean {
    const regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return regexp.test(search);
  }

  trimJson(json: any) {
    return JSON.parse(JSON.stringify(json).replace(/"\s+|\s+"/g, '"'));
  }

  naturalCompare(a, b) {
    const ax = [];
    const bx = [];

    // tslint:disable:only-arrow-functions
    a.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { ax.push([$1 || Infinity, $2 || '']); });
    b.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { bx.push([$1 || Infinity, $2 || '']); });

    while (ax.length && bx.length) {
      const an = ax.shift();
      const bn = bx.shift();
      const nn = (bn[0] - an[0]) || bn[1].localeCompare(an[1]);
      if (nn) {
        return nn;
      }
    }
    return bx.length - ax.length;
  }

  sortStringAsc(list: any, name: string) {
    list.sort((a, b) => {
      return this.naturalCompare(b[name].toLowerCase(), a[name].toLowerCase());
    });
  }

  sortStringDesc(list: any, name: string) {
    list.sort((a, b) => {
      return this.naturalCompare(a[name].toLowerCase(), b[name].toLowerCase());
    });
  }

  sortAsc(list: any, fieldName: string) {
    list.sort((a, b) => {
      return  a[fieldName] - b[fieldName];
    });
  }

  sortDesc(list: any, fieldName: string) {
    list.sort(function(a, b) {
      return b[fieldName] - a[fieldName];
    });
  }

  deepCopy(obj: any): any {
    let clonedObject;

    if (obj === null || typeof obj !== 'object') {
      return obj;
    }
    if (obj instanceof Array) {
      clonedObject = [];
      for (let i = 0; i < obj.length; i++) {
        clonedObject[i] = this.deepCopy(obj[i]);
      }
      return clonedObject;
    }
    if (obj instanceof Date) {
      clonedObject = new Date(obj.valueOf());
      return clonedObject;
    }
    if (obj instanceof RegExp) {
      clonedObject = RegExp(obj.source, obj.flags);
      return clonedObject;
    }
    if (obj instanceof Object) {
      clonedObject = new obj.constructor();
      for (const attr in obj) {
        if (obj.hasOwnProperty(attr)) {
          clonedObject[attr] = this.deepCopy(obj[attr]);
        }
      }
      return clonedObject;
    }
  }
}
