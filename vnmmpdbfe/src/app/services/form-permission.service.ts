import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({ providedIn: 'root' })
export class FormPermissionService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl + '/form-permission';

  getAllFormPermission() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getFormPermissionWithId(id: any) {
    return this.http.get(this.baseUrl + `/get/${id}`);
  }

  getFormPermissionWithCondition(json: any) {
    return this.http.post(this.baseUrl + '/facet-search', json);
  }

  addNewFormPermission(json: any) {
    return this.http.post(this.baseUrl + '/add', json);
  }

  updateFormPermission(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  updateListFormPermission(listJson: any) {
    return this.http.put(this.baseUrl + '/upsert-many', listJson);
  }

  deleteFormPermission(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }
}
