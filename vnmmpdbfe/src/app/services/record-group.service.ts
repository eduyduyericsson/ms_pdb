import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({ providedIn: 'root' })
export class RecordGroupService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl + '/record-group';

  getAllRecordGroup() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getRecordGroupWithId(id: any) {
    return this.http.get(this.baseUrl + `/get/${id}`);
  }

  getRecordGroupWithCondition(json: any) {
    return this.http.post(this.baseUrl + '/facet-search', json);
  }

  addNewRecordGroup(json: any) {
    return this.http.post(this.baseUrl + '/add', json);
  }

  updateRecordGroup(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  deleteRecordGroup(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }
}
