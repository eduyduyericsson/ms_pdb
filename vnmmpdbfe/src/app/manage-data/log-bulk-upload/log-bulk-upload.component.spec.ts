import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogBulkUploadComponent } from './log-bulk-upload.component';

describe('LogBulkUploadComponent', () => {
  let component: LogBulkUploadComponent;
  let fixture: ComponentFixture<LogBulkUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogBulkUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogBulkUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
