import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UtilService} from '../../services/util.service';
import {Subject} from 'rxjs';
import {debounceTime, first} from 'rxjs/operators';
import {DataService} from '../../services/data.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';

@Component({
  selector: 'app-log-bulk-upload',
  templateUrl: './log-bulk-upload.component.html',
  styleUrls: ['./log-bulk-upload.component.scss']
})
export class LogBulkUploadComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private dataService: DataService,
    private utilService: UtilService,
    private spinnerOverlayService: SpinnerOverlayService
  ) { }

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  modalReference: any;
  page = 1;
  pageSize = 5;

  listData: any = [];
  listFilteredData: any = [];

  popUpData: any;
  popUpId: number;

  listSearch: any = {};

  ngOnInit() {
    this.setUpMessage();
    this.setListSearch();

    this.dataService.getAllLog()
      .pipe(first())
      .subscribe(
        data => {
          this.listData = data;
          this.utilService.sortStringDesc(this.listData, 'time');
          this.listFilteredData = this.listData;
        });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  setListSearch() {
    this.listSearch.username = '';
    this.listSearch.name = '';
  }

  performFilter() {
    this.listFilteredData = this.listData.filter((field: any) =>
      field.username.toLocaleLowerCase().indexOf(this.listSearch.username.toLocaleLowerCase()) !== -1 &&
      field.name.toLocaleLowerCase().indexOf(this.listSearch.name.toLocaleLowerCase()) !== -1
    );
  }

  onPopUp(content: any, modalBig: boolean) {
    if (modalBig) {
      this.modalReference = this.modalService.open(content, { windowClass : 'customModalClassBig'});
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    } else {
      this.modalReference = this.modalService.open(content);
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    }
  }

  onPopUpResult(content: any, modalBig: any, id: any) {
    this.popUpData = this.listFilteredData[id];
    this.popUpId = id;
    this.onPopUp(content, modalBig);
  }

  onDownloadData(fileId: any) {
    this.spinnerOverlayService.show();
    this.dataService.onDownloadFileData(fileId)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.spinnerOverlayService.hide();
        });
  }

  getListProject() {
    return this.listFilteredData
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  getCurrentId(id: any) {
    return (id + (this.page - 1) * this.pageSize);
  }
}
