import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NgbModal, NgbPanelChangeEvent} from '@ng-bootstrap/ng-bootstrap';
import {ProjectService} from '../../services/project.service';
import {DataService} from '../../services/data.service';
import {UtilService} from '../../services/util.service';
import {BehaviorSubject, Subject} from 'rxjs';
import {debounceTime, first} from 'rxjs/operators';
import {JWTTokenService} from '../../services/jwt-token.service';
import {CookieService} from '../../services/cookie.service';
import {FieldPermissionService} from '../../services/field-permission.service';
import {RecordGroupPermissionService} from '../../services/record-group-permission.service';
import {RecordGroupService} from '../../services/record-group.service';
import {RoleService} from '../../services/role.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';
import {FormService} from '../../services/form.service';
import {TableService} from '../../services/table.service';
import { DatePipe } from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {SharedDataService} from '../../services/sharedData.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private projectService: ProjectService,
    private dataService: DataService,
    private utilService: UtilService,
    private jwtTokenService: JWTTokenService,
    private cookieService: CookieService,
    private fieldPermissionService: FieldPermissionService,
    private recordGroupPermissionService: RecordGroupPermissionService,
    private recordGroupService: RecordGroupService,
    private roleService: RoleService,
    private spinnerOverlayService: SpinnerOverlayService,
    private formService: FormService,
    private tableService: TableService,
    private datePipe: DatePipe,
    private sharedDataService: SharedDataService
  ) { }
  @ViewChild('importBtn', {static: false}) importBtn: ElementRef;
  @ViewChild('deleteBtn', {static: false}) deleteBtn: ElementRef;
  selectedFile: File;

  listOperatorText = [
    'Equal to',
    'Not equal to',
    'Start with',
    'Not start with',
    'Contains',
    'Not contains'
  ];

  listOperatorNumber = [
    'Equal to',
    'Not equal to',
    'Greater than',
    'Smaller than',
    'Greater than or equal to',
    'Less than or equal to'
  ];

  listOperatorDropdown = [
    'Equal to',
    'Not equal to',
  ];

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';

  page = 1;
  pageSize = 5;
  reverse = false;
  propertyName: string;
  active = 0;

  pageDetail = 1;

  totalWidth = 0;
  totalWidthMaster = 0;
  totalWidthDetail = 0;

  colNumFilter = 5;
  rowNumFilter = 0;

  modalReference: any;
  selectedProjectId: any;
  selectedForm: any;

  selectedTable: any;
  selectedMasterTable: any;
  selectedDetailTable: any;

  checkAll = false;
  listForm: any;
  listFieldName: any = [];

  listDataRelation: any = [];
  popUpData: any = {};
  popUpDataId: number;
  listRgPermission: any = [];
  listFieldPermission: any = [];

  // Master Detail View
  listDataRelationDetail: any = [];
  listFieldNameDetail: any = [];
  listFieldNameDetailSection: any = [];
  popUpAddDataDetail: any;
  popUpDataDetail: any;

  // Data Filter
  listFilter: any = [];

  // Column Filter
  listFilterColumn: any = [];
  listFilterCheck: any = [];
  listShowField: any = [];

  // Search
  listSearch: any = {};

  listData: any = [];
  listFilteredData: any = [];
  listDataChanged: any = [];
  listDataChangedArray: any = {};

  // Section
  listFieldNameSection: any = [];

  public beforeChange($event: NgbPanelChangeEvent) {
    if ($event.panelId === 'static-1') {
      $event.preventDefault();
    }
  }

  // tslint:disable:prefer-for-of forin
  async ngOnInit() {
    this.spinnerOverlayService.show();
    this.setUpMessage();

    this.selectedProjectId = this.route.snapshot.paramMap.get('id');
    await this.updateListFormName();
    if (this.listForm.length === 0) {
      this.spinnerOverlayService.hide();
      return;
    }
    await this.onChangeForm();
    this.spinnerOverlayService.hide();
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  sortBy(name: string) {
    this.propertyName = name;
    this.reverse = !this.reverse;
    if (this.reverse) {
      this.listFilteredData = this.listFilteredData.sort((a, b) => {
        return this.utilService.naturalCompare(b[name], a[name]);
      });
    } else {
      this.listFilteredData = this.listFilteredData.sort((a, b) => {
        return this.utilService.naturalCompare(a[name], b[name]);
      });
    }
  }

  onChangeData(id: any) {
    this.listDataChangedArray[this.listFilteredData[id]._id] = this.listFilteredData[id];
  }

  onSaveMultiData() {
    this.listDataChanged = [];
    for (const a in this.listDataChangedArray) {
      this.listDataChanged.push(this.listDataChangedArray[a]);
    }
    if (this.listDataChanged.length !== 0) {
      this.listDataChanged = this.utilService.trimJson(this.listDataChanged);
      for (let i = 0; i < this.listDataChanged.length; i++) {
        for (let j = 0; j < this.listFieldName.length; j++) {
          if (this.listDataChanged[i][this.listFieldName[j][0]] === '') {
            this.listDataChanged[i][this.listFieldName[j][0]] = null;
          }
          if (this.selectedForm.metadata[this.listFieldName[j][0]].mandatory) {
            if (!this.listDataChanged[i][this.listFieldName[j][0]]) {
              alert('You have to fill all the required information (*)');
              return;
            }
          }
        }
      }

      this.spinnerOverlayService.show();
      const listSaveData: any = {};
      listSaveData.data = this.utilService.deepCopy(this.listDataChanged);
      listSaveData.table_id = this.selectedTable._id;
      delete listSaveData.data.record_group;

      for (let i = 0; i < this.listDataChanged.length; i++) {
        delete listSaveData.data[i].checked;
        for (let j = 0; j < this.listFieldName.length; j++) {
          // convert date to format
          if (this.selectedTable.metadata[this.listFieldName[j][0]].type === 'Date') {
            listSaveData.data[i][this.listFieldName[j][0]] = this.datePipe.transform(this.listDataChanged[i][this.listFieldName[j][0]], 'yyyy-MM-dd');
          } else if (this.selectedTable.metadata[this.listFieldName[j][0]].type === 'Datetime') {
            listSaveData.data[i][this.listFieldName[j][0]] = this.datePipe.transform(this.listDataChanged[i][this.listFieldName[j][0]], 'yyyy-MM-dd HH:mm:ss');
          }
        }
      }

      this.dataService.updateManyData(listSaveData)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.getDataWithConditionMulti(this.listDataChanged, this.listDataChangedArray);
            this.listDataChanged = [];
            this.listDataChangedArray = {};
            this.spinnerOverlayService.hide();
          },
          error => {
            this.getDataWithConditionMulti(this.listDataChanged, this.listDataChangedArray);
            this.listDataChanged = [];
            this.listDataChangedArray = {};
          });
    } else {
      alert(`You haven't changed any data yet`);
    }
  }

  getDataWithConditionMulti(listDataChanged, listDataChangedArray) {
    const json: any = {};
    json.table_id = this.selectedTable._id;
    json.condition = [];

    const condition: any = {};
    condition.field_type = 'object_id';
    condition.field_name = '_id';
    condition.operator = 'In';
    condition.value = [];
    for (let i = 0; i < listDataChanged.length; i++) {
      condition.value.push(listDataChanged[i]._id);
    }
    json.condition.push(condition);
    setTimeout(() => {

      this.dataService.getDataWithCondition(json)
        .pipe(first())
        .subscribe(
          (data: any) => {
            for (let i = 0; i < data.length; i++) {

              for (let j = 0; j < this.listFieldName.length; j++) {
                if (this.listFieldName[j][3].type === 'Datetime') {
                  if (data[i][this.listFieldName[j][0]]) {
                    data[i][this.listFieldName[j][0]] = new Date(data[i][this.listFieldName[j][0]]);
                  }
                }
              }

              const index = this.listData.indexOf(listDataChangedArray[data[i]._id]);
              this.listData[index] = data[i];
              delete listDataChangedArray[data[i]._id];
            }
            if (Object.keys(listDataChangedArray).length !== 0) {
              for (const a in listDataChangedArray) {
                this.listData = this.listData.filter((field: any) =>
                  field._id !== a
                );
              }
            }
            this.listFilteredData = this.listData.filter((field: any) => this.Fil(field));
          });

    }, 500);
  }

  // save multi record view
  async onSaveData() {
    this.spinnerOverlayService.show();
    for (let i = 0; i < this.listFieldName.length; i++) {
      if (this.listFieldName[i][3].type === 'Text') {
        if (this.popUpData[this.listFieldName[i][0]]) {
          this.popUpData[this.listFieldName[i][0]] = this.popUpData[this.listFieldName[i][0]].trim();
          if (this.popUpData[this.listFieldName[i][0]] === '') {
            this.popUpData[this.listFieldName[i][0]] = null;
          }
        }
      }
      if (this.selectedForm.metadata[this.listFieldName[i][0]].mandatory) {
        if (!this.popUpData[this.listFieldName[i][0]]) {
          this.spinnerOverlayService.hide();
          alert('You have to fill all the required information (*)');
          return;
        }
      }
    }

    for (let i = 0; i < this.listFieldName.length; i++) {
      // upload file data
      if (this.selectedTable.metadata[this.listFieldName[i][0]].type === 'File' && this.popUpData[this.listFieldName[i][0]]) {
        if (this.popUpData[this.listFieldName[i][0]].file) {
          await this.onUploadFileData(i);
        }
      }
    }
    const saveData: any = {};
    saveData.data = this.utilService.deepCopy(this.popUpData);
    saveData.table_id = this.selectedTable._id;
    delete saveData.data.record_group;
    delete saveData.data.checked;

    for (let i = 0; i < this.listFieldName.length; i++) {
      // convert date to format
      if (this.selectedTable.metadata[this.listFieldName[i][0]].type === 'Date') {
        saveData.data[this.listFieldName[i][0]] = this.datePipe.transform(this.popUpData[this.listFieldName[i][0]], 'yyyy-MM-dd');
      } else if (this.selectedTable.metadata[this.listFieldName[i][0]].type === 'Datetime') {
        saveData.data[this.listFieldName[i][0]] = this.datePipe.transform(this.popUpData[this.listFieldName[i][0]], 'yyyy-MM-dd HH:mm:ss');
      }
    }

    if (!this.popUpData._id) {
      this.dataService.addNewData(saveData)
        .pipe(first())
        .subscribe(
          async (data: any) => {
            await this.getDataWithId(data._id, this.selectedTable._id, false);
            this.spinnerOverlayService.hide();
            this.modalReference.close();
          });
    } else {
      saveData._id = this.popUpData._id;
      delete saveData.data._id;
      this.dataService.updateData(saveData)
        .pipe(first())
        .subscribe(
          async data => {
            await this.getDataWithId(this.popUpData._id, this.selectedTable._id, true);
            delete this.listDataChangedArray[this.popUpData._id];
            this.spinnerOverlayService.hide();
          });
    }
  }

  getDataWithId(id: number, tableId: any, isEdit: boolean) {
    return new Promise<void>(resolve => {
      const json: any = {};
      json.table_id = tableId;
      json._id = id;
      setTimeout(() => {

        this.dataService.getDataWithId(json)
          .pipe(first())
          .subscribe(
            (data: any) => {
              if (data) {
                if (isEdit) {
                  if (Object.keys(data).length !== 0) {
                    for (let i = 0; i < this.listFieldName.length; i++) {
                      if (this.listFieldName[i][3].type === 'Datetime') {
                        if (data[this.listFieldName[i][0]]) {
                          data[this.listFieldName[i][0]] = new Date(data[this.listFieldName[i][0]]);
                        }
                      }
                    }
                    this.listFilteredData[this.popUpDataId] = data;
                    this.popUpData = this.utilService.deepCopy(data);
                  } else {
                    this.listData = this.listData.filter((field: any) =>
                      field._id !== this.popUpData._id
                    );
                    this.listFilteredData = this.listData.filter((field: any) => this.Fil(field));
                    this.modalReference.close();
                  }
                } else {
                  if (Object.keys(data).length !== 0) {
                    for (let i = 0; i < this.listFieldName.length; i++) {
                      if (this.listFieldName[i][3].type === 'Datetime') {
                        if (data[this.listFieldName[i][0]]) {
                          data[this.listFieldName[i][0]] = new Date(data[this.listFieldName[i][0]]);
                        }
                      }
                    }
                    this.listData.push(data);
                    this.listFilteredData = this.listData.filter((field: any) => this.Fil(field));
                  }
                }
              }
              resolve();
            });

      }, 200);
    });
  }

  onUploadFileData(i: any): Promise<void> {
    return new Promise<void>(resolve => {
      const uploadData = new FormData();
      uploadData.append('file', this.popUpData[this.listFieldName[i][0]].file, this.popUpData[this.listFieldName[i][0]].file.name);
      this.dataService.onUploadFileData(uploadData)
        .pipe(first())
        .subscribe(
          (data: any) => {
            if (this.popUpData[this.listFieldName[i][0]].file_id) {
              this.dataService.onDeleteFileData(this.popUpData[this.listFieldName[i][0]].file_id)
                .pipe(first())
                .subscribe(
                  (data1: any) => {
                  });
            }
            this.popUpData[this.listFieldName[i][0]] = {
              file_id: data.file_id,
              file_name: data.file_name
            };
            resolve();
          });
    });
  }

  onUploadFileDataDetail(i: any): Promise<void> {
    return new Promise<void>(resolve => {
      const uploadData = new FormData();
      uploadData.append('file', this.popUpDataDetail[this.listFieldName[i][0]].file, this.popUpData[this.listFieldName[i][0]].file.name);
      this.dataService.onUploadFileData(uploadData)
        .pipe(first())
        .subscribe(
          (data: any) => {
            if (this.popUpData[this.listFieldName[i][0]].file_id) {
              this.dataService.onDeleteFileData(this.popUpData[this.listFieldName[i][0]].file_id)
                .pipe(first())
                .subscribe(
                  (data1: any) => {
                  });
            }
            this.popUpData[this.listFieldName[i][0]] = {
              file_id: data.file_id,
              file_name: data.file_name
            };
            resolve();
          });
    });
  }

  onDeleteData(id: any) {
    const json: any = {};
    json._id = this.listFilteredData[id]._id;
    if (this.selectedForm.form_type === 'Multi Record View') {
      json.table_id = this.selectedTable._id;
    } else if (this.selectedForm.form_type === 'Master Detail View') {
      json.table_id = this.selectedMasterTable._id;
    }
    this.dataService.deleteData(json)
      .pipe(first())
      .subscribe(
        data => {
          this.listData = this.listData.filter((field: any) =>
            field._id !== this.listFilteredData[id]._id
          );
          this.listFilteredData = this.listData.filter((field: any) => this.Fil(field));
          this.modalReference.close();
        });
  }

  onDeleteSelected() {
    this.spinnerOverlayService.show();
    const json: any = {};
    json.table_id = this.selectedTable._id;
    json._ids = [];
    for (let i = 0; i < this.listData.length; i++) {
      if (this.listData[i].checked) {
        json._ids.push(this.listData[i]._id);
      }
    }
    this.dataService.deleteManyData(json)
      .pipe(first())
      .subscribe(
        data => {
          for (let i = 0; i < this.listData.length; i++) {
            if (this.listData[i].checked) {
              this.listData.splice(i, 1);
              i--;
            }
          }
          this.listFilteredData = this.listData.filter((field: any) => this.Fil(field));
          this.modalReference.close();
          this.spinnerOverlayService.hide();
        });
  }

  onSelectAll() {
    for (let i = 0; i < this.listFilteredData.length; i++) {
      this.listFilteredData[i].checked = this.checkAll;
    }
  }

  onPopUp(content: any, modalBig: boolean) {
    if (modalBig) {
      this.modalReference = this.modalService.open(content, { windowClass : 'customModalClassBig'});
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    } else {
      this.modalReference = this.modalService.open(content);
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    }
  }

  onPopUpAddData(content: any, modalBig: boolean) {
    this.popUpData = {};
    if (this.selectedForm.form_type === 'Multi Record View') {
      this.totalWidth = 0;
      for (let i = 0; i < this.selectedForm.sections.length; i++) {
        if (this.selectedForm.sections[i].column > this.totalWidth) {
          this.totalWidth = this.selectedForm.sections[i].column;
        }
      }
      this.totalWidth *= 410;
    } else if (this.selectedForm.form_type === 'Master Detail View') {
      this.totalWidthMaster = 0;
      for (let i = 0; i < this.selectedForm.master_table_sections.length; i++) {
        if (this.selectedForm.master_table_sections[i].column > this.totalWidthMaster) {
          this.totalWidthMaster = this.selectedForm.master_table_sections[i].column;
        }
      }
      this.totalWidthMaster *= 410;
    }
    this.onPopUp(content, modalBig);
  }

  onPopUpAddDetailData(content: any, modalBig: boolean) {
    this.popUpAddDataDetail = {};
    this.totalWidthDetail = 0;
    for (let i = 0; i < this.selectedForm.detail_table_sections.length; i++) {
      if (this.selectedForm.detail_table_sections[i].column > this.totalWidthDetail) {
        this.totalWidthDetail = this.selectedForm.detail_table_sections[i].column;
      }
    }
    this.totalWidthDetail *= 410;

    for (let i = 0; i < this.listFieldNameDetail.length; i++) {
      if (this.listFieldNameDetail[i][3].type === 'Relation') {
        if (this.listFieldNameDetail[i][3].values.table_id === this.selectedMasterTable._id) {
          this.popUpAddDataDetail[this.listFieldNameDetail[i][0]] = this.listFilteredData[this.popUpDataId][this.listFieldNameDetail[i][3].values.field_id];
        }
      }
    }

    this.onPopUp(content, modalBig);
  }

  onPopUpData(content: any, id: any, modalBig: boolean) {
    this.popUpData = this.utilService.deepCopy(this.listFilteredData[id]);
    this.popUpDataId = id;
    if (this.selectedForm.form_type === 'Multi Record View') {
      this.totalWidth = 0;
      for (let i = 0; i < this.selectedForm.sections.length; i++) {
        if (this.selectedForm.sections[i].column > this.totalWidth) {
          this.totalWidth = this.selectedForm.sections[i].column;
        }
      }
      this.totalWidth *= 410;
    } else if (this.selectedForm.form_type === 'Master Detail View') {
      if (!this.popUpData['#detail_table_data#']) {
        this.popUpData['#detail_table_data#'] = [];
      }
      this.totalWidthMaster = 0;
      for (let i = 0; i < this.selectedForm.master_table_sections.length; i++) {
        if (this.selectedForm.master_table_sections[i].column > this.totalWidthMaster) {
          this.totalWidthMaster = this.selectedForm.master_table_sections[i].column;
        }
      }
      this.totalWidthMaster *= 410;

      this.totalWidthDetail = 0;
      for (let i = 0; i < this.selectedForm.detail_table_sections.length; i++) {
        if (this.selectedForm.detail_table_sections[i].column > this.totalWidthDetail) {
          this.totalWidthDetail = this.selectedForm.detail_table_sections[i].column;
        }
      }
      this.totalWidthDetail *= 410;
      this.popUpDataDetail = null;
    }
    this.onPopUp(content, modalBig);
  }

  updateListFormName(): Promise<void> {
    return new Promise<void>(resolve => {
      this.listForm = [];
      const json: any = {};
      json.project_id = this.selectedProjectId;
      this.formService.getFormWithCondition(json)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.listForm = data;

            this.listForm.sort((a, b) => {
              return b.sort_id - a.sort_id;
            });
            this.listForm.reverse();

            resolve();
          });
    });
  }

  async onChangeForm() {
    this.spinnerOverlayService.show();
    this.selectedForm = this.listForm[this.active];
    this.listFieldName = [];
    this.listFieldNameSection = [];
    this.listFilter = [];
    this.listFilterColumn = [];
    this.listShowField = this.listFieldName;

    await this.getTable();
    await this.getPermission();
    if (this.selectedForm.form_type === 'Multi Record View') {
      for (const a in this.selectedForm.metadata) {
        if (this.listFieldPermission[a]) {
          this.listFieldName.push([a, this.selectedForm.metadata[a], this.listFieldPermission[a], this.selectedTable.metadata[a]]);

          // check datetime
          if (this.selectedTable.metadata[a].type === 'Datetime') {
            for (let i = 0; i < this.listData.length; i++) {
              if (this.listData[i][a]) {
                this.listData[i][a] = new Date(this.listData[i][a]);
              }
            }
          }
        }
      }
      if (this.listFieldName.length === 0) {
        this.spinnerOverlayService.hide();
      } else {
        this.listFieldName.sort((a, b) => {
          return b[1].sort_id - a[1].sort_id;
        });
        this.listFieldName.reverse();

        // section
        for (let i = 0; i < this.selectedForm.sections.length; i++) {
          this.listFieldNameSection[i] = [];
          for (const a in this.selectedForm.sections[i].metadata) {
            if (a === 'Blank') {
              this.listFieldNameSection[i].push([a, this.selectedForm.sections[i].metadata[a]]);
            } else if (this.listFieldPermission[a]) {
              this.listFieldNameSection[i].push([a, this.selectedForm.sections[i].metadata[a], this.listFieldPermission[a], this.selectedTable.metadata[a]]);
            }
          }
          if (this.listFieldNameSection[i].length !== 0) {
            this.listFieldNameSection[i].sort((a, b) => {
              return b[1].sort_id - a[1].sort_id;
            });
            this.listFieldNameSection[i].reverse();
          }
        }
      }
    } else if (this.selectedForm.form_type === 'Master Detail View') {
      // get master field name
      for (const a in this.selectedForm.master_table_metadata) {
        if (this.listFieldPermission.master[a]) {
          this.listFieldName.push([a, this.selectedForm.master_table_metadata[a], this.listFieldPermission.master[a], this.selectedMasterTable.metadata[a]]);

          // check datetime
          if (this.selectedMasterTable.metadata[a].type === 'Datetime') {
            for (let i = 0; i < this.listData.length; i++) {
              if (this.listData[i][a]) {
                this.listData[i][a] = new Date(this.listData[i][a]);
              }
            }
          }
        }
      }
      if (this.listFieldName.length === 0) {
        this.spinnerOverlayService.hide();
      } else {
        this.listFieldName.sort((a, b) => {
          return b[1].sort_id - a[1].sort_id;
        });
        this.listFieldName.reverse();

        // section
        for (let i = 0; i < this.selectedForm.master_table_sections.length; i++) {
          this.listFieldNameSection[i] = [];
          for (const a in this.selectedForm.master_table_sections[i].metadata) {
            if (a === 'Blank') {
              this.listFieldNameSection[i].push([a, this.selectedForm.master_table_sections[i].metadata[a]]);
            } else if (this.listFieldPermission.master[a]) {
              this.listFieldNameSection[i].push([a, this.selectedForm.master_table_sections[i].metadata[a], this.listFieldPermission.master[a], this.selectedMasterTable.metadata[a]]);
            }
          }
          if (this.listFieldNameSection[i].length !== 0) {
            this.listFieldNameSection[i].sort((a, b) => {
              return b[1].sort_id - a[1].sort_id;
            });
            this.listFieldNameSection[i].reverse();
          }
        }
      }

      // get detail field name
      this.listFieldNameDetail = [];
      for (const a in this.selectedForm.detail_table_metadata) {
        if (this.listFieldPermission.detail[a]) {
          this.listFieldNameDetail.push([a, this.selectedForm.detail_table_metadata[a], this.listFieldPermission.detail[a], this.selectedDetailTable.metadata[a]]);
        }
      }
      if (this.listFieldNameDetail.length === 0) {
        this.spinnerOverlayService.hide();
      } else {
        this.listFieldNameDetail.sort((a, b) => {
          return b[1].sort_id - a[1].sort_id;
        });
        this.listFieldNameDetail.reverse();
      }

      // section
      this.listFieldNameDetailSection = [];

      for (let i = 0; i < this.selectedForm.detail_table_sections.length; i++) {
        this.listFieldNameDetailSection[i] = [];
        for (const a in this.selectedForm.detail_table_sections[i].metadata) {
          if (a === 'Blank') {
            this.listFieldNameDetailSection[i].push([a, this.selectedForm.detail_table_sections[i].metadata[a]]);
          } else if (this.listFieldPermission.detail[a]) {
            this.listFieldNameDetailSection[i].push([a, this.selectedForm.detail_table_sections[i].metadata[a], this.listFieldPermission.detail[a], this.selectedDetailTable.metadata[a]]);
          }
        }
        if (this.listFieldNameDetailSection[i].length !== 0) {
          this.listFieldNameDetailSection[i].sort((a, b) => {
            return b[1].sort_id - a[1].sort_id;
          });
          this.listFieldNameDetailSection[i].reverse();
        }
      }
      console.log(this.listFieldNameDetailSection);
    }

    this.spinnerOverlayService.hide();
    this.getLinkedTable();
    this.setListSearch();
  }

  getTable(): Promise<void> {
    return new Promise<void>(resolve => {
      if (this.selectedForm.form_type === 'Multi Record View') {
        this.tableService.getTableWithId(this.selectedForm.table_id)
          .pipe(first())
          .subscribe(
            (data: any) => {
              this.selectedTable = data;
              resolve();
            });
      } else if (this.selectedForm.form_type === 'Master Detail View') {
        this.tableService.getTableWithId(this.selectedForm.master_table_id)
          .pipe(first())
          .subscribe(
            (data: any) => {
              this.selectedMasterTable = data;
              this.tableService.getTableWithId(this.selectedForm.detail_table_id)
                .pipe(first())
                .subscribe(
                  (data1: any) => {
                    this.selectedDetailTable = data1;
                    resolve();
                  });
            });
      }
    });
  }

  getLinkedTable() {
    this.listDataRelation = [];
    for (let i = 0; i < this.listFieldNameSection.length; i++) {
      for (let j = 0; j < this.listFieldNameSection[i].length; j++) {
        if (this.listFieldNameSection[i][j][0] !== 'Blank') {
          if (this.listFieldNameSection[i][j][3].type === 'Relation') {
            const json: any = {};
            json.table_id = this.listFieldNameSection[i][j][3].values.table_id;
            this.dataService.getDataFromTable(json)
              .pipe(first())
              .subscribe(
                (data: any) => {
                  if (!this.listDataRelation[i]) {
                    this.listDataRelation[i] = [];
                  }
                  this.listDataRelation[i][j] = data;
                  this.listDataRelation[i][j].linked_field = this.listFieldNameSection[i][j][3].values.field_id;
                });
          }
        }
      }
    }
    if (this.selectedForm.form_type === 'Master Detail View') {
      for (let i = 0; i < this.listFieldNameDetailSection.length; i++) {
        for (let j = 0; j < this.listFieldNameDetailSection[i].length; j++) {
          if (this.listFieldNameDetailSection[i][j][0] !== 'Blank') {
            if (this.listFieldNameDetailSection[i][j][3].type === 'Relation') {
              const json: any = {};
              json.table_id = this.listFieldNameDetailSection[i][j][3].values.table_id;
              this.dataService.getDataFromTable(json)
                .pipe(first())
                .subscribe(
                  (data: any) => {
                    if (!this.listDataRelationDetail[i]) {
                      this.listDataRelationDetail[i] = [];
                    }
                    this.listDataRelationDetail[i][j] = data;
                    this.listDataRelationDetail[i][j].linked_field = this.listFieldNameDetailSection[i][j][3].values.field_id;
                  });
            }
          }
        }
      }
    }
  }

  getPermission(): Promise<void> {
    return new Promise<void>(resolve => {
      this.listFilteredData = [];
      this.listRgPermission = [];
      this.listFieldPermission = [];
      const json: any = {};
      json.form_id = this.selectedForm._id;
      if (this.listFilter.length !== 0) {
        json.filter = this.listFilter;
      }

      this.dataService.getDataFromForm(json)
        .pipe(first())
        .subscribe(
          (data: any) => {
            console.log(data);
            this.listData = data.data;
            this.listRgPermission = data.rg_permission;
            this.listFieldPermission = data.field_permission;

            this.listFilteredData = this.listData.filter((field: any) => this.Fil(field));
            resolve();
          });
    });
  }

  getListData() {
    return this.listFilteredData
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  getCurrentId(id: any) {
    return (id + (this.page - 1) * this.pageSize);
  }

  onClickImport() {
    this.importBtn.nativeElement.click();
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.onUploadData();
  }

  onFileFieldChanged(event, idSection, id) {
    if (!this.popUpData[this.listFieldNameSection[idSection][id][0]]) {
      this.popUpData[this.listFieldNameSection[idSection][id][0]] = {};
    }
    this.popUpData[this.listFieldNameSection[idSection][id][0]].file = event.target.files[0];
    this.popUpData[this.listFieldNameSection[idSection][id][0]].file_name = this.popUpData[this.listFieldNameSection[idSection][id][0]].file.name;
  }

  onFileFieldChangedDetail(event, idSection, id) {
    if (!this.popUpDataDetail[this.listFieldNameDetailSection[id][0]]) {
      this.popUpDataDetail[this.listFieldNameDetailSection[id][0]] = {};
    }
    this.popUpDataDetail[this.listFieldNameDetailSection[id][0]].file = event.target.files[0];
    this.popUpDataDetail[this.listFieldNameDetailSection[id][0]].file_name = this.popUpDataDetail[this.listFieldNameDetailSection[id][0]].file.name;
  }

  onUploadData() {
    this.spinnerOverlayService.show();
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    this.dataService.onUploadData(uploadData, this.selectedForm.table_id)
      .pipe(first())
      .subscribe(
        async (data1: any) => {
          this.spinnerOverlayService.hide();
          await this.onRefresh();
        });
    this.importBtn.nativeElement.value = '';
  }

  onDownloadData(fileId: any) {
    this.spinnerOverlayService.show();
    this.dataService.onDownloadFileData(fileId)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.spinnerOverlayService.hide();
        });
  }

  onDownloadImportTemplate() {
    const json: any = {};
    json.form_id = this.selectedForm._id;
    this.dataService.downloadImportTemplate(json)
      .pipe(first())
      .subscribe(
        (data: any) => {
        });
  }

  onExportAll() {
    this.spinnerOverlayService.show();
    const json: any = {};
    json.form_id = this.selectedForm._id;
    this.dataService.onExportAll(json)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.spinnerOverlayService.hide();
        });
  }

  onExportSelected() {
    const json: any = {};
    json.form_id = this.selectedForm._id;
    json._ids = [];
    for (let i = 0; i < this.listData.length; i++) {
      if (this.listData[i].checked) {
        json._ids.push(this.listData[i]._id);
      }
    }
    this.dataService.onExportSelected(json)
      .pipe(first())
      .subscribe(
        (data1: any) => {
        });
  }

  async onRefresh() {
    this.spinnerOverlayService.show();
    this.listFilter = [];
    // await this.updateListFormName();
    // await this.onChangeForm();

    await this.getPermission();
    this.spinnerOverlayService.hide();
  }

  // Master Detail View
  onSelectDetailData(id: number) {
    this.popUpDataDetail = this.utilService.deepCopy(this.popUpData['#detail_table_data#'][id]);
  }

  async onSaveMasterDetailData() {
    this.spinnerOverlayService.show();
    if (this.popUpDataDetail) {
      await this.onSaveDetailData();
    }
    for (let i = 0; i < this.listFieldName.length; i++) {
      if (this.selectedForm.master_table_metadata[this.listFieldName[i][0]].mandatory) {
        if (!this.popUpData[this.listFieldName[i][0]]) {
          this.spinnerOverlayService.hide();
          alert('You have to fill all the required information (*) in Master Data');
          return;
        }
      }
    }

    for (let i = 0; i < this.listFieldName.length; i++) {
      // upload file data
      if (this.selectedMasterTable.metadata[this.listFieldName[i][0]].type === 'File' && this.popUpData[this.listFieldName[i][0]]) {
        if (this.popUpData[this.listFieldName[i][0]].file) {
          await this.onUploadFileData(i);
        }
      }
    }

    const saveData: any = {};
    saveData.data = this.utilService.deepCopy(this.popUpData);
    saveData._id = this.popUpData._id;
    saveData.table_id = this.selectedMasterTable._id;
    delete saveData.data['#detail_table_data#'];
    delete saveData.data.record_group;
    delete saveData.data.checked;
    delete saveData.data._id;

    for (let i = 0; i < this.listFieldName.length; i++) {
      // convert date to format
      if (this.selectedMasterTable.metadata[this.listFieldName[i][0]].type === 'Date') {
        saveData.data[this.listFieldName[i][0]] = this.datePipe.transform(this.popUpData[this.listFieldName[i][0]], 'yyyy-MM-dd');
      } else if (this.selectedMasterTable.metadata[this.listFieldName[i][0]].type === 'Datetime') {
        saveData.data[this.listFieldName[i][0]] = this.datePipe.transform(this.popUpData[this.listFieldName[i][0]], 'yyyy-MM-dd HH:mm:ss');
      }
    }

    this.dataService.updateData(saveData)
      .pipe(first())
      .subscribe(
        async data => {
          const listDetail = this.popUpData['#detail_table_data#'];
          await this.getDataWithId(this.popUpData._id, this.selectedMasterTable._id, true);
          this.popUpData['#detail_table_data#'] = listDetail;
          this.spinnerOverlayService.hide();
        });
  }

  onSaveDetailData() {
    return new Promise<void>(async resolve => {
      for (let i = 0; i < this.listFieldNameDetail.length; i++) {
        if (this.selectedForm.detail_table_metadata[this.listFieldNameDetail[i][0]].mandatory) {
          if (!this.popUpDataDetail[this.listFieldNameDetail[i][0]]) {
            this.spinnerOverlayService.hide();
            alert('You have to fill all the required information (*) in Detail Data');
            return;
          }
        }
      }

      for (let i = 0; i < this.listFieldNameDetail.length; i++) {
        // upload file data
        if (this.selectedDetailTable.metadata[this.listFieldNameDetail[i][0]].type === 'File' && this.popUpDataDetail[this.listFieldNameDetail[i][0]]) {
          if (this.popUpDataDetail[this.listFieldNameDetail[i][0]].file) {
            await this.onUploadFileData(i);
          }
        }
      }

      const saveData: any = {};
      saveData.data = this.utilService.deepCopy(this.popUpDataDetail);
      saveData.table_id = this.selectedDetailTable._id;
      delete saveData.data.record_group;
      delete saveData.data.checked;

      for (let i = 0; i < this.listFieldNameDetail.length; i++) {
        // convert date to format
        if (this.selectedDetailTable.metadata[this.listFieldNameDetail[i][0]].type === 'Date') {
          saveData.data[this.listFieldNameDetail[i][0]] = this.datePipe.transform(this.popUpDataDetail[this.listFieldNameDetail[i][0]], 'yyyy-MM-dd');
        } else if (this.selectedDetailTable.metadata[this.listFieldNameDetail[i][0]].type === 'Datetime') {
          saveData.data[this.listFieldNameDetail[i][0]] = this.datePipe.transform(this.popUpDataDetail[this.listFieldNameDetail[i][0]], 'yyyy-MM-dd HH:mm:ss');
        }
      }

      saveData._id = this.popUpDataDetail._id;
      delete saveData.data._id;
      this.dataService.updateData(saveData)
        .pipe(first())
        .subscribe(
          data => {
            resolve();
          });
    });
  }

  async onAddMasterData() {
    this.spinnerOverlayService.show();
    for (let i = 0; i < this.listFieldName.length; i++) {
      if (this.listFieldName[i][3].type === 'Text') {
        if (this.popUpData[this.listFieldName[i][0]]) {
          this.popUpData[this.listFieldName[i][0]] = this.popUpData[this.listFieldName[i][0]].trim();
          if (this.popUpData[this.listFieldName[i][0]] === '') {
            this.popUpData[this.listFieldName[i][0]] = null;
          }
        }
      }
      if (this.selectedForm.master_table_metadata[this.listFieldName[i][0]].mandatory) {
        if (!this.popUpData[this.listFieldName[i][0]]) {
          this.spinnerOverlayService.hide();
          alert('You have to fill all the required information (*)');
          return;
        }
      }
    }

    for (let i = 0; i < this.listFieldName.length; i++) {
      // upload file data
      if (this.selectedMasterTable.metadata[this.listFieldName[i][0]].type === 'File' && this.popUpData[this.listFieldName[i][0]]) {
        if (this.popUpData[this.listFieldName[i][0]].file) {
          await this.onUploadFileData(i);
        }
      }
    }
    const saveData: any = {};
    saveData.data = this.utilService.deepCopy(this.popUpData);
    saveData.table_id = this.selectedMasterTable._id;

    for (let i = 0; i < this.listFieldName.length; i++) {
      // convert date to format
      if (this.selectedMasterTable.metadata[this.listFieldName[i][0]].type === 'Date') {
        saveData.data[this.listFieldName[i][0]] = this.datePipe.transform(this.popUpData[this.listFieldName[i][0]], 'yyyy-MM-dd');
      } else if (this.selectedMasterTable.metadata[this.listFieldName[i][0]].type === 'Datetime') {
        saveData.data[this.listFieldName[i][0]] = this.datePipe.transform(this.popUpData[this.listFieldName[i][0]], 'yyyy-MM-dd HH:mm:ss');
      }
    }

    this.dataService.addNewData(saveData)
      .pipe(first())
      .subscribe(
        async (data: any) => {
          await this.getDataWithId(data._id, this.selectedMasterTable._id, false);
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  async onAddDetailData() {
    this.spinnerOverlayService.show();
    // if (!this.popUpAddDataDetail[this.selectedDetailTable.primary_key]) {
    //   this.spinnerOverlayService.hide();
    //   alert('Primary Key (' + this.selectedDetailTable.primary_key + ') must not be blanked');
    //   return;
    // }
    for (let i = 0; i < this.listFieldNameDetail.length; i++) {
      if (this.listFieldNameDetail[i][3].type === 'Text') {
        if (this.popUpAddDataDetail[this.listFieldNameDetail[i][0]]) {
          this.popUpAddDataDetail[this.listFieldNameDetail[i][0]] = this.popUpAddDataDetail[this.listFieldNameDetail[i][0]].trim();
          if (this.popUpAddDataDetail[this.listFieldNameDetail[i][0]] === '') {
            this.popUpAddDataDetail[this.listFieldNameDetail[i][0]] = null;
          }
        }
      }
      if (this.selectedForm.detail_table_metadata[this.listFieldNameDetail[i][0]].mandatory) {
        if (!this.popUpAddDataDetail[this.listFieldNameDetail[i][0]]) {
          this.spinnerOverlayService.hide();
          alert('You have to fill all the required information (*)');
          return;
        }
      }
    }

    for (let i = 0; i < this.listFieldNameDetail.length; i++) {
      // upload file data
      if (this.selectedDetailTable.metadata[this.listFieldNameDetail[i][0]].type === 'File' && this.popUpAddDataDetail[this.listFieldNameDetail[i][0]]) {
        if (this.popUpAddDataDetail[this.listFieldNameDetail[i][0]].file) {
          await this.onUploadFileData(i);
        }
      }
    }
    const saveData: any = {};
    saveData.data = this.utilService.deepCopy(this.popUpAddDataDetail);
    saveData.table_id = this.selectedDetailTable._id;

    for (let i = 0; i < this.listFieldNameDetail.length; i++) {
      // convert date to format
      if (this.selectedDetailTable.metadata[this.listFieldNameDetail[i][0]].type === 'Date') {
        saveData.data[this.listFieldNameDetail[i][0]] = this.datePipe.transform(this.popUpAddDataDetail[this.listFieldNameDetail[i][0]], 'yyyy-MM-dd');
      } else if (this.selectedDetailTable.metadata[this.listFieldNameDetail[i][0]].type === 'Datetime') {
        saveData.data[this.listFieldNameDetail[i][0]] = this.datePipe.transform(this.popUpAddDataDetail[this.listFieldNameDetail[i][0]], 'yyyy-MM-dd HH:mm:ss');
      }
    }

    this.dataService.addNewData(saveData)
      .pipe(first())
      .subscribe(
        async (data: any) => {
          this.modalReference.close();
          this.spinnerOverlayService.hide();
          await this.onRefresh();
          this.popUpData = this.utilService.deepCopy(this.listFilteredData[this.popUpDataId]);
        });
  }

  onDeleteDetailData() {
    const json: any = {};
    json._id = this.popUpDataDetail._id;
    json.table_id = this.selectedDetailTable._id;
    this.dataService.deleteData(json)
      .pipe(first())
      .subscribe(
        async data => {
          await this.onRefresh();
          this.popUpData = this.utilService.deepCopy(this.listFilteredData[this.popUpDataId]);
          this.popUpDataDetail = null;
          this.modalReference.close();
        });
  }

  // Data Filter
  onAddFilter() {
    const newRow = {};
    this.listFilter.push(newRow);
  }

  onDeleteFilter(id: any) {
    this.listFilter.splice(id, 1);
  }

  async onSearch() {
    this.spinnerOverlayService.show();
    await this.getPermission();
    this.spinnerOverlayService.hide();
  }

  // column filter
  onFilterColumn() {
    this.listFilterColumn = [];

    for (let i = 0; i < this.listFieldName.length; i++) {
      if (this.listFilterCheck[i + 1]) {
        this.listFilterColumn.push(this.listFieldName[i]);
      }
    }
    this.listShowField = this.listFilterColumn;
  }

  onFilterColumnAll() {
    for (let i = 0; i < this.listFieldName.length; i++) {
      this.listFilterCheck[i + 1] = this.listFilterCheck[0];
    }
  }

  onPopUpFilterColumn(content: any, modalBig: boolean) {
    this.popUpData = {};
    if (this.listFieldName.length === 0) {
      this.rowNumFilter = 0;
    } else {
      this.rowNumFilter = Math.floor(this.listFieldName.length / this.colNumFilter);
    }
    if (this.listFieldName.length % this.colNumFilter !== 0) {
      this.rowNumFilter += 1;
    }
    this.onPopUp(content, modalBig);
  }

  setListSearch() {
    for (let i = 0; i < this.listFieldName.length; i++) {
      this.listSearch[this.listFieldName[i][0]] = '';
    }
  }

  performFilter(event: any) {
    if (event.keyCode === 13) {
      this.listFilteredData = this.listData.filter((field: any) => this.Fil(field));
    }
  }

  Fil(field: any) {
    let flag = true;
    for (let i = 0; i < this.listFieldName.length; i++) {
      if (this.listSearch[this.listFieldName[i][0]] !== '') {
        if (field[this.listFieldName[i][0]]) {
        if (field[this.listFieldName[i][0]].toString().toLocaleLowerCase().indexOf(this.listSearch[this.listFieldName[i][0]].toLocaleLowerCase()) === -1 ) {
          flag = false;
          break;
        }
       } else {
         flag = false;
         break;
       }
      }
    }
    return flag;
  }
}
