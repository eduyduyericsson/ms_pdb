import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';
import {ProjectService} from '../../services/project.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-select-project',
  templateUrl: './select-project.component.html',
  styleUrls: ['./select-project.component.scss']
})
export class SelectProjectComponent implements OnInit {

  constructor(
    private router: Router,
    private spinnerOverlayService: SpinnerOverlayService,
    private projectService: ProjectService,
  ) { }

  listProject: any = [];
  selectedProject: any;

  ngOnInit() {
    this.spinnerOverlayService.show();
    this.projectService.getAllProject()
      .pipe(first())
      .subscribe(
        data => {
          this.listProject = data;
          this.spinnerOverlayService.hide();
        });
  }

  onSelectProject(event: any) {
    if (event) {
      this.selectedProject = event;
    } else {
      this.selectedProject = null;
    }
  }

  onContinue() {
    if (this.selectedProject) {
      this.router.navigate([`/data/${this.selectedProject._id}`]);
    }
  }
}
