import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppRoutingModule} from './app.routing';
import { MatIconModule } from '@angular/material';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { OWL_DATE_TIME_FORMATS, OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { NgSelectModule } from '@ng-select/ng-select';
import {DatePipe} from '@angular/common';



import { AppComponent } from './app.component';
import { NavBarComponent} from './nav-bar';
import { DashboardComponent } from './dashboard/dashboard.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ManageProjectsComponent } from './admin/manage-projects/manage-projects.component';
import { DataComponent } from './manage-data/data/data.component';
import {JwtInterceptor} from './services/jwt.interceptor';
import { ManageRecordGroupsComponent } from './admin/manage-record-groups/manage-record-groups.component';
import { ManageRolesComponent } from './admin/manage-roles/manage-roles.component';
import { LoginComponent } from './login/login.component';
import { ManageUsersComponent } from './admin/manage-users/manage-users.component';
import { SpinnerOverlayComponent } from './spinner-overlay/spinner-overlay.component';
import { SetRulesComponent } from './admin/set-rules/set-rules.component';
import { ManageTablesComponent } from './admin/manage-tables/manage-tables.component';
import { ManageFormsComponent } from './admin/manage-forms/manage-forms.component';
import { ManageTemplatesComponent } from './admin/manage-templates/manage-templates.component';
import { BulkUploadComponent } from './manage-data/bulk-upload/bulk-upload.component';
import { SelectProjectComponent } from './manage-data/select-project/select-project.component';
import { DownloadFileComponent } from './manage-data/download-file/download-file.component';
import { ManagePermissionsComponent } from './admin/manage-permissions/manage-permissions.component';
import { LogBulkUploadComponent } from './manage-data/log-bulk-upload/log-bulk-upload.component';
import { ManageRuleGroupsComponent } from './admin/manage-rule-groups/manage-rule-groups.component';
import { SigninComponent } from './login/signin/signin.component';
import { ForgotpwdComponent } from './login/forgotpwd/forgotpwd.component';

export const MY_NATIVE_FORMATS = {
  fullPickerInput: {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric'},
  datePickerInput: {year: 'numeric', month: 'short', day: 'numeric'},
  timePickerInput: {hour: 'numeric', minute: 'short'},
  monthYearLabel: {year: 'numeric', month: 'short'},
  dateA11yLabel: {year: 'numeric', month: 'short', day: 'numeric'},
  monthYearA11yLabel: {year: 'numeric', month: 'short'},
};

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    DashboardComponent,
    ManageProjectsComponent,
    DataComponent,
    ManageRecordGroupsComponent,
    ManageRolesComponent,
    LoginComponent,
    ManageUsersComponent,
    SpinnerOverlayComponent,
    SetRulesComponent,
    ManageTablesComponent,
    ManageFormsComponent,
    ManageTemplatesComponent,
    BulkUploadComponent,
    SelectProjectComponent,
    DownloadFileComponent,
    ManagePermissionsComponent,
    LogBulkUploadComponent,
    ManageRuleGroupsComponent,
    SigninComponent,
    ForgotpwdComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgSelectModule,
    BrowserAnimationsModule,
    MatIconModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    DragDropModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_NATIVE_FORMATS},
    DatePipe
  ],
  entryComponents: [SpinnerOverlayComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
