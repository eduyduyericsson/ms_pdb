from flask_jwt_extended import JWTManager
from .db import db

jwt = JWTManager()

@jwt.user_claims_loader
def add_claims(identity):
    return {
        'user': identity,
    }
