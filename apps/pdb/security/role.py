from flask import Blueprint, request, jsonify
from flask_cors import CORS

from .db_interaction.role import get_role, get_all, add_role, update_role, delete_role
from apps.pdb.decorators import admin_required

role_api = Blueprint('role_api', 'role_api', url_prefix='/api/pdb/role')
CORS(role_api)


@role_api.route('/get/<id>', methods=['GET'])
@admin_required
def api_get_role(id):
    role = get_role(id)
    return role


@role_api.route('/get-all', methods=['GET'])
@admin_required
def api_get_all():
    roles = get_all()
    return jsonify(roles)


@role_api.route('/add', methods=['POST'])
@admin_required
def api_add_role():
    inp = request.get_json()
    if 'name' not in inp: 
        return jsonify({'msg': 'role\'s name is missing'}), 400 

    resp = add_role(inp)
    if isinstance(resp, dict):
        return jsonify({'msg': resp['err']}), 400

    return jsonify({'_id': resp.inserted_id}) 


@role_api.route('/update', methods=['PUT'])
@admin_required
def api_update_role():
    inp = request.get_json()
    if '_id' not in inp: 
        return jsonify({'msg': '_id is missing'}), 400 

    update_role(inp)
    return jsonify({'msg': 'success'})


@role_api.route('/delete/<_id>', methods=['DELETE'])
@admin_required
def api_delete_role(_id):
    resp = delete_role(_id)
    if isinstance(resp, str):
        return jsonify({'msg': resp}), 400
    return jsonify({'msg': 'success'})
