from flask import Blueprint, request, jsonify
from flask_cors import CORS

from bson.objectid import ObjectId

from .db_interaction.field_permission import get_field_permission, get_all, facet_search, add_field_permission, add_many, upsert_many, update_field_permission, delete_field_permission
from apps.pdb.decorators import admin_required

field_permission_api = Blueprint('field_permission_api', 'field_permission_api', url_prefix='/api/pdb/field-permission')
CORS(field_permission_api)

@field_permission_api.route('/get/<id>', methods=['GET'])
@admin_required
def api_get_field_permission(id):
    field_permission = get_field_permission(id)
    return field_permission

@field_permission_api.route('/get-all', methods=['GET'])
@admin_required
def api_get_all():
    field_permissions = get_all()
    return jsonify(field_permissions)

@field_permission_api.route('/facet-search', methods=['POST'])
@admin_required
def api_facet_search():
    condition = request.get_json()

    field_permissions = facet_search(condition)
    return jsonify(field_permissions)

@field_permission_api.route('/add', methods=['POST'])
@admin_required
def api_add_field_permission():
    inp = request.get_json()
    if 'table_id' not in inp                    \
        or 'role_id' not in inp                 \
        or 'field_name' not in inp              \
        or 'field_permission' not in inp        \
        or 'S' not in inp['field_permission']   \
        or 'U' not in inp['field_permission']:
        return jsonify({'msg': 'Some columns are missing'}), 400 

    inserted_id = add_field_permission(inp)
    return jsonify({'_id': inserted_id}) 

@field_permission_api.route('/add-many', methods=['POST'])
@admin_required
def api_add_many():
    inp = request.get_json()

    for d in inp:
        if 'table_id' not in d                      \
            or 'role_id' not in d                   \
            or 'field_name' not in d                \
            or 'field_permission' not in d          \
            or 'S' not in d['field_permission']     \
            or 'U' not in d['field_permission']:
            return jsonify({'msg': 'Some fields are missing'}), 400 

    resp = add_many(inp)
    return jsonify({'_ids': resp.inserted_ids})

@field_permission_api.route('/update', methods=['PUT'])
@admin_required
def api_update_field_permission():
    inp = request.get_json()
    if '_id' not in inp: 
        return jsonify({'msg': '_id is missing'}), 400 

    update_field_permission(inp)
    return jsonify({'msg': 'success'})

@field_permission_api.route('/upsert-many', methods=['PUT'])
@admin_required
def api_upsert_many():
    try:
        inp = request.get_json() 
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    resp = upsert_many(inp)
    if resp != None:
        return jsonify({'_ids': resp.inserted_ids})
    else:
        return jsonify({'msg': 'success'})

@field_permission_api.route('/delete/<id>', methods=['DELETE'])
@admin_required
def api_delete_field_permission(id):
    delete_field_permission(id)
    return jsonify({'msg': 'success'})