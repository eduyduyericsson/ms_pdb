from flask import Blueprint, jsonify, request
from flask_cors import CORS

from bson.json_util import loads, dumps
from bson.objectid import ObjectId
from flask_jwt_extended import jwt_required, create_access_token, create_refresh_token, get_jwt_claims, get_jwt_identity, jwt_refresh_token_required

from apps.security import jwt, bcrypt, add_claims_to_access_token
from apps.pdb.decorators import admin_required
from .db_interaction.user import *


user_api = Blueprint('user_api', 'user_api', url_prefix='/api/pdb/user')
CORS(user_api)

class User(object):
    def __init__(self, userdata):
        self.email = userdata.get('email')
        self.username = userdata.get('username')
        self.name = userdata.get('name')
        self.password_hash = userdata.get('password_hash')
        self.roles = userdata.get('roles', [])

    def to_json(self):
        return loads(dumps(self, default=lambda o: o.__dict__, sort_keys=True))

    @staticmethod
    def from_claims(claims):
        return User(claims.get('user'))

@user_api.route('/get', methods=['GET'])
@jwt_required
def api_get_info():
    current_user = get_jwt_identity()
    user_id = current_user['_id']
    u = get_info(user_id)
    if u == {} or u == None:
        return jsonify({'msg': 'user not found'}), 400
    return jsonify(u)

@user_api.route('/get-all', methods=['GET'])
@admin_required
def api_get_all():
    users = get_all()
    return jsonify(users)

@user_api.route('/register', methods=['POST'])
@admin_required
def api_register():
    try:
        inp = request.get_json()
        username = inp['username']
        name = inp['name']
        email = inp['email']
        password = inp['password']
        roles = inp.get('roles', [])
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    insertion_res = add_user(username, email, name, bcrypt.generate_password_hash(password=password.encode('utf8').decode('utf-8')), roles)
    if isinstance(insertion_res, dict):
        return jsonify({'msg': insertion_res['err']}), 400
    
    u_check = get_user(username)
    if u_check is None:
        return jsonify({'msg': 'Internal error, please try again later'}), 500

    return jsonify({'_id': insertion_res})

@user_api.route('/update', methods=['PUT'])
@jwt_required
def api_update_user_info():
    # need to get roles from DB
    try:
        inp = request.get_json()
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    if '_id' not in inp:
        return jsonify({'msg': '_id is missing'}), 400

    current_user = get_jwt_identity()
    current_user_roles = current_user['roles']
    user_id = current_user['_id']

    if str(inp['_id']) != str(user_id) and 'admin' not in current_user_roles:
        return jsonify({'msg': 'You do not have permission to update information of other users'}), 400

    update_info = {}
    update_info['_id'] = inp['_id']
    for key in ['email', 'name', 'roles']:
        if key in inp:
            update_info[key] = inp[key]
    if 'password' in inp:
        password = inp['password']
        update_info['password_hash'] = bcrypt.generate_password_hash(password=password.encode('utf8').decode('utf-8'))

    userdata = update_user_info(update_info)
    userdata = {
        '_id': userdata.get('_id'),
        'username': userdata.get('username'), 
        'name': userdata.get('name'),
        'email': userdata.get('email'),
        'roles': userdata.get('roles', [])
    }

    u = User(userdata)
    access_token = create_access_token(u.to_json())
    login_user(u.username, access_token)

    response_object = {'access_token': access_token, 'info': userdata}
    return jsonify(response_object), 201

@user_api.route('/login', methods=['POST'])
def api_login():
    try:
        inp = request.get_json()
        username = inp['username']
        pwd = inp['password']
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    userdata = get_user(username)
    if userdata is None:
        return jsonify({'msg': 'Username or password is not valid'}), 401

    if not bcrypt.check_password_hash(userdata['password_hash'], pwd):
        return jsonify({'msg': 'Username or password is not valid'}), 401

    userdata = {
        '_id': userdata.get('_id'),
        'username': userdata.get('username'), 
        'name': userdata.get('name'),
        'email': userdata.get('email'),
        'roles': userdata.get('roles', [])
    }
    
    u = User(userdata)
    access_token = create_access_token(userdata)
    refresh_token = create_refresh_token(userdata)

    try:
        login_user(u.username, access_token)
        response_object = {'access_token': access_token, 'refresh_token': refresh_token, 'info': userdata}
        return jsonify(response_object), 201
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

@user_api.route('/refresh-token', methods=['GET'])
@jwt_refresh_token_required
def api_refresh_access_token():
    current_user = get_jwt_identity()
    new_access_token = create_access_token(current_user)
    return jsonify({'access_token': new_access_token})

@user_api.route('/logout', methods=['GET'])
@jwt_required
def api_logout():
    claims = get_jwt_claims()
    u = User.from_claims(claims)
    try:
        logout_user(u.username)
        return jsonify({'msg': 'logged_out'}), 201
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

@user_api.route('/delete/<id>', methods=['DELETE'])
@admin_required
def api_delete_user(id):
    resp = delete_user(id)
    return jsonify({'num_deleted': resp.deleted_count})