from apps.db import db 
from bson.objectid import ObjectId

def get_rg_permission(id):
    return db.rg_permission.find_one({'_id': ObjectId(id)})

def get_all():
    return list(db.rg_permission.find())

def facet_search(condition):
    if 'record_group_id' in condition:
        record_group_id = ObjectId(condition['record_group_id'])
        condition['record_group_id'] = record_group_id
    
    if 'role_id' in condition:
        role_id = ObjectId(condition['role_id'])
        condition['role_id'] = role_id

    return list(db.rg_permission.find(condition))

def add_rg_permission(inp):
    record_group_id = ObjectId(inp['record_group_id'])
    inp['record_group_id'] = record_group_id
    
    role_id = ObjectId(inp['role_id'])
    inp['role_id'] = role_id

    resp = db.rg_permission.insert_one(inp)
    return resp.inserted_id

def add_many(inp: list):
    inp_convert = []

    for d in inp:
        d2 = d[:]
        
        record_group_id = ObjectId(d2['record_group_id'])
        d2['record_group_id'] = record_group_id
        
        role_id = ObjectId(d2['role_id'])
        d2['role_id'] = role_id

        inp_convert.append(d2)

    resp = db.rg_permission.insert_many(inp_convert)
    return resp

def update_rg_permission(inp):
    _id = ObjectId(inp.pop('_id')) 
    if 'record_group_id' in inp:
        record_group_id = ObjectId(inp['record_group_id'])
        inp['record_group_id'] = record_group_id
    
    if 'role_id' in inp:
        role_id = ObjectId(inp['role_id'])
        inp['role_id'] = role_id
        
    resp = db.rg_permission.update_one({'_id': _id}, {'$set': inp})
    return resp
    
def upsert_many(inp):
    add_list = []
    for rgp in inp:
        new_rgp = dict.copy(rgp)

        if 'record_group_id' in new_rgp:
            record_group_id = ObjectId(new_rgp['record_group_id'])
            new_rgp['record_group_id'] = record_group_id 

        if 'role_id' in new_rgp:
            role_id = ObjectId(new_rgp['role_id'])
            new_rgp['role_id'] = role_id 

        if '_id' not in new_rgp:
            add_list.append(new_rgp)
        else:
            _id = ObjectId(new_rgp.pop('_id'))
            db.rg_permission.update_one({'_id': _id}, {'$set': new_rgp})

    if len(add_list) != 0:
        add_resp = db.rg_permission.insert_many(add_list)
        return add_resp
    else:
        return None
def delete_rg_permission(id):
    resp = db.rg_permission.delete_one({'_id': ObjectId(id)})
    return resp 