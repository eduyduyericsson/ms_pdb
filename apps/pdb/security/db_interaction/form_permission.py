from apps.db import db 
from bson.objectid import ObjectId

def get_form_permission(id):
    return db.form_permission.find_one({'_id': ObjectId(id)})

def get_all():
    return list(db.form_permission.find())

def facet_search(condition):
    if '_id' in condition:
        _id = ObjectId(condition['_id'])
        condition['_id'] = _id

    if 'project_id' in condition:
        project_id = ObjectId(condition['project_id'])
        condition['project_id'] = project_id

    if 'role_id' in condition:
        role_id = ObjectId(condition['role_id'])
        condition['role_id'] = role_id

    if 'form_id' in condition:
        form_id = ObjectId(condition['form_id'])
        condition['form_id'] = form_id

    return list(db.form_permission.find(condition))

def add_form_permission(inp):
    if 'project_id' in inp:
        project_id = ObjectId(inp['project_id'])
        inp['project_id'] = project_id

    if 'role_id' in inp:
        role_id = ObjectId(inp['role_id'])
        inp['role_id'] = role_id
    
    if 'form_id' in inp:
        form_id = ObjectId(inp['form_id'])
        inp['form_id'] = form_id

    resp = db.form_permission.insert_one(inp)

    # # Add full permissions for admin
    # admin_id = ObjectId(db.role.find_one({'name': 'admin'})['_id']) 
    # db.rg_permission.insert_one({
    #     'role_id': admin_id, 
    #     'form_permission_id': ObjectId(resp.inserted_id),
    #     'S': True, 
    #     'U': True, 
    #     'I': True, 
    #     'D': True
    # })

    return resp.inserted_id

def update_form_permission(inp):
    _id = ObjectId(inp.pop('_id')) 
    if 'project_id' in inp:
        project_id = ObjectId(inp['project_id'])
        inp['project_id'] = project_id

    if 'role_id' in inp:
        role_id = ObjectId(inp['role_id'])
        inp['role_id'] = role_id
    
    if 'form_id' in inp:
        form_id = ObjectId(inp['form_id'])
        inp['form_id'] = form_id

    resp = db.form_permission.update_one({'_id': _id}, {'$set': inp})
    return resp

def upsert_many(inp):
    add_list = []
    for pp in inp:
        new_pp = dict.copy(pp)

        if 'project_id' in new_pp:
            project_id = ObjectId(new_pp['project_id'])
            new_pp['project_id'] = project_id 

        if 'role_id' in new_pp:
            role_id = ObjectId(new_pp['role_id'])
            new_pp['role_id'] = role_id 

        if 'form_id' in new_pp:
            form_id = ObjectId(new_pp['form_id'])
            new_pp['form_id'] = form_id 

        if '_id' not in new_pp:
            if 'project_id' not in new_pp or 'role_id' not in new_pp or 'form_id' not in new_pp or 'form_permission' not in new_pp:
                raise Exception('Some fields are missing')
            add_list.append(new_pp)
        else:
            _id = ObjectId(new_pp.pop('_id'))            
            db.form_permission.update_one({'_id': _id}, {'$set': new_pp})

    if len(add_list) != 0:
        add_resp = db.form_permission.insert_many(add_list)
        return add_resp
    else:
        return None

def delete_form_permission(id):
    resp = db.form_permission.delete_one({'_id': ObjectId(id)})
    return resp 