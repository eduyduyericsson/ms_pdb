from apps.db import db 
from bson.objectid import ObjectId

def get_field_permission(id):
    return db.field_permission.find_one({'_id': ObjectId(id)})

def get_all():
    return list(db.field_permission.find())

def facet_search(condition):
    if 'table_id' in condition:
        table_id = ObjectId(condition['table_id'])
        condition['table_id'] = table_id
    
    if 'role_id' in condition:
        role_id = ObjectId(condition['role_id'])
        condition['role_id'] = role_id
        
    return list(db.field_permission.find(condition))

def add_field_permission(inp):
    table_id = ObjectId(inp['table_id'])
    inp['table_id'] = table_id

    role_id = ObjectId(inp['role_id'])
    inp['role_id'] = role_id

    resp = db.field_permission.insert_one(inp)
    return resp.inserted_id

def add_many(inp: list):
    inp_convert = []
    for d in inp:       
        d2 = d[:]

        table_id = ObjectId(d2['table_id'])
        d2['table_id'] = table_id

        role_id = ObjectId(d2['role_id'])
        d2['role_id'] = role_id

        inp_convert.append(d2)
    
    resp = db.field_permission.insert_many(inp_convert)
    return resp  

def update_field_permission(inp):
    _id = ObjectId(inp.pop('_id')) 
    if 'table_id' in inp:
        table_id = ObjectId(inp['table_id'])
        inp['table_id'] = table_id

    if 'role_id' in inp:
        role_id = ObjectId(inp['role_id'])
        inp['role_id'] = role_id

    resp = db.field_permission.update_one({'_id': _id}, {'$set': inp})
    return resp

def upsert_many(inp):
    add_list = []
    for fp in inp:
        new_fp = dict.copy(fp)
        if 'table_id' in new_fp:
            table_id = ObjectId(new_fp['table_id'])
            new_fp['table_id'] = table_id 
        if 'role_id' in new_fp:
            role_id = ObjectId(new_fp['role_id'])
            new_fp['role_id'] = role_id 

        if '_id' not in new_fp:
            add_list.append(new_fp)
        else:
            _id = ObjectId(new_fp.pop('_id'))
            db.field_permission.update_one({'_id': _id}, {'$set': new_fp})

    if len(add_list) != 0:
        add_resp = db.field_permission.insert_many(add_list)
        return add_resp
    else:
        return None

def delete_field_permission(id):
    resp = db.field_permission.delete_one({'_id': ObjectId(id)})
    return resp 