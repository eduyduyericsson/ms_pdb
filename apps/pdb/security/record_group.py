from flask import Blueprint, request, jsonify
from flask_cors import CORS

from bson.objectid import ObjectId

from .db_interaction.record_group import get_record_group, get_all, facet_search, add_record_group, update_record_group, delete_record_group
from apps.pdb.decorators import admin_required

record_group_api = Blueprint('record_group_api', 'record_group_api', url_prefix='/api/pdb/record-group')
CORS(record_group_api)


@record_group_api.route('/get/<_id>', methods=['GET'])
@admin_required
def api_get_record_group(_id):
    record_group = get_record_group(_id)
    return record_group


@record_group_api.route('/get-all', methods=['GET'])
@admin_required
def api_get_all():
    groups = get_all()
    return jsonify(groups)


@record_group_api.route('/facet-search', methods=['POST'])
@admin_required
def api_facet_search():
    condition = request.get_json()

    record_groups = facet_search(condition)
    return jsonify(record_groups)


@record_group_api.route('/add', methods=['POST'])
@admin_required
def api_add_record_group():
    inp = request.get_json()
    if 'name' not in inp: 
        return jsonify({'msg': 'Record_group\'s name is missing'}), 400 

    resp = add_record_group(inp)
    if isinstance(resp, dict):
        return jsonify({'msg': resp['err']}), 400
    return jsonify({'_id': resp.inserted_id})


@record_group_api.route('/update', methods=['PUT'])
@admin_required
def api_update_record_group():
    inp = request.get_json()
    if '_id' not in inp: 
        return jsonify({'msg': '_id is missing'}), 400 

    update_record_group(inp)
    return jsonify({'msg': 'success'})


@record_group_api.route('/delete/<_id>', methods=['DELETE'])
@admin_required
def api_delete_record_group(_id):
    delete_record_group(_id)
    return jsonify({'msg': 'success'})
