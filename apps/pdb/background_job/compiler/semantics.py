import math
import random
import pymongo
from datetime import date, datetime

from .parser import get_next_token, is_number, is_text
from .scanner import scan, key_words, basic_num_func


from apps.db import db


def exec_basic_num_func(func_name, param: float):
    if func_name == 'abs':
        return abs(param)
    elif func_name == 'acos':
        return math.acos(param)
    elif func_name == 'acosh':
        return math.acosh(param)
    elif func_name == 'asin':
        return math.asin(param)
    elif func_name == 'asinh':
        return math.asinh(param)
    elif func_name == 'atan':
        return math.atan(param)
    elif func_name == 'atanh':
        return math.atanh(param)
    elif func_name == 'ceil':
        return math.ceil(param)
    elif func_name == 'cos':
        return math.cos(param)
    elif func_name == 'cosh':
        return math.cosh(param)
    elif func_name == 'exp':
        return math.exp(param)
    elif func_name == 'floor':
        return math.floor(param)
    elif func_name == 'ln':
        return math.log(param, math.e)
    elif func_name == 'log':
        return math.log10(param)
    elif func_name == 'round':
        return round(param)
    elif func_name == 'sin':
        return math.sin(param)
    elif func_name == 'sinh':
        return math.sinh(param)
    elif func_name == 'sqrt':
        return math.sqrt(param)
    elif func_name == 'tan':
        return math.tan(param)
    elif func_name == 'tanh':
        return math.tanh(param)


def exec_basic_text_func(func_name, param: str):
    if func_name == 'length':
        return len(param)
    elif func_name == 'toLower':
        return param.lower()
    elif func_name == 'toUpper':
        return param.upper()


def exec_seq_params_num_func(func_name, params: list):
    if func_name == 'avg':
        return sum(params) / len(params)
    elif func_name == 'max':
        return max(params)
    elif func_name == 'median':
        pass
    elif func_name == 'min':
        return min(params)
    elif func_name == 'sum':
        return sum(params)
    elif func_name == 'count':
        return len(params)


def exec_concat(params):
    return ''.join(params)


def exec_if_func(op1, cmp_token, op2, value1, value2):
    bool_res = False
    if cmp_token == '=':
        bool_res = (op1 == op2)
    elif cmp_token == '<>':
        bool_res = (op1 != op2)
    elif cmp_token == '>':
        bool_res = (op1 > op2)
    elif cmp_token == '>=':
        bool_res = (op1 >= op2)
    elif cmp_token == '<':
        bool_res = (op1 < op2)
    elif cmp_token == '<=':
        bool_res = (op1 <= op2)

    if bool_res == True:
        return value1
    else:
        return value2


def factor(current_pos: int, list_token: list, record: dict, table: dict):
    current_pos, token = get_next_token(current_pos, list_token)
    if is_number(token):
        return current_pos, float(token), 'Number'

    elif is_text(token):
        return current_pos, token[1:-1], 'Text'

    elif token in ['+', '-']:
        current_pos, param, data_type = expression(current_pos-1, list_token, record, table)
        return current_pos, param, data_type

    elif token == '(':
        current_pos, param, data_type = expression(current_pos, list_token, record, table)
        current_pos, token = get_next_token(current_pos, list_token)

        return current_pos, param, data_type

    elif token in key_words:
        if token in basic_num_func:
            current_pos, token1 = get_next_token(current_pos, list_token)
            current_pos, param, data_type = expression(current_pos, list_token, record, table)
            current_pos, token2 = get_next_token(current_pos, list_token)

            return current_pos, exec_basic_num_func(token, param), 'Number'

        elif token in ['avg', 'max', 'median', 'min', 'sum']:
            func_name = token
            params = []

            current_pos, token = get_next_token(current_pos, list_token)
            current_pos, param, data_type = expression(current_pos, list_token, record, table)
            params.append(param)

            current_pos, token = get_next_token(current_pos, list_token)
            if token == ')':
                return current_pos, exec_seq_params_num_func(func_name, params), 'Number'
            elif token == ',':
                while token == ',':
                    current_pos, param, data_type = expression(current_pos, list_token, record, table)
                    params.append(param)
                    current_pos, token = get_next_token(current_pos, list_token)
                return current_pos, exec_seq_params_num_func(func_name, params), 'Number'

        elif token in ['countFields', 'avgFields', 'maxFields', 'minFields', 'medianFields', 'sumFields']:
            # # need to check data type of field
            # current_pos, token = get_next_token(current_pos, list_token)
            # if token != '(':
            #     raise Exception('( is missing')

            # current_pos, param_data_type = factor(current_pos, list_token, record, table)
            # if param_data_type != 'text':
            #     raise Exception('param data type must be text')
            # ###
            # current_pos, token2 = get_next_token(current_pos, list_token)

            # if token2 != ',':
            #     raise Exception(', is missing')

            # current_pos, param_data_type = factor(current_pos, list_token, record, table)
            # if param_data_type != 'text':
            #     raise Exception('param data type must be text')
            # ###
            # current_pos, token2 = get_next_token(current_pos, list_token)

            # if token2 != ')':
            #     raise Exception(') is missing')

            # return current_pos, 'number'
            pass

        elif token == 'clearField':
            current_pos, token = get_next_token(current_pos, list_token)
            current_pos, token = get_next_token(current_pos, list_token)

            return current_pos, None, 'None'

        elif token == 'concat':
            params = []
            current_pos, token = get_next_token(current_pos, list_token)

            current_pos, param, data_type = expression(current_pos, list_token, record, table)
            params.append(param)

            current_pos, token = get_next_token(current_pos, list_token)
            if token == ')':
                return current_pos, exec_concat(params), 'Text'
            elif token == ',':
                while token == ',':
                    current_pos, param, data_type = expression(current_pos, list_token, record, table)
                    params.append(param)
                    current_pos, token = get_next_token(current_pos, list_token)
                return current_pos, exec_concat(params), 'Text'

        elif token == 'currentDate':
            current_pos, token = get_next_token(current_pos, list_token)
            current_pos, token = get_next_token(current_pos, list_token)

            return current_pos, date.today().strftime('%Y-%m-%d'), 'Date'

        elif token == 'currentDateTime':
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'Datetime'

        elif token == 'getRecordName':
            current_pos, token = get_next_token(current_pos, list_token)
            current_pos, token = get_next_token(current_pos, list_token)
            # Need to be changed, return primary value
            primary_key = table['metadata']['primary_key']
            return current_pos, record[primary_key], 'Text'

        elif token in ['hour', 'minute', 'second']:
            # get hour, minute, second from datetime
            current_pos, token1 = get_next_token(current_pos, list_token)
            current_pos, param, data_type = expression(current_pos, list_token, record, table)
            current_pos, token2 = get_next_token(current_pos, list_token)

            d = datetime.strptime(param, '%Y-%m-%d %H:%M:%S')
            if token == 'hour':
                result = d.hour
            elif token == 'minute':
                result = d.minute
            elif token == 'second':
                result = d.second

            return current_pos, result, 'Number'

        elif token in ['year', 'month', 'day']:
            # get hour, minute, second from datetime
            current_pos, token1 = get_next_token(current_pos, list_token)
            current_pos, param, data_type = expression(current_pos, list_token, record, table)
            current_pos, token2 = get_next_token(current_pos, list_token)

            if len(param) == 10:
                d = datetime.strptime(param, '%Y-%m-%d')
            else:
                d = datetime.strptime(param, '%Y-%m-%d %H:%M:%S')

            if token == 'year':
                result = d.year
            elif token == 'month':
                result = d.month
            elif token == 'day':
                result = d.day

            return current_pos, result, 'Number'

        elif token == 'if':
            current_pos, token = get_next_token(current_pos, list_token)
            current_pos, op1, data_type = expression(current_pos, list_token, record, table)
            current_pos, cmp_token = get_next_token(current_pos, list_token)
            current_pos, op2, data_type = expression(current_pos, list_token, record, table)

            current_pos, token = get_next_token(current_pos, list_token)

            current_pos, value1, data_type = expression(current_pos, list_token, record, table)

            current_pos, token = get_next_token(current_pos, list_token)

            current_pos, value2, data_type = expression(current_pos, list_token, record, table)

            current_pos, token = get_next_token(current_pos, list_token)

            return current_pos, exec_if_func(op1, cmp_token, op2, value1, value2), data_type

        elif token == 'lastFieldModifiedDate':
            current_pos, token = get_next_token(current_pos, list_token)

            current_pos, field_name, data_type = expression(current_pos, list_token, record, table)

            current_pos, token = get_next_token(current_pos, list_token)

            table_name = table['name']
            record_id = record['_id']
            # Need to check
            newest_action = list(
                db.action_history.find({'table_name': table_name, 'key_id': record_id, 'field': field_name}).sort(
                    'time', pymongo.DESCENDING).limit(1))
            if len(newest_action) != 0:
                modified_date = newest_action[0]['time'].strftime('%Y-%m-%d')
            else:
                modified_date = ''

            return current_pos, modified_date, 'Date'

        elif token == 'lastRecordModifiedDate':

            current_pos, token = get_next_token(current_pos, list_token)

            current_pos, token = get_next_token(current_pos, list_token)

            return current_pos, record['updated_at'].strftime('%Y-%m-%d'), 'Date'

        elif token in ['lastRecordModifiedName', 'lastRecordModifiedUserName']:
            function_name = token
            current_pos, token = get_next_token(current_pos, list_token)
            current_pos, token = get_next_token(current_pos, list_token)

            if function_name == 'lastRecordModifiedUserName':
                return current_pos, record.get('updated_by', ''), 'Text'
            else:
                username = record.get('updated_by', '')
                user = db.pdb_user.find_one({'username': username})

                if user is not None:
                    return current_pos, user['name'], 'Text'
                else:
                    return current_pos, '', 'Text'

        elif token in ['lastFieldModifiedName', 'lastFieldModifiedUserName']:
            function_name = token
            current_pos, token = get_next_token(current_pos, list_token)

            current_pos, field_name, data_type = expression(current_pos, list_token, record, table)

            current_pos, token = get_next_token(current_pos, list_token)

            table_name = table['name']
            record_id = record['_id']
            # Need to check
            newest_action = list(
                db.action_history.find({'table_name': table_name, 'key_id': record_id, 'field': field_name}).sort(
                    'time', pymongo.DESCENDING).limit(1))
            if len(newest_action) != 0:
                username = newest_action[0]['user']
            else:
                username = ''

            if function_name == 'lastFieldModifiedUserName':
                return current_pos, username, 'Text'
            else:
                user = db.pdb_user.find_one({'username': username})
                if user is not None:
                    return current_pos, user['name'], 'Text'
                else:
                    return current_pos, '', 'Text'

        elif token in ['length', 'toLower', 'toUpper']:
            func_name = token
            current_pos, token1 = get_next_token(current_pos, list_token)
            current_pos, param, data_type = expression(current_pos, list_token, record, table)
            current_pos, token2 = get_next_token(current_pos, list_token)

            if token == 'length':
                data_type = 'Number'
            else:
                data_type = 'Text'

            return current_pos, exec_basic_text_func(func_name, param), data_type

        elif token in ['padLeft', 'padRight', 'subString']:
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = expression(current_pos, list_token, record, table)
            if param_data_type != 'text':
                raise Exception('param data type must be text')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ',':
                raise Exception(', is missing')

            current_pos, param_data_type = expression(current_pos, list_token, record, table)
            if param_data_type != 'number':
                raise Exception('param2 in function must be number')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ',':
                raise Exception(', is missing')

            current_pos, param_data_type = expression(current_pos, list_token, record, table)
            if param_data_type != 'number':
                raise Exception('param3 in function must be number')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'text'

        elif token == 'rand':
            current_pos, token = get_next_token(current_pos, list_token)
            current_pos, token = get_next_token(current_pos, list_token)

            return current_pos, random.random(), 'Number'

        elif token == 'replace':
            current_pos, token = get_next_token(current_pos, list_token)
            current_pos, param1, data_type = expression(current_pos, list_token, record, table)
            current_pos, token = get_next_token(current_pos, list_token)

            current_pos, param2, data_type = expression(current_pos, list_token, record, table)

            current_pos, token = get_next_token(current_pos, list_token)

            current_pos, param3, data_type = expression(current_pos, list_token, record, table)

            current_pos, token = get_next_token(current_pos, list_token)

            return current_pos, param1.replace(param2, param3), 'Text'

        elif token == 'str':
            current_pos, token1 = get_next_token(current_pos, list_token)

            current_pos, param, data_type = expression(current_pos, list_token, record, table)

            current_pos, token2 = get_next_token(current_pos, list_token)

            return current_pos, str(param), 'Text'

        elif token == 'value':

            current_pos, token1 = get_next_token(current_pos, list_token)

            current_pos, field_name = get_next_token(current_pos, list_token)

            field_name = field_name[1:-1]

            current_pos, token2 = get_next_token(current_pos, list_token)

            if table['metadata'][field_name]['type'] == 'Number':

                data = record.get(field_name, 0)

                if data is None:
                    data = 0

            else:

                data = record.get(field_name, '')

                if data is None:
                    data = ''
            # print(data)

            return current_pos, data, table['metadata'][field_name]['type']

    else:
        print(current_pos)
        return current_pos, 'Unknown', 'Unknown'


def term(current_pos, list_token, record, table):
    current_pos, value, data_type = factor(current_pos, list_token, record, table)

    if data_type == 'Number':
        result = float(value)
    else:
        return current_pos, value, data_type

    current_pos, token = get_next_token(current_pos, list_token)
    while token in ['*', '/']:
        current_pos, value, data_type = factor(current_pos, list_token, record, table)
        # print('result', result, 'value', value)
        if token == '*':
            result *= float(value)
        else:
            if value == 0:
                return current_pos, None, 'Number'
            result /= float(value)

        current_pos, token = get_next_token(current_pos, list_token)

    return current_pos - 1, result, 'Number'


def expression(current_pos, list_token, record, table):
    result = 0
    current_pos, token = get_next_token(current_pos, list_token)
    if token in ['+', '-']:
        current_pos, value, data_type = term(current_pos, list_token, record, table)
        if token == '+':
            result += float(value)
        else:
            result -= float(value)
    else:
        current_pos, value, data_type = term(current_pos - 1, list_token, record, table)
        if data_type == 'Number':
            result = float(value)
        else:
            return current_pos, value, data_type

    current_pos, token = get_next_token(current_pos, list_token)
    while token in ['+', '-']:
        current_pos, value, data_type = term(current_pos, list_token, record, table)
        if token == '+':
            result += float(value)
        else:
            result -= float(value)
        current_pos, token = get_next_token(current_pos, list_token)

    return current_pos - 1, result, 'Number'


def exec_function(text, record, table):
    # print(text, record, table)
    list_token = scan(text)
    # print(list_token)
    return expression(-1, list_token, record, table)[1]


if __name__ == "__main__":
    table_metadata = {'a': {'type': 'Text'}}
    record = {'a': 'nguyen duy manh'}
    # text = 'abs(acos(1/2))'
    # text = 'if( abs(-9) < 0, 0, 1)'
    # text = "if(avg(1,2,3) < 10, abs(-1.5), 1)"
    # text = "if('a' = 'a' , 'True', 'False   ')"
    # text = '-abs(-9)'
    # text = 'clearField())'
    # text = "concat('1', '2', '3')"
    # text = 'currentDate)'
    # text = 'getRecordName()'
    # # text = 'lastFieldModifiedDate()'
    # text = 'length(currentDate())'
    # # text = "padLeft('duymanh', 4, 1)"
    # text = 'rand()'
    # text = "replace('duymanh', 'a', 'b')"
    # text = 'sum(1, 2, floor(3.5))'
    # text = "month(currentDate())"
    # text = "if ('a' = 'b', floor(1.5), round(1.5)"
    # text = "toUpper('aBcD')"
    text = 'avg(1, 2, 3)'

    list_token = scan(text)
    print(list_token)
    # try:
    value = expression(-1, list_token, record, table_metadata)
    print(value)
    # except Exception as e:
    #     print(e)
