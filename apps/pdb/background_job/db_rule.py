from flask import current_app, copy_current_request_context
from flask_mail import Message

from apps.db import db
from .compiler.semantics import exec_function

from bson.objectid import ObjectId
from datetime import datetime
from threading import Thread  # for test, will be replaced by Celery


def get_rule(_id):
    return db.rule.find_one({'_id': ObjectId(_id)})


def get_all():
    return list(db.rule.find())


def facet_search(condition):
    if '_id' in condition:
        _id = ObjectId(condition['_id'])
        condition['_id'] = _id

    if 'table_id' in condition:
        table_id = ObjectId(condition['table_id'])
        condition['table_id'] = table_id

    if 'rule_group_id' in condition:
        rule_group_id = ObjectId(condition['rule_group_id'])
        condition['rule_group_id'] = rule_group_id

    return list(db.rule.find(condition))


def add_rule(inp):
    if 'created_at' in inp:
        inp.pop('created_at')
    name = inp.pop('name')

    if 'rule_group_id' in inp:
        rule_group_id = ObjectId(inp['rule_group_id'])
        inp['rule_group_id'] = rule_group_id

    check_exist = db.rule.find_one(inp)

    if check_exist is not None:
        return 'Rule already exists'

    inp['created_at'] = datetime.now()
    resp = db.rule.insert_one({'name': name, **inp})

    # For Test
    exec_rule(resp.inserted_id, created_fields=[], updated_fields=[], deleted_fields=[], record_flag=None)
    ###########
    
    return resp


def add_many(inp: list):
    # resp = db.field_permission.insert_many(inp)
    # return resp  
    pass 


def update_rule(inp):
    _id = ObjectId(inp.pop('_id'))
    if 'rule_group_id' in inp:
        rule_group_id = ObjectId(inp['rule_group_id'])
        inp['rule_group_id'] = rule_group_id

    resp = db.rule.update_one({'_id': _id}, {'$set': inp})

    # For Test
    exec_rule(_id, created_fields=[], updated_fields=[], deleted_fields=[], record_flag=None)
    ###########

    return resp


def delete_rule(_id):
    resp = db.rule.delete_one({'_id': ObjectId(_id)})
    return resp 


def exec_rule(rule_id, created_fields, updated_fields, deleted_fields, record_flag, additional_conditions={}):
    print('running rule %s' % rule_id)
    rule_id = ObjectId(rule_id) 
    rule = db.rule.find_one({'_id': rule_id})
    trigger_table = rule['trigger_table']
    condition = rule['condition']
    actions = rule['action']

    mongo_cond = convert_rule_filter(condition, created_fields, updated_fields, deleted_fields, record_flag)
    # print(mongo_cond)
    # resp = update_many_data_by_condition(None, {**mongo_cond, **additional_conditions}, {target_field: value}, table_name=target_table)

    for action in actions:
        action_type = action['action_type']

        if action_type in ['Set Value', 'Set Value by Function']:
            target_table = action['target_table']
            value = action['value']
            target_field = action['target_field']

            records = list(db[target_table].find({**mongo_cond, **additional_conditions}))
            table = db.table_metadata.find_one({'name': target_table})

            for r in records:
                _id = r['_id']
                if action_type == 'Set Value':
                    db[target_table].update_one({'_id': _id}, {'$set': {target_field: value}})
                else:
                    v = exec_function(value, r, table)
                    db[target_table].update_one({'_id': _id}, {'$set': {target_field: v}})

        elif action_type == 'Send Mail':
            mail = current_app.mail

            subject = action['subject']
            recipients = action['recipients']

            body = action['content']
            subject_params = action.get('subject_field', [])
            body_params = action.get('content_field', [])

            records = list(db[trigger_table].find({**mongo_cond, **additional_conditions}))
            # data = db[table_name].find_one({'_id': ObjectId(record_id), **mongo_cond})
            # if data is None or data == {}:
            #     continue
            for r in records:
                print(r)
                print(recipients)
                subject_params_values = tuple([r.get(p, '') for p in subject_params])
                body_params_values = tuple([r.get(p, '') for p in body_params])

                static_addresses = [address['name'] for address in recipients if address['type'] == 'Static Address']
                users = [address['email'] for address in recipients if address['type'] == 'User']
                roles = [address['name'] for address in recipients if address['type'] == 'Role']

                db_users = list(db.pdb_user.find({
                    '$or': [
                        {'email': {'$in': users}},
                        {'roles': {'$in': roles}}
                    ]
                }))
                db_emails = [d['email'] for d in db_users]
                receiver = static_addresses + db_emails

                subject = subject % subject_params_values
                body = body % body_params_values

                msg = Message(subject=subject, recipients=receiver)
                msg.html = body

                with current_app.app_context():
                    current_app.mail.send(msg)

                def asynchronous(f):
                    def wrapper(*args, **kwargs):
                        thr = Thread(target=f, args=args, kwargs=kwargs)
                        thr.start()

                    return wrapper

                @copy_current_request_context
                @asynchronous
                def send_async_email(app, message):
                    with app.app_context():
                        app.mail.send(message)

                send_async_email(current_app, msg)


def exec_rules(inp: list):
    for _id in inp:
        count = db.rule.count({'_id': ObjectId(_id)})
        if count == 0: 
            return 'failed'

    return ''


def get_rule_group(_id):
    return db.rule_group.find_one({'_id': ObjectId(_id)})


def get_all_rule_group():
    return list(db.rule_group.find())


def rule_group_facet_search(condition):
    if '_id' in condition:
        _id = ObjectId(condition['_id'])
        condition['_id'] = _id

    return list(db.rule_group.find(condition))


def add_rule_group(inp):
    check_exist = db.rule_group.find_one(inp)

    if check_exist is not None:
        return 'Rule group already exists'
    resp = db.rule_group.insert_one(inp)

    return resp


def update_rule_group(inp):
    _id = ObjectId(inp.pop('_id'))
    resp = db.rule_group.update_one({'_id': _id}, {'$set': inp})

    return resp


def delete_rule_group(_id):
    resp = db.rule_group.delete_one({'_id': ObjectId(_id)})
    return resp


def convert_rule_filter(fil: dict, inserted_fields: list, updated_fields: list, deleted_fields: list, record_flag=None):
    if len(fil) == 0:
        return {}

    mongodb_filter = []
    for f in fil:
        if isinstance(f, dict) and 'or' in f and isinstance(f['or'], list):
            list_conditions = [convert_to_dict(condition, inserted_fields, updated_fields, deleted_fields, record_flag) for condition in f['or']]
            mongodb_filter.append({'$or': list_conditions})
        else:
            mongodb_filter.append(convert_to_dict(f, inserted_fields, updated_fields, deleted_fields, record_flag))
    return {'$and': mongodb_filter}


def convert_to_dict(condition, inserted_fields, updated_fields, deleted_fields, record_flag):
    compare_type = condition['compare_type']

    if compare_type in ['Absolute', 'Other Field']:
        field_name = condition.get('field_id', '')
        operator = condition.get('operator', '')
        value = condition.get('value', '')

        if operator == 'Equal to':
            if compare_type == 'Absolute':
                cond_to_dict = {'$expr': {'$eq': ['$' + field_name, value]}}
            elif compare_type == 'Other Field':
                cond_to_dict = {'$expr': {'$eq': ['$' + field_name, '$' + value]}}

        elif operator == 'Not equal to':
            if compare_type == 'Absolute':
                cond_to_dict = {'$expr': {'$ne': ['$' + field_name, value]}}
            elif compare_type == 'Other Field':
                cond_to_dict = {'$expr': {'$ne': ['$' + field_name, '$' + value]}}

        # elif operator == 'Start with':
        #     cond_to_dict = {field_name: {'$regex': '^%s' % value}}
        #     if compare_type == 'Absolute':
        #         cond_to_dict = {'$expr': {'$eq': ['$' + field_name, value]}}
        #     elif compare_type == 'Other Field':
        #         cond_to_dict = {'$expr': {'$eq': ['$' + field_name, '$' + value]}}
        #
        # elif operator == 'Not start with':
        #     cond_to_dict = {field_name: {'$not': {'$regex': '^%s' % value}}}
        #     if compare_type == 'Absolute':
        #         cond_to_dict = {'$expr': {'$eq': ['$' + field_name, value]}}
        #     elif compare_type == 'Other Field':
        #         cond_to_dict = {'$expr': {'$eq': ['$' + field_name, '$' + value]}}
        #
        # elif operator == 'Contains':
        #     cond_to_dict = {field_name: {'$regex': '%s' % value}}
        #     if compare_type == 'Absolute':
        #         cond_to_dict = {'$expr': {'$eq': ['$' + field_name, value]}}
        #     elif compare_type == 'Other Field':
        #         cond_to_dict = {'$expr': {'$eq': ['$' + field_name, '$' + value]}}
        #
        # elif operator == 'Not contains':
        #     cond_to_dict = {field_name: {'$not': {'$regex': '%s' % value}}}
        #     if compare_type == 'Absolute':
        #         cond_to_dict = {'$expr': {'$eq': ['$' + field_name, value]}}
        #     elif compare_type == 'Other Field':
        #         cond_to_dict = {'$expr': {'$eq': ['$' + field_name, '$' + value]}}

        elif operator == 'Greater than':
            if compare_type == 'Absolute':
                cond_to_dict = {'$expr': {'$gt': ['$' + field_name, value]}}
            elif compare_type == 'Other Field':
                cond_to_dict = {'$expr': {'$gt': ['$' + field_name, '$' + value]}}

        elif operator == 'Smaller than':
            if compare_type == 'Absolute':
                cond_to_dict = {'$expr': {'$lt': ['$' + field_name, value]}}
            elif compare_type == 'Other Field':
                cond_to_dict = {'$expr': {'$lt': ['$' + field_name, '$' + value]}}

        elif operator == 'Greater than or equal to':
            if compare_type == 'Absolute':
                cond_to_dict = {'$expr': {'$gte': ['$' + field_name, value]}}
            elif compare_type == 'Other Field':
                cond_to_dict = {'$expr': {'$gte': ['$' + field_name, '$' + value]}}

        elif operator == 'Less than or equal to':
            if compare_type == 'Absolute':
                cond_to_dict = {'$expr': {'$lte': ['$' + field_name, value]}}
            elif compare_type == 'Other Field':
                cond_to_dict = {'$expr': {'$lte': ['$' + field_name, '$' + value]}}

        # Check exception: row_id, type is string but must be checked as int
        if field_name == 'row_id':
            op = list(cond_to_dict[field_name].keys())[0]
            cond_to_dict = {'$expr': {op: [{'$toInt': '$row_id'}, value]}}

    elif compare_type == 'Keyword':
        field_name = condition.get('field_name', '')
        value = condition.get('value', '')

        if value == 'Has no value':
            cond_to_dict = {
                '$or': [
                    {field_name: ''},
                    {field_name: None}
                ]
            }
        elif value == 'Inserted':
            cond_to_dict = {
                '$expr': {
                    '$in': [field_name, inserted_fields]
                }
            }
        elif value == 'Updated':
            cond_to_dict = {
                '$expr': {
                    '$in': [field_name, updated_fields]
                }
            }
        elif value == 'Deleted':
            cond_to_dict = {
                '$expr': {
                    '$in': [field_name, deleted_fields]
                }
            }

    elif compare_type == 'Record Keyword':
        value = condition.get('value', '')
        if value == 'Created':
            cond_to_dict = {
                '$expr': {
                    '$eq': ['created', record_flag]
                }
            }
        elif value == 'Updated':
            cond_to_dict = {
                '$expr': {
                    '$eq': ['updated', record_flag]
                }
            }
        elif value == 'Deleted':
            cond_to_dict = {
                '$expr': {
                    '$eq': ['deleted', record_flag]
                }
            }
    else:
        cond_to_dict = {}

    return cond_to_dict
