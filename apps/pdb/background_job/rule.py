from flask import Blueprint, request, jsonify
from flask_cors import CORS

from .db_rule import *
from .compiler.parser import validate_function
from apps.pdb.decorators import admin_required

rule_api = Blueprint('rule_api', 'rule_api', url_prefix='/api/pdb/rule')
CORS(rule_api)


class Rule():
    def __init__(self, project_id, form_name, field_name, operator, value, record_group):
        self.project_id = project_id 
        self.form_name = form_name
        self.field_name = field_name
        self.operator = operator
        self.value = value
        self.record_group = record_group

    def exec(self):
        pass 


@rule_api.route('/get/<_id>', methods=['GET'])
@admin_required
def api_get_rule(_id):
    rule = get_rule(_id)
    return rule


@rule_api.route('/get-all', methods=['GET'])
@admin_required
def api_get_all():
    groups = get_all()
    return jsonify(groups)


@rule_api.route('/facet-search', methods=['POST'])
@admin_required
def api_facet_search():
    try:
        inp = request.get_json()
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    rules = facet_search(inp)
    return jsonify(rules)


@rule_api.route('/add', methods=['POST'])
@admin_required
def api_add_rule():
    try:
        inp = request.get_json()
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    # missing = [field for field in ['target_table', 'trigger_table', 'condition', 'target_field', 'value'] if field not in inp]
    
    # if len(missing) != 0:
    #     return jsonify({'msg': 'Some fields are missing: %s' % ', '.join(missing)}), 400 

    resp = add_rule(inp)
    if isinstance(resp, str):
        return jsonify({'msg': resp}) , 400
        
    return jsonify({'_id': resp.inserted_id})


@rule_api.route('/add-many', methods=['POST'])
@admin_required
def api_add_many():
    inp = request.get_json()
    
    for d in inp:
        missing = []
        for field in ['target_table', 'trigger_table', 'condition', 'target_field', 'value']:
            if field not in d:
                missing.append(field)
        if len(missing) != 0:
            return jsonify({'msg': 'Some fields are missing: %s' % ', '.join(missing)}), 400 
    
    resp = add_many(inp)
    return jsonify({'_ids': resp.inserted_ids})


@rule_api.route('/update', methods=['PUT'])
@admin_required
def api_update_rule():
    inp = request.get_json()
    if '_id' not in inp: 
        return jsonify({'msg': '_id is missing'}), 400 

    update_rule(inp)
    return jsonify({'msg': 'success'})


@rule_api.route('/delete/<_id>', methods=['DELETE'])
@admin_required
def api_delete_rule(_id):
    delete_rule(_id)
    return jsonify({'msg': 'success'})


@rule_api.route('/exec-rule/<_id>')
@admin_required
def api_exec_rule(_id):
    resp = exec_rule(_id)
    return jsonify({'msg': 'success'})


@rule_api.route('/exec-rules', methods=['POST'])
@admin_required
def api_exec_rules():
    inp = request.get_json()
    resp = exec_rules(inp)
    return jsonify({'msg': 'success'})


@rule_api.route('/get-group/<_id>', methods=['GET'])
@admin_required
def api_get_rule_group(_id):
    rule_group = get_rule_group(_id)
    return rule_group


@rule_api.route('/get-all-group', methods=['GET'])
@admin_required
def api_get_all_rule_group():
    groups = get_all_rule_group()
    return jsonify(groups)


@rule_api.route('/group-facet-search', methods=['POST'])
@admin_required
def api_rg_facet_search():
    try:
        inp = request.get_json()
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    rules = rule_group_facet_search(inp)
    return jsonify(rules)


@rule_api.route('/add-group', methods=['POST'])
@admin_required
def api_add_rule_group():
    try:
        inp = request.get_json()
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    resp = add_rule_group(inp)
    if isinstance(resp, str):
        return jsonify({'msg': resp}), 400

    return jsonify({'_id': resp.inserted_id})


@rule_api.route('/update-group', methods=['PUT'])
@admin_required
def api_update_rule_group():
    inp = request.get_json()
    if '_id' not in inp:
        return jsonify({'msg': '_id is missing'}), 400

    update_rule_group(inp)
    return jsonify({'msg': 'success'})


@rule_api.route('/delete-group/<_id>', methods=['DELETE'])
@admin_required
def api_delete_rule_group(_id):
    delete_rule_group(_id)
    return jsonify({'msg': 'success'})


@rule_api.route('/function/validate', methods=['POST'])
@admin_required
def api_validate_function():
    inp = request.get_json()
    table_id = inp['table_id']
    function = inp['function']
    res = validate_function(table_id, function)
    return {}
