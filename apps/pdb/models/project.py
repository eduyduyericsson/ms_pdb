from flask import Blueprint, request, jsonify, Response
from flask_cors import CORS
from flask_jwt_extended import jwt_required, get_jwt_identity

from bson.objectid import ObjectId
import pandas as pd
import json

from .db_interaction.project import *
from apps.pdb.decorators import admin_required

project_api = Blueprint('project_api', 'project_api', url_prefix='/api/pdb/project')
CORS(project_api)

@project_api.route('/get/<id>')
@jwt_required
def api_get_project(id):
    current_user = get_jwt_identity()
    user_id = current_user['_id']
    project = get_project(id, user_id)
    if project == {} or project is None: 
        return jsonify({'msg': 'Project not found or you do not have access permission on this project'}), 400
    return jsonify(project)

@project_api.route('/get-all')
@jwt_required
def api_get_all():
    current_user = get_jwt_identity()
    user_id = current_user['_id']
    return jsonify(get_all_project(user_id))

@project_api.route('/add', methods=['POST'])
@admin_required
def api_add_project():
    inp = request.get_json()
    project_id = add_project(inp)
    return jsonify({'_id': project_id})

# @project_api.route('/add-form-by-xlsx', methods=['GET', 'PUT'])
# def api_add_form_by_xlsx():
#     file = request.files['file']
#     js = json.load(request.files['json'])
#     project_id = js['project_id']
#     form_name = js['form_name']
#     df = pd.read_excel(file)
#     for col in df.columns:
#         df[col].fillna('', inplace=True)
#     inp = df.values.tolist()

#     resp, err_msgs = add_form_by_xlsx(project_id, form_name, inp)
          
#     if len(err_msgs) == 0:
#         return jsonify({'msg': 'success'})
#     else:
#         print(err_msgs)
#         return jsonify({'msg': '\n'.join(err_msgs)}), 400

@project_api.route('/update', methods=['PUT'])
@admin_required
def api_update_project():
    try:
        inp = request.get_json()
        _id = inp.pop('_id')
        resp = update_project(_id, inp)
        return jsonify({'msg': 'success'})
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

@project_api.route('/delete/<id>', methods=['DELETE'])
@admin_required
def api_delete_project(id):
    # Need to be modified 
    try:
        project_resp, data_resp = delete_project(id)
        return jsonify({'project_deleted': project_resp.deleted_count, 'data_deleted': data_resp.deleted_count})
    except Exception as e:
        return jsonify({'msg': str(e)}), 400