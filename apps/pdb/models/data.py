from flask import Blueprint, request, jsonify, send_file, Response, current_app
from flask_cors import CORS
from flask_jwt_extended import jwt_required, get_jwt_identity

from bson.objectid import ObjectId
import json, time

from .db_interaction.data import *
from apps.pdb.decorators import admin_required
from apps.utils import expect

data_api = Blueprint('data_api', 'data_api', url_prefix='/api/pdb/data')
CORS(data_api)


@data_api.route('/get', methods=['POST'])
@jwt_required
def api_get_data():
    try:
        inp = request.get_json()
        table_id = inp['table_id']
        record_id = inp['_id']
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    current_user = get_jwt_identity()
    user_id = current_user['_id']

    data = get_data(table_id, user_id, record_id)

    if data is None:
        data == {}
    return jsonify(data)


@data_api.route('/get-all-from-table', methods=['POST'])
@jwt_required
def api_get_all_data():
    try:
        inp = request.get_json()
        table_id = inp['table_id']
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    current_user = get_jwt_identity()
    user_id = current_user['_id']
    data = get_all_data(table_id, user_id)
    return jsonify(data)


@data_api.route('/facet-search', methods=['POST'])
@jwt_required
def api_search_data_faceted():
    inp = request.get_json()
    if 'table_id' not in inp or 'condition' not in inp:
        return jsonify({'msg': 'table_id or inp is missing'}), 400
    table_id = inp['table_id']
    condition = inp['condition']

    current_user = get_jwt_identity()
    user_id = current_user['_id']
    out = facet_search(table_id, user_id, condition)
    return jsonify(out)


@data_api.route('/count-all', methods=['POST'])
@jwt_required
def api_count_all():
    try:
        inp = request.get_json()
        table_id = inp['table_id']
        filter = [] if 'filter' not in inp else inp['filter']

        current_user = get_jwt_identity()
        user_id = current_user['_id']

        num = count_all(user_id, table_id, filter)
        return jsonify({'num': num})
    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@data_api.route('/get-form-data', methods=['POST'])
@jwt_required
def api_get_form_data():
    try:
        inp = request.get_json()
        form_id = inp['form_id']
        page = inp['page']
        item_per_page = inp['item_per_page']
        filter = [] if 'filter' not in inp else inp['filter']
        
        current_user = get_jwt_identity()
        user_id = current_user['_id']
        
        data, rg_permission, field_permission = get_form_data(user_id, form_id, filter, page, item_per_page)
        # print(rg_permission)
        # print(field_permission)
        return jsonify({'data': data, 'rg_permission': rg_permission, 'field_permission': field_permission})
    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@data_api.route('/add', methods=['POST'])
@jwt_required
def api_add_data():
    try:
        inp = request.get_json()
        if 'table_id' not in inp or 'data' not in inp:
            return jsonify({'msg': 'table id or data is missing'}), 400

        table_id = inp['table_id']
        data = inp['data']

        current_user = get_jwt_identity()
        user_id = current_user['_id']
        user_name = current_user['username']

        resp = add_data(user_id, table_id, data)
        if isinstance(resp, list):
            return jsonify({'msg': '\n\n'.join(resp)}), 400

        return jsonify({'_id': resp.inserted_id})
    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@data_api.route('/bulk-upload-by-xlsx', methods=['POST'])
@jwt_required
def api_bulk_upload_by_xlsx():
    try:
        file = request.files['file']
        js = json.load(request.files['json'])
        table_id = js['table_id']
        current_user = get_jwt_identity()

        user_id = current_user['_id'] 

        resp, upload_log = bulk_upload_by_xlsx(user_id, file, table_id)

        return jsonify({'upload_log': upload_log})

    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@data_api.route('/bulk-update-by-xlsx', methods=['POST'])
@jwt_required
def api_bulk_update_by_xlsx():
    try:
        file = request.files['file']
        js = json.load(request.files['json'])
        skip_blank = js['skip_blank']
        table_id = js['table_id']
        current_user = get_jwt_identity()

        user_id = current_user['_id']

        resp, upload_log = bulk_update_by_xlsx(user_id, file, table_id, skip_blank)

        return jsonify({'upload_log': upload_log})

    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@data_api.route('/update', methods=['PUT'])
@jwt_required
def api_update_data():
    inp = request.get_json()
    if 'table_id' not in inp or '_id' not in inp or 'data' not in inp:
        return jsonify({'msg': 'table id or _id or data is missing'}), 400
    
    table_id = inp['table_id']
    _id = inp['_id']
    data = inp['data']

    current_user = get_jwt_identity()
    user_id = current_user['_id']

    resp = update_data(user_id, table_id, _id, data)
    if isinstance(resp, list):
        return jsonify({'msg': '\n\n'.join(resp)}), 400 
    return jsonify({'msg': 'success'})


@data_api.route('/update-many', methods=['PUT'])
@jwt_required
def api_update_many_data():
    try:
        inp = request.get_json()
        table_id = inp['table_id']
        data = inp['data']

        current_user = get_jwt_identity()
        user_id = current_user['_id']

        update_many(user_id, table_id, data)
        return jsonify({'msg': 'success'})
    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@data_api.route('/upload-file', methods=['POST'])
@jwt_required
def api_upload_file():
    """
    Create new record, add file to GridFS, file metadata to record
    """
    file = request.files['file']
    
    file_id, file_name = upload_file(file)

    return jsonify({'file_id': file_id, 'file_name': file_name})


@data_api.route('/delete-file/<id>', methods=['DELETE'])
@jwt_required
def api_delete_file(id):
    delete_file(id)
    return jsonify({'msg': 'success'})


@data_api.route('/download-file/<file_id>', methods=['GET'])
@jwt_required
def api_download_file(file_id):
    file = download_file(file_id)
    headers = {'Content-Type': 'application/octet-stream', 'Content-Disposition': 'attachment; %s' % file.__dict__['_file']['filename'], 'Access-Control-Expose-Headers': 'Content-Disposition'}
    return Response(file, headers=headers, mimetype=file.content_type, direct_passthrough=True)


@data_api.route('/delete', methods=['POST'])
@jwt_required
def api_delete_data():
    # need to check delete permission (not have yet)
    inp = request.get_json()
    if 'table_id' not in inp or '_id' not in inp:
        return jsonify({'msg': 'table_id or _id is missing'}), 400

    table_id = inp['table_id']
    _id = inp['_id']
    current_user = get_jwt_identity()
    user_id = current_user['_id']

    resp = delete_data(table_id, _id)
    return jsonify({'deleted': resp.deleted_count})


@data_api.route('/delete-many', methods=['POST'])
@jwt_required
def api_delete_many_data():
    # need to check delete permission (not have yet)
    inp = request.get_json()
    if 'table_id' not in inp or '_ids' not in inp:
        pass
    table_id = inp['table_id']
    _ids = inp['_ids']

    current_user = get_jwt_identity()
    user_id = current_user['_id']

    resp = delete_many_data(table_id, user_id, '_id', _ids)
    if isinstance(resp, str):
        return jsonify({'msg': resp}), 400

    return jsonify({'deleted': resp.deleted_count})


@data_api.route('/bulk-delete-by-xlsx', methods=['POST'])
@jwt_required
def api_bulk_delete_by_xlsx():
    try:
        file = request.files['file']
        js = json.load(request.files['json'])
        table_id = js['table_id']

        current_user = get_jwt_identity()
        user_id = current_user['_id']

        upload_log = bulk_delete_by_xlsx(table_id, user_id, file)
        
        return jsonify({'upload_log': upload_log})
    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@data_api.route('/export', methods=['POST'])
@jwt_required
def api_export():
    try:
        inp = request.get_json()
        form_id = inp['form_id']
        current_user = get_jwt_identity()
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    user_id = current_user['_id']

    data = export_data(user_id, form_id)

    return current_app.excel.make_response_from_array(data, 'xlsx', file_name='data')


@data_api.route('/download-many', methods=['POST'])
@jwt_required
def api_download_many():
    try:
        inp = request.get_json()
        current_user = get_jwt_identity()
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    if 'form_id' not in inp or '_ids' not in inp:
        return jsonify({'msg': 'form_id or _ids is missing'}), 400

    form_id = inp['form_id']
    _ids = inp['_ids']
    user_id = current_user['_id']

    data = download_many(user_id, form_id, _ids)
    return current_app.excel.make_response_from_array(data, 'xlsx', file_name='data')


@data_api.route('/download-import-template', methods=['POST'])
@jwt_required
def api_download_import_template():
    try:
        inp = request.get_json()
        form_id = inp['form_id']
        current_user = get_jwt_identity()
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    user_id = current_user['_id']
    columns = download_import_template(form_id, user_id)
    return current_app.excel.make_response_from_array(columns, 'xlsx', file_name='data')


@data_api.route('/download-import-template-new', methods=['POST'])
@jwt_required
def api_download_import_template_new():
    inp = request.get_json()
    template_id = inp['_id']
    type = inp['type']
    # current_user = get_jwt_identity()
    # user_id = current_user['_id']
    template_name, columns = download_import_template_new(template_id, type)
    return current_app.excel.make_response_from_array(columns, 'xlsx', file_name=template_name)


@data_api.route('/get-import-template/<template_id>')
@jwt_required
def api_get_import_template(template_id):
    """return template info base on user permissions
    """
    current_user = get_jwt_identity()
    user_id = current_user['_id']
    return jsonify(get_import_template_new(user_id, template_id)) 


@data_api.route('/get-all-import-template')
@jwt_required
def api_get_all_import_template():
    """return all templates info base on user permissions
    """
    current_user = get_jwt_identity()
    user_id = current_user['_id']
    return jsonify(get_all_import_template(user_id))  


@data_api.route('/search-import-template', methods=['POST'])
@jwt_required
def api_search_import_template():
    inp = request.get_json()
    current_user = get_jwt_identity()
    user_id = current_user['_id']
    data = search_import_template(user_id, inp)
    return jsonify(data) 


@data_api.route('/create-import-template', methods=['POST'])
@admin_required
def api_create_import_template():
    try:
        inp = request.get_json()
        table_id = inp['table_id']
        role_id = inp['role_id']
        name = inp['name']
        fields = inp['fields']

        expect(name, str, name)
        expect(table_id, str, table_id)
        expect(role_id, str, table_id)
        expect(fields, list, fields)
    
        resp = create_import_template(name, table_id, role_id, fields)

        return jsonify({'inserted_id': resp.inserted_id})
    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@data_api.route('/update-import-template', methods=['POST'])
@admin_required
def api_update_import_template():
    inp = request.get_json()
    _id = inp.pop('_id') 
    resp = update_import_template(_id, inp)
    return jsonify({'modified': resp.modified_count})


@data_api.route('/delete-import-template/<_id>', methods=['DELETE'])
@admin_required
def api_delete_import_template(_id):
    resp = delete_import_template(_id)
    return jsonify({'deleted': resp.deleted_count})


@data_api.route('/download-delete-template', methods=['POST'])
@jwt_required
def api_download_delete_template():
    inp = request.get_json()
    table_id = inp['table_id']
    columns = download_delete_template(table_id)
    return current_app.excel.make_response_from_array(columns, 'xlsx', file_name='delete_template')


@data_api.route('/get-log/<log_id>', methods=['GET'])
@jwt_required
def api_get_log(log_id):
    current_user = get_jwt_identity()
    user_id = current_user['_id']
    log = get_log(user_id, log_id)
    return jsonify(log)


@data_api.route('/get-all-log', methods=['GET'])
@jwt_required
def api_get_all_log():
    current_user = get_jwt_identity()
    user_id = current_user['_id']
    log = get_all_log(user_id)
    return jsonify(log)