from flask import Blueprint, request, jsonify
from flask_cors import CORS
from flask_jwt_extended import jwt_required, get_jwt_identity

from .db_interaction.form import get_form, get_all, facet_search, add_form, update_form, delete_form

from apps.pdb.decorators import admin_required

form_api = Blueprint('form_api', 'form_api', url_prefix='/api/pdb/form')
CORS(form_api)

@form_api.route('/get/<_id>', methods=['GET'])
@jwt_required
def api_get_form(_id):
    current_user = get_jwt_identity()
    user_id = current_user['_id']
    form = get_form(_id, user_id)
    return form

@form_api.route('/get-all', methods=['GET'])
@jwt_required
def api_get_all():
    current_user = get_jwt_identity()
    user_id = current_user['_id']
    forms = get_all(user_id)
    return jsonify(forms)

@form_api.route('/facet-search', methods=['POST'])
@jwt_required
def api_facet_search():
    current_user = get_jwt_identity()
    user_id = current_user['_id']
    condition = request.get_json()
    forms = facet_search(condition, user_id)
    return jsonify(forms)

@form_api.route('/add', methods=['POST'])
@admin_required
def api_add_form():
    inp = request.get_json()
    if 'name' not in inp or 'form_type' not in inp or 'project_id' not in inp: 
        return jsonify({'msg': 'Form name, form type or project_id is missing'}), 400 

    resp = add_form(inp)
    if isinstance(resp, dict):
        return jsonify({'msg': resp['err']}), 400

    return jsonify({'_id': resp.inserted_id})

@form_api.route('/update', methods=['PUT'])
@admin_required
def api_update_form():
    inp = request.get_json()
    if '_id' not in inp: 
        return jsonify({'msg': '_id is missing'}), 400

    resp = update_form(inp)
    return jsonify({'modified': resp.modified_count})

@form_api.route('/delete/<_id>', methods=['DELETE'])
@admin_required
def api_delete_form(_id):
    resp = delete_form(_id)
    return jsonify({'deleted': resp.deleted_count})