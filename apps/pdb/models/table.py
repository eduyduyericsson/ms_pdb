from flask import Blueprint, request, jsonify, Response, current_app
from flask_cors import CORS
from flask_jwt_extended import jwt_required, get_jwt_identity

import pandas as pd
import json

from .db_interaction.table import *
from apps.pdb.decorators import admin_required
from apps.utils import expect

table_api = Blueprint('table_api', 'table_api', url_prefix='/api/pdb/table')
CORS(table_api)


@table_api.route('/get/<_id>')
@jwt_required
def api_get_table(_id):
    table = get_table(_id)
    if table == {} or table is None: 
        return jsonify({'msg': 'Not found'}), 400
    return jsonify(table)


@table_api.route('/get-all')
@jwt_required
def api_get_all():
    return jsonify(get_all_table())


@table_api.route('/add', methods=['POST'])
@admin_required
def api_add_table():
    try:
        inp = request.get_json()

        expect(inp['name'], str, 'name')
        expect(inp['metadata'], dict, 'metadata')

        resp = add_table(inp)
        if isinstance(resp, str):
            return jsonify({'msg': resp}), 400    
        return jsonify({'_id': resp.inserted_id})

    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@table_api.route('/download-import-template')
@admin_required
def api_download_import_template():
    fields = [['Field Name', 'Field Type: Text, Number, Date, Datetime, Dropdown, Checkbox, File, Relation', 'Dropdown Values: |-|', 'Is Unique: x']]
    return current_app.excel.make_response_from_array(fields, 'xlsx', file_name='import_table_template')


@table_api.route('/update', methods=['PUT'])
@admin_required
def api_update_table():
    try:
        inp = request.get_json()
        _id = inp.pop('_id')
        resp = update_table(_id, inp)
        return jsonify({'modified': resp.modified_count})
    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@table_api.route('/delete/<_id>', methods=['DELETE'])
@admin_required
def api_delete_project(_id):
    try:
        metadata_resp, fp_resp, form_resp, table_resp, rule_resp = delete_table(_id)
        if form_resp != None:
            form_deleted = form_resp.deleted_count
        else:
            form_deleted = 0

        if 'errmsg' in table_resp:
            table_deleted = 0
        else:
            table_deleted = 1

        if fp_resp != None:
            fp_deleted = fp_resp.deleted_count
        else:
            fp_deleted = 0

        if rule_resp != None:
            rule_deleted = rule_resp.deleted_count
        else:
            rule_deleted = 0

        return jsonify({'metadata_deleted': metadata_resp.deleted_count, 'form_deleted': form_deleted, 'table_deleted': table_deleted, 'fp_deleted': fp_deleted, 'rule_deleted': rule_deleted})
    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@table_api.route('/add-field', methods=['POST'])
@admin_required
def api_add_field():
    try:
        inp = request.get_json()
        _id = inp['_id']
        field_name = inp['field_name']
        field_info = inp['field_info']
    except Exception as e:
        return jsonify({'msg': str(e)}), 400 

    resp = add_field(_id, field_name, field_info)
    if isinstance(resp, dict):
        return jsonify({'msg': resp['err']}), 400 

    return jsonify({'inserted_id': resp.inserted_id})


@table_api.route('/add-many-fields-by-xlsx', methods=['POST'])
@admin_required
def api_add_many_fields_by_xlsx():
    try:
        file = request.files['file']
        js = json.load(request.files['json'])
        table_id = js['table_id']
        df = pd.read_excel(file)
    except Exception as e:
        return jsonify({'msg': str(e)}), 400  

    # if 'field_name' not in df.columns or 'field_type' not in df.columns or 'values' not in df.columns or
    # 'is_unique' not in df.columns:
    #     return jsonify({'msg': 'Invalid template'}), 400

    processed_col_name = [col.strip() for col in df.columns]
    rename = dict(zip(df.columns, processed_col_name))
    df.rename(columns=rename, inplace=True)

    for col in df.columns:
        df[col].fillna('', inplace=True)
    fields_info = df.values.tolist()
    
    resp, err_msgs = add_many_fields_by_xlsx(table_id, fields_info)
          
    if len(err_msgs) == 0:
        return jsonify({'inserted_count': len(resp.inserted_ids)})
    else:
        # print(err_msgs)
        return jsonify({'msg': '\n'.join(err_msgs)}), 400


@table_api.route('/update-field', methods=['PUT'])
@admin_required
def api_update_field():
    try:
        inp = request.get_json()
        _id = inp['_id']
        field_info = inp['field_info']    
        expect(field_info, dict, field_info)
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    resp = update_field(_id, field_info)

    if isinstance(resp, dict):
        return jsonify({'msg': resp['err']}), 400

    return jsonify({'modified': resp.modified_count})


@table_api.route('/update-many-fields', methods=['PUT'])
@admin_required
def api_update_many_fields():
    inp = request.get_json()
    resp = update_many_fields(inp)
    return jsonify({'modified': resp.modified_count})


@table_api.route('/delete-field/<field_id>', methods=['DELETE'])
@admin_required
def api_delete_field(field_id):
    try:
        resp = delete_field(field_id)
        return jsonify({'deleted_field': resp.deleted_count})
    except Exception as e:
        return jsonify({'msg': str(e)}), 400


@table_api.route('/delete-many-fields', methods=['POST'])
@admin_required
def api_delete_many_fields():
    try:
        inp = request.get_json()
        _id = inp['_id']
        fields = inp['fields']
    except Exception as e:
        return jsonify({'msg': str(e)}), 400  

    metadata_resp, data_resp = delete_many_fields(_id, fields)

    if isinstance(metadata_resp, dict):
        return jsonify({'msg': metadata_resp['err']}), 400 

    return jsonify({'metadata_modified': metadata_resp.modified_count, 'data_modified': data_resp.modified_count}) 


@table_api.route('/delete-many-fields-by-xlsx', methods=['POST'])
@admin_required
def api_delete_many_fields_by_xlsx():
    try:
        file = request.files['file']
        js = json.load(request.files['json'])
        table_id = js['table_id']
        df = pd.read_excel(file)
    except Exception as e:
        return jsonify({'msg': str(e)}), 400  

    fields = df[df.columns[0]].tolist()
    metadata_resp, data_resp = delete_many_fields(table_id, fields)

    if isinstance(metadata_resp, dict):
        return jsonify({'msg': metadata_resp['err']}), 400 

    return jsonify({'metadata_modified': metadata_resp.modified_count, 'data_modified': data_resp.modified_count}) 


@table_api.route('/get-detail-tables', methods=['POST'])
@admin_required
def api_get_detail_tables():
    master_table_id = request.get_json()['master_table_id']
    detail_tables = get_detail_tables_list(master_table_id)
    return jsonify(detail_tables)


@table_api.route('/get-related-tables', methods=['POST'])
@admin_required
def api_get_related_tables():
    try:
        table_id = request.get_json()['table_id']
        related_tables = get_related_tables(table_id)
        return jsonify(related_tables)
    except Exception as e:
        return jsonify({'msg': str(e)}), 400