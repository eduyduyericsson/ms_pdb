from flask import current_app

from apps.db import db 
from .data_filter import convert_filter
from .table import get_table

from pymongo import UpdateOne, ReturnDocument
from pymongo.errors import BulkWriteError, OperationFailure
from pymongo.results import DeleteResult
from bson.objectid import ObjectId
from datetime import datetime
import pandas as pd
import numpy as np
import gridfs


def get_data(table_id, user_id, record_id):
    table_id = ObjectId(table_id)
    user_id = ObjectId(user_id)
    table_name = db.table_metadata.find_one({'_id': table_id})['name']

    rg_permission = aggregate_rg_permission(user_id)
    field_permission = aggregate_field_permission(table_id, user_id)

    showed_record_groups = list(rg_permission.keys())
    showed_fields = list(field_permission.keys())
    showed_fields = {k: 1 for k in showed_fields + ['record_group']}
    return db[table_name].find_one({'_id': ObjectId(record_id), 'record_group': {'$in': showed_record_groups}}, showed_fields)


def get_all_data(table_id, user_id):
    table_id = ObjectId(table_id)
    user_id = ObjectId(user_id)
    table_name = db.table_metadata.find_one({'_id': table_id})['name']

    rg_permission = aggregate_rg_permission(user_id)
    field_permission = aggregate_field_permission(table_id, user_id)

    showed_record_groups = list(rg_permission.keys())
    showed_fields = list(field_permission.keys())
    showed_fields = {k: 1 for k in showed_fields + ['record_group']}

    return list(db[table_name].find({'record_group': {'$in': showed_record_groups}}, showed_fields))


def facet_search(table_id, user_id, condition):
    table_id = ObjectId(table_id)
    table = db.table_metadata.find_one({'_id': table_id})
    table_name = table['name']

    rg_permission = aggregate_rg_permission(user_id)
    field_permission = aggregate_field_permission(table_id, user_id)
    showed_record_groups = list(rg_permission.keys())
    showed_fields = list(field_permission.keys())
    showed_fields = {k: 1 for k in showed_fields + ['record_group']}

    mongodb_filter = convert_filter(condition)
    mongodb_filter['$and'].append({'record_group': {'$in': showed_record_groups}})

    return list(db[table_name].find(mongodb_filter, showed_fields))


def count_all(user_id, table_id, filter):
    table_id = ObjectId(table_id)
    table = db.table_metadata.find_one({'_id': table_id})
    table_name = table['name']

    filter = convert_filter(filter)
    rg_permission = aggregate_rg_permission(user_id)
    showed_record_groups = list(rg_permission.keys())

    num = db[table_name].count_documents({**filter, 'record_group': {'$in': showed_record_groups}})
    return num


def aggregate_field_permission(table_id, user_id):
    """
    Return fields that user can select
    """
    field_pipeline = [
        {
            '$match': {
                'table_id': ObjectId(table_id)
            }
        }, {
            '$lookup': {
                'from': 'role', 
                'localField': 'role_id', 
                'foreignField': '_id', 
                'as': 'role'
            }
        }, {
            '$lookup': {
                'from': 'pdb_user', 
                'localField': 'role.name', 
                'foreignField': 'roles', 
                'as': 'user'
            }
        }, {
            '$match': {
                'user._id': ObjectId(user_id),
                'field_permission.S': True
            }
        }
    ]

    f_permission = list(db.field_permission.aggregate(field_pipeline))

    field_permission = {}
    for f in f_permission:
        field_name = f['field_id']
        if field_name not in field_permission:
            field_permission[field_name] = f['field_permission']
        else:
            perm = f['field_permission']
            # if perm['S'] == True:
            #     field_permission[field_name]['S'] = True                
            if perm['U'] == True:    
                field_permission[field_name]['U'] = True 

    return field_permission


def aggregate_rg_permission(user_id):
    """Return record_groups that user can select
    """
    pipeline = [
        {
            '$match': {
                '_id': ObjectId(user_id)
            }
        }, {
            '$lookup': {
                'from': 'role', 
                'localField': 'roles', 
                'foreignField': 'name', 
                'as': 'role'
            }
        }, {
            '$lookup': {
                'from': 'rg_permission', 
                'localField': 'role._id', 
                'foreignField': 'role_id', 
                'as': 'rg_permission'
            }
        }, {
            '$unwind': {
                'path': '$rg_permission', 
                'preserveNullAndEmptyArrays': True
            }
        }, {
            '$project': {
                '_id': '$rg_permission.record_group_id', 
                'perm': {
                    'S': '$rg_permission.S', 
                    'U': '$rg_permission.U', 
                    'I': '$rg_permission.I', 
                    'D': '$rg_permission.D'
                }
            }
        }, {
            '$lookup': {
                'from': 'record_group', 
                'localField': '_id', 
                'foreignField': '_id', 
                'as': 'record_group'
            }
        }, {
            '$unwind': {
                'path': '$record_group', 
                'preserveNullAndEmptyArrays': True
            }
        }, {
            '$project': {
                '_id': 0, 
                'name': '$record_group.name', 
                'perm': '$perm'
            }
        }, {
            '$match': {
                'perm.S': True
            }
        }
    ]

    record_group_permission = list(db.pdb_user.aggregate(pipeline))
    
    rg_permission = {}
    for rgp in record_group_permission:
        name = rgp['name']
        if name not in rg_permission:
            rg_permission[name] = rgp['perm']
        else:
            # s_perm = rgp['perm']['S']
            u_perm = rgp['perm']['U']
            i_perm = rgp['perm']['I']
            d_perm = rgp['perm']['D']
            # if s_perm == True:
            #     rg_permission[name]['S'] = True
            if u_perm == True:
                rg_permission[name]['U'] = True
            if i_perm == True:
                rg_permission[name]['I'] = True
            if d_perm == True:
                rg_permission[name]['D'] = True 
    
    return rg_permission


def get_multi_record_view_form_data(user_id, form_id, page=None, item_per_page=None, filter={}):
    user_id = ObjectId(user_id)
    roles_name = db.pdb_user.find_one({'_id': user_id})['roles']
    roles = db.role.find({'name': {'$in': roles_name}})
    roles_id = [r['_id'] for r in roles]

    table_id = ObjectId(db.form.find_one({'_id': ObjectId(form_id)})['table_id'])
    table_name = db.table_metadata.find_one({'_id': table_id})['name']
    
    field_permission = aggregate_field_permission(table_id, user_id)
    rg_permission = aggregate_rg_permission(user_id)

    form_fields = list(db.form.find_one({'_id': ObjectId(form_id)})['metadata'].keys())

    showed_fields = list(set(field_permission.keys()).intersection(set(form_fields)))   # --> find out the list of fields that can be viewed by user
    showed_fields = dict(zip(showed_fields, [1]*len(showed_fields)))                    # --> create dict from the above list
    showed_fields = {'$project': {'record_group': 1, **showed_fields}}                           

    showed_record_groups = list(rg_permission.keys())

    data_pipeline = [
        {
            '$match': {
                'record_group': {'$in': showed_record_groups},
            }
        }, 
        showed_fields
    ]

    if filter != {}:
        data_pipeline.append({'$match': filter})

    if page is not None and item_per_page is not None:
        skip_stage = {'$skip': page * item_per_page}
        limit_stage = {'$limit': item_per_page}

        data_pipeline.append(skip_stage)
        data_pipeline.append(limit_stage)

    return_data = list(db[table_name].aggregate(data_pipeline))

    return return_data, rg_permission, field_permission


def get_master_detail_view_data(user_id, form_id, page=None, item_per_page=None, filter={}):
    user_id = ObjectId(user_id)
    roles_name = db.pdb_user.find_one({'_id': user_id})['roles']
    roles = db.role.find({'name': {'$in': roles_name}})
    roles_id = [r['_id'] for r in roles]

    form = db.form.find_one({'_id': ObjectId(form_id)})

    master_table_id = ObjectId(form['master_table_id'])
    master_table_name = db.table_metadata.find_one({'_id': master_table_id})['name']
    detail_table_id = ObjectId(form['detail_table_id'])
    detail_table_name = db.table_metadata.find_one({'_id': detail_table_id})['name']

    master_fp = aggregate_field_permission(master_table_id, user_id)
    master_form_fields = form['master_table_metadata'].keys()

    master_showed_fields = list(set(master_fp.keys()).intersection(set(master_form_fields)))
    master_showed_fields = dict(zip(master_showed_fields, [1]*len(master_showed_fields)))
    master_showed_fields = {'record_group': 1, **master_showed_fields}

    detail_fp = aggregate_field_permission(detail_table_id, user_id)
    detail_form_fields = form['detail_table_metadata'].keys()

    detail_showed_fields = list(set(detail_fp.keys()).intersection(set(detail_form_fields)))
    detail_showed_fields += ['record_group', '_id']
    detail_showed_fields = {'#detail_table_data#.%s' % k: 1 for k in detail_showed_fields}

    showed_fields = {'$project': {**master_showed_fields, **detail_showed_fields}}

    rg_permission = aggregate_rg_permission(user_id)

    showed_record_groups = list(rg_permission.keys())

    relation = db.table_relationship.find_one({'master_table': master_table_id, 'detail_table': detail_table_id})
    master_field = relation['master_field']
    detail_field = relation['detail_field']

    data_pipeline = [
        {
            '$match': {
                'record_group': {'$in': showed_record_groups},
            }
        }, {
            '$lookup': {
                'from': detail_table_name, 
                'localField': master_field, 
                'foreignField': detail_field, 
                'as': '#detail_table_data#'
            }
        }, 
        showed_fields
    ]

    if filter != {}:
        data_pipeline.append({'$match': filter})

    if page is not None and item_per_page is not None:
        skip_stage = {'$skip': page * item_per_page}
        limit_stage = {'$limit': item_per_page}

        data_pipeline.append(skip_stage)
        data_pipeline.append(limit_stage)

    return_data = list(db[master_table_name].aggregate(data_pipeline))
    return return_data, rg_permission, {'master': master_fp, 'detail': detail_fp}


def get_form_data(user_id, form_id, filter, page=0, item_per_page=10):
    form = db.form.find_one({'_id': ObjectId(form_id)})
    form_type = form['form_type']

    filter = convert_filter(filter)

    if form_type == 'Multi Record View':
        # return get_multi_record_view_form_data(user_id, form_id, 0, 10)
        return get_multi_record_view_form_data(user_id, form_id, filter=filter, page=page, item_per_page=item_per_page)
    elif form_type == 'Master Detail View':
        return get_master_detail_view_data(user_id, form_id, filter=filter, page=page, item_per_page=item_per_page)


def get_next_sequence(table_id: ObjectId):
    """
    Get auto increment ID for tables which use default primary key
    """
    resp = db.sequence.find_one_and_update({'table_id': table_id}, {'$inc': {'seq': 1}}, upsert=True, return_document=ReturnDocument.AFTER)
    return resp['seq']


# def insert_one(table, data: dict):
#     table_name = table['name']
#     table_metadata = table['metadata']
#
#     resp = insert_one(table, {**row_id, **data, 'created_at': created_at, 'created_by': created_by})
#
#     # send signal: adding data
#     trigger_fields = [f for f in data if f in table_metadata]
#
#     data_changed_signal = current_app.data_changed
#     data_changed_signal.send(current_app._get_current_object(), msg='insert_one', inserted_id=resp.inserted_id, table_name=table_name, trigger_fields=trigger_fields)
#     #############################
#
#     return resp


def add_data(user_id, table_id, data: dict):
    user_id = ObjectId(user_id)
    table_id = ObjectId(table_id)

    table = db.table_metadata.find_one({'_id': table_id})
    table_name = table['name']
    # table_metadata = table['metadata']
    primary_key = table['primary_key']
    use_default_pk = table['use_default_primary_key']

    row_id = {primary_key: get_next_sequence(table_id)} if use_default_pk else {}
    created_at = datetime.now()
    created_by = db.pdb_user.find_one({'_id': user_id})['username']

    if 'record_group' not in data:
        data['record_group'] = 'default'

    # resp = insert_one(table, {**row_id, **data, 'created_at': created_at, 'created_by': created_by})
    resp = db[table_name].insert_one({**row_id, **data, 'created_at': created_at, 'created_by': created_by})

    # send signal: adding data 
    # trigger_fields = list(data.keys()) + ['row_id'] if use_default_pk else list(data.keys())
    #
    # data_changed_signal = current_app.data_changed
    # data_changed_signal.send(current_app._get_current_object(), msg='insert_one', inserted_id=resp.inserted_id, table_name=table_name, trigger_fields=trigger_fields)
    #############################

    return resp


def validate_data(df: pd.DataFrame, table_name, table_metadata, primary_key, upload_type, field_permission, rg_permission=None, use_default_primary_key=False):
    """
    Validate data before add to DB
    """
    err_msgs = []
    name_id_mapping = {table_metadata[k]['field_name']: k for k in table_metadata}

    # Check column ########################

    # trim columns name
    processed_col_name = [col.strip() for col in df.columns]
    rename = dict(zip(df.columns, processed_col_name))
    df.rename(columns=rename, inplace=True)

    # rename to field ID
    df.rename(columns=name_id_mapping, inplace=True)

    if upload_type == 'insert' and primary_key not in df.columns and use_default_primary_key == False:
        raise AssertionError('Primary key (%s) is missing' % table_metadata[primary_key]['field_name'])

    if upload_type == 'insert' and primary_key in df.columns and use_default_primary_key == True:
        raise Exception('row_id is auto increment')    

    if upload_type == 'update' and primary_key not in df.columns:
        raise AssertionError('Primary key (%s) is missing' % table_metadata[primary_key]['field_name'])

    for col in df.columns:
        if col not in field_permission:
            err_msgs.append('Do not have permission to upload column %s' % table_metadata[col]['field_name'])
    
    if len(err_msgs) != 0:
        return None, err_msgs
    ###########################################################

    # Process data #########################
    if primary_key in df.columns:
        primary_key_data_type = table_metadata[primary_key]['type']
        # df[primary_key] = df[primary_key].fillna('').astype(str).str.strip()
        # primary_value = df[primary_key]
        primary_value = df[primary_key].fillna('').astype(str).str.strip()

        # Check blank keys in excel file 
        err_ids = df.index[primary_value == ''].map(lambda id: id+2).tolist()
        if len(err_ids) != 0:
            msg = 'Primary key \'%s\' at the following rows are blank: %s' % (table_metadata[primary_key]['field_name'], str(err_ids)[1:-1])
            err_msgs.append(msg)

        if primary_key_data_type == 'Text':
            df[primary_key] = df[primary_key].fillna('').astype(str).str.strip()
        elif primary_key_data_type in ['Number', 'Auto Increment']:
            df[primary_key] = df[primary_key].apply(pd.to_numeric, errors='coerce').fillna(-1)

        # Check duplicated keys in excel file 
        duplicated_keys = primary_value[primary_value.duplicated()].values.tolist()
        if len(duplicated_keys) != 0:
            err_msgs.append('Column \'%s\', the following keys are duplicated: %s' % (table_metadata[primary_key]['field_name'], ', '.join(duplicated_keys)))

        if upload_type == 'insert':
            # Check existed keys in db 
            db_duplicated_keys = [el[primary_key] for el in list(db[table_name].find({primary_key: {'$in': primary_value.values.tolist()}}, {primary_key: 1}))]
            if len(db_duplicated_keys) != 0:
                err_msgs.append('The following keys already exist in DB: %s' % ', '.join(db_duplicated_keys))

        elif upload_type == 'update': 
            db_data = list(db[table_name].find({primary_key: {'$in': df[primary_key].tolist()}}, {primary_key: 1, 'record_group': 1}))
            # Check primary key not in db 
            if len(db_data) < len(df[primary_key].unique()):
                not_exist_keys = list(set(df[primary_key]) - set([d[primary_key] for d in db_data]))
                err_ids = df.index[df[primary_key].isin(not_exist_keys)].map(lambda id: id+2).to_list()
                msg = 'Column \'%s\', values at the following rows does not exist in database: %s' % (table_metadata[primary_key]['field_name'], str(err_ids)[1:-1])
                err_msgs.append(msg)
            
            # Check record that user do not have permission to update 
            can_not_update_key = [d[primary_key] for d in db_data if d['record_group'] not in rg_permission]
            err_ids = df.index[df[primary_key].isin(can_not_update_key)].map(lambda id: id+2).to_list()

            if len(err_ids) != 0:
                msg = 'Column \'%s\', user do not have permission to update record at the following rows: %s' % (table_metadata[primary_key]['field_name'], str(err_ids)[1:-1])
                err_msgs.append(msg)

    # Validate data 
    for col in df.columns:
        if col == primary_key: 
            continue

        col_type = table_metadata[col]['type']
        is_unique = table_metadata[col].get('is_unique', False)

        if col_type == 'Text':
            df[col] = df[col].fillna('').astype(str).str.strip().replace({'': None})
            # df[col] = df[col].replace({'': None})

        elif col_type == 'Dropdown':
            col_values = table_metadata[col]['values']
            df[col] = df[col].fillna('').astype(str).str.strip()
            err_ids = df.index[(df[col].isin(col_values) == False) & (df[col] != '')].map(lambda id: id+2).tolist()
        
            if len(err_ids) != 0:
                msg = 'Column \'%s\', values at the following rows are not in dropdown list: %s' % (table_metadata[col]['field_name'], str(err_ids)[1:-1])
                err_msgs.append(msg)

        elif col_type == 'Checkbox':
            df[col] = (df[col].isna() == False)

        elif col_type == 'Number':            
            err_ids = df.index[df[col].fillna(0).apply(pd.to_numeric, errors='coerce').isna() == True].map(lambda id: id+2).tolist()
            if len(err_ids) != 0:
                msg = 'Column \'%s\', values at the following rows are not numerical: %s' % (table_metadata[col]['field_name'], str(err_ids)[1:-1])
                err_msgs.append(msg)
                continue
            
            df[col] = df[col].apply(pd.to_numeric, errors='coerce').replace({np.nan: None})

        elif col_type == 'Date': 
            # try to convert to datetime 
            tmp_data = df[col].fillna('').apply(pd.to_datetime, errors='coerce')
            # find out rows not contain datetime data 
            # year == 1970: pandas auto convert int to datetime with year = 1970
            not_datetime_ids = df.index[(tmp_data.isna() == True) | (tmp_data.dt.year == 1970)]
            # extract data from df 
            tmp_col = df[col][not_datetime_ids]
            # find out error ids 
            err_ids = tmp_col.index[tmp_col.fillna('').astype(str).str.strip() != ''].map(lambda id: id+2).tolist()
            if len(err_ids) != 0:
                msg = 'Column \'%s\', values at the following rows are not date type: %s' % (table_metadata[col]['field_name'], str(err_ids)[1:-1])
                err_msgs.append(msg)
                continue

            df[col] = df[col].fillna('').apply(pd.to_datetime, errors='coerce').dt.date.fillna('').astype('str')

        elif col_type == 'Datetime':
            # try to convert to datetime 
            tmp_data = df[col].fillna('').apply(pd.to_datetime, errors='coerce')
            # find out rows not contain datetime data 
            not_datetime_ids = df.index[(tmp_data.dt.year == 1970) | (tmp_data.isna() == True)]
            # extract data from df 
            tmp_col = df[col][not_datetime_ids]
            # find out err_ids 
            err_ids = tmp_col.index[tmp_col.fillna('').astype(str).str.strip() != ''].map(lambda id: id+2).tolist()
            if len(err_ids) != 0:
                msg = 'Column \'%s\', values at the following rows are not datetime type: %s' % (table_metadata[col]['field_name'], str(err_ids)[1:-1])
                err_msgs.append(msg)
                continue

            df[col] = df[col].fillna('').apply(pd.to_datetime, errors='coerce').fillna('').astype('str')

        elif col_type == 'Relation':
            # Need to check many relation fields at the same time (not completed yet)
            master_table_id = ObjectId(table_metadata[col]['values']['table_id'])
            master_field_id = table_metadata[col]['values']['field_id']

            master_table = get_table(master_table_id)
            master_table_name = master_table['name']
            master_field_type = master_table['metadata'][master_field_id]['type']

            if master_field_type in ['Number', 'Auto Increment']:
                err_ids = df.index[df[col].fillna(0).apply(pd.to_numeric, errors='coerce').isna() == True].map(lambda id: id + 2).tolist()
                if len(err_ids) != 0:
                    msg = 'Column \'%s\', values at the following rows are not numerical: %s' % (table_metadata[col]['field_name'], str(err_ids)[1:-1])
                    err_msgs.append(msg)
                    continue

                df[col] = df[col].apply(pd.to_numeric, errors='coerce').replace({np.nan: None})

            else:
                df[col] = df[col].fillna('').astype(str).str.strip().replace({'': None})

            column_values = list(df[col].unique())
            if None in column_values:
                column_values.remove(None)

            master_data = [d[master_field_id] for d in list(db[master_table_name].find({master_field_id: {'$in': column_values}}, {master_field_id: 1}))]
            not_exist_data = list(set(column_values) - set(master_data))
            if len(not_exist_data) > 0:
                err_ids = df.index[df[col].isin(not_exist_data)].map(lambda id: id + 2).to_list()
                msg = 'Column \'%s\', values at the following rows do not exist in master table: %s' % (table_metadata[col]['field_name'], str(err_ids)[1:-1])
                err_msgs.append(msg)
                continue

        elif col_type in ['File', 'Table', 'Field Group']:
            # msg = 'Unable to upload one of the following types: File, Table, Field Group'
            # err_msgs.append(msg)
            # Need to check Relation
            # df[col] = None
            df = df.drop([col], axis=1)

        # Check unique:
        if is_unique:
            col_values = df[col]
            duplicated_keys = col_values[(col_values != '') & col_values.notnull() & (col_values.duplicated())].values.tolist()
            if len(duplicated_keys) != 0:
                err_msgs.append('Column \'%s\', the following keys are duplicated: %s' % (table_metadata[col]['field_name'], ', '.join(duplicated_keys)))

        # MongoDB will check the rest

    return df, err_msgs


def bulk_upload_by_xlsx(user_id, file, table_id, **kwargs):
    file_id, file_name = upload_file(file)

    user_id = ObjectId(user_id)
    table_id = ObjectId(table_id)

    user = db.pdb_user.find_one({'_id': user_id})
    username = user['username']
    user_full_name = user['name']

    fields = aggregate_field_permission(table_id, user_id)
    fields = [f for f in fields if fields[f]['U'] == True]

    table = get_table(table_id)
    table_name = table['name']
    table_metadata = table['metadata']
    primary_key = table['primary_key']
    default_pk = table['use_default_primary_key']

    created_at = datetime.now()
    created_by = username
    plus_info = {'record_group': 'default', 'created_at': created_at, 'created_by': created_by}

    df = pd.read_excel(file, dtype=object)
    err_msgs = []

    df, invalid_msgs = validate_data(df, table_name, table_metadata, primary_key, 'insert', fields, use_default_primary_key=default_pk)

    err_msgs = err_msgs + invalid_msgs

    if len(err_msgs) != 0:
        # Save upload log
        upload_log = {
            'username': username,
            'name': user_full_name,
            'table_name': table_name,
            'time': created_at,
            'file': {
                'file_id': file_id,
                'file_name': file_name
            },
            'result': {
                'records_created': 0,
                'records_updated': 0,
                'records_deleted': 0,
                'errors': err_msgs
            }
        }
        db.data_upload_log.insert_one(upload_log)

        return err_msgs, upload_log
        
    inp = list(df.transpose().to_dict().values())
    
    for i in range(len(inp)):
        row_id = {primary_key: get_next_sequence(table_id)} if default_pk == True else {}
        inp[i].update(**row_id, **plus_info)

    try:
        resp = db[table_name].insert_many(inp, ordered=False)

        # Save upload log
        upload_log = {
            'username': username,
            'name': user_full_name,
            'table_name': table_name,
            'time': created_at,
            'file': {
                'file_id': file_id,
                'file_name': file_name
            },
            'result': {
                'records_created': len(resp.inserted_ids),
                'records_updated': 0,
                'records_deleted': 0,
                'errors': []
            }
        }
        db.data_upload_log.insert_one(upload_log)

    except BulkWriteError as bwe:
        write_errors = bwe.details['writeErrors']
        dup_fields = {}
        for e in write_errors:
            if e.get('code', 0) == 11000:
                err_key_value = e['keyValue']
                field = list(err_key_value.keys())[0]
                field_name = table_metadata[field]['field_name']
                value = err_key_value[field]
                if field_name not in dup_fields:
                    dup_fields[field_name] = [value]
                else:
                    dup_fields[field_name].append(value)
        for k, v in dup_fields.items():
            v = [str(elm) for elm in v]
            err_msgs.append('Column \'%s\', the following keys are duplicated: %s' % (k, ', '.join(v)))
        
        # Save upload log
        upload_log = {
            'username': username,
            'name': user_full_name,
            'table_name': table_name,
            'time': created_at,
            'file': {
                'file_id': file_id,
                'file_name': file_name
            },
            'result': {
                'records_created': bwe.details['nInserted'],
                'records_updated': 0,
                'records_deleted': 0,
                'errors': err_msgs
            }
        }
        db.data_upload_log.insert_one(upload_log)

        resp = bwe
        # inserted_ids = [d['_id'] for d in list(db[table_name].find({primary_key: {'$in': df[primary_key].tolist()}}))]
        

    ################ send signal: adding data ################
    # trigger_fields = list(df.columns) + ['row_id'] if default_pk == True else list(df.columns)
    #
    # data_changed_signal = current_app.data_changed
    # data_changed_signal.send(current_app._get_current_object(), msg='insert_many', inserted_ids=inserted_ids, table_name=table_name, trigger_fields=trigger_fields)
    ##########################################################

    return resp, upload_log


def bulk_update_by_xlsx(user_id, file, table_id, skip_blank=False, **kwargs):
    file_id, file_name = upload_file(file)

    user_id = ObjectId(user_id)
    table_id = ObjectId(table_id)

    user = db.pdb_user.find_one({'_id': user_id})
    username = user['username']
    user_full_name = user['name']

    # fields contains all fields that user can update
    fields = aggregate_field_permission(table_id, user_id)
    fields = [f for f in fields if fields[f]['U'] == True]

    # rg_permission contains all record_groups that user can update 
    rg_permission = aggregate_rg_permission(user_id)
    rg_permission = [rgp for rgp in rg_permission if rg_permission[rgp]['U'] == True]

    table = get_table(table_id)
    table_name = table['name']
    table_metadata = table['metadata']
    primary_key = table['primary_key']
    default_pk = table['use_default_primary_key']

    updated_at = datetime.now()
    updated_by = username
    plus_info = {'updated_at': updated_at, 'updated_by': updated_by}

    df = pd.read_excel(file, dtype=object)
    err_msgs = []

    df, invalid_msgs = validate_data(df, table_name, table_metadata, primary_key, 'update', fields + [primary_key], rg_permission, use_default_primary_key=default_pk)

    err_msgs = err_msgs + invalid_msgs

    if len(err_msgs) != 0:
        # Save upload log
        upload_log = {
            'username': username,
            'name': user_full_name,
            'table_name': table_name,
            'time': updated_at,
            'file': {
                'file_id': file_id,
                'file_name': file_name
            },
            'result': {
                'records_created': 0,
                'records_updated': 0,
                'records_deleted': 0,
                'errors': err_msgs
            }
        }
        db.data_upload_log.insert_one(upload_log)

        return err_msgs, upload_log

    primary_value = df[primary_key].tolist()
    old_data = list(db[table_name].find({primary_key: {'$in': primary_value}}))

    inp = list(df.transpose().to_dict().values())

    data_to_update = []

    for i in range(len(inp)):
        # skip blank cells in excel file 
        if skip_blank:
            # Need to check more
            # inp[i] = {k: v for k, v in inp[i].items() if v != ''}
            inp[i] = {k: v for k, v in inp[i].items() if (v != '' and v is not None and v != False)}

        inp[i].update(plus_info)
        key_value = inp[i].pop(primary_key)
        data_to_update.append(UpdateOne({primary_key: key_value}, {'$set': inp[i]}))

    try:
        resp = db[table_name].bulk_write(data_to_update, ordered=False)

        # Save upload log 
        upload_log = {
            'username': username,
            'name': user_full_name,
            'table_name': table_name,
            'time': updated_at,
            'file': {
                'file_id': file_id,
                'file_name': file_name
            },
            'result': {
                'records_created': 0,
                'records_updated': resp.modified_count,
                'records_deleted': 0,
                'errors': []
            }
        }
        db.data_upload_log.insert_one(upload_log)

    except BulkWriteError as bwe:
        write_errors = bwe.details['writeErrors']
        dup_fields = {}
        for e in write_errors:
            if e.get('code', 0) == 11000:
                err_key_value = e['keyValue']
                field = list(err_key_value.keys())[0]
                field_name = table_metadata[field]['field_name']
                value = err_key_value[field]
                if field_name not in dup_fields:
                    dup_fields[field_name] = [value]
                else:
                    dup_fields[field_name].append(value)
        for k, v in dup_fields.items():
            v = [str(elm) for elm in v]
            err_msgs.append('Column \'%s\', the following keys are duplicated: %s' % (k, ', '.join(v)))
        
        # Save upload log 
        upload_log = {
            'username': username,
            'name': user_full_name,
            'table_name': table_name,
            'time': updated_at,
            'file': {
                'file_id': file_id,
                'file_name': file_name
            },
            'result': {
                'records_created': bwe.details['nModified'],
                'records_updated': 0,
                'records_deleted': 0,
                'errors': err_msgs
            }
        }
        db.data_upload_log.insert_one(upload_log)

        # print(dup_fields)
        # primary_value = list(set(primary_value) - set(list(dup_fields.values())))

        resp = bwe

    # Update relation
    updated_data = list(db[table_name].find({primary_key: {'$in': primary_value}, 'updated_at': updated_at}))
    updated_df = pd.DataFrame.from_dict(updated_data)
    old_data = list(filter(lambda x: x[primary_key] in updated_df[primary_key].tolist(), old_data))
    old_data = sorted(old_data, key=lambda x: x[primary_key])
    updated_data = sorted(updated_data, key=lambda x: x[primary_key])
    update_detail_table_relation(table, old_data, updated_data)
    
    # send signal: updating data ######################
    # record_ids = [d['_id'] for d in list(db[table_name].find({primary_key: {'$in': primary_value}}, {'_id': 1}))]
    # data_changed_signal = current_app.data_changed
    # data_changed_signal.send(current_app._get_current_object(), msg='update_many', table_name=table_name, trigger_fields=list(df.columns), modified_ids=record_ids)
    ############################################################

    return resp, upload_log


def update_data(user_id, table_id, _id, data, condition={}):
    _id = ObjectId(_id)
    table_id = ObjectId(table_id)
    user_id = ObjectId(user_id)

    table = db.table_metadata.find_one({'_id': table_id})
    table_name = table['name']
    # table_metadata = table['metadata']
    # PK = table['primary_key']

    # field_permission = aggregate_field_permission(table_id, user_id)
    # rg_permission = aggregate_rg_permission(user_id)

    # df = pd.DataFrame.from_dict([data], dtype=object)
    # df, err_msgs = validate_data(df, table_name, table_metadata, PK, 'update', field_permission, rg_permission)

    # if len(err_msgs) != 0:
    #     return err_msgs

    old_data = db[table_name].find_one({'_id': _id})
    # master_fields = db.table_relationship.find({'master_table': table_id, 'master_fields': {'$in': list(old_data.keys())}})

    updated_at = datetime.now()
    updated_by = db.pdb_user.find_one({'_id': user_id})['username']

    resp = db[table_name].update_one({'_id': _id, **condition}, {'$set': {**data, 'updated_at': updated_at, 'updated_by': updated_by}})

    update_detail_table_relation(table, [old_data], [data])

    # if detail_table_resp.modified_count != 0:
    #     data_changed_signal = current_app.data_changed
    #     data_changed_signal.send(current_app._get_current_object(), msg='update_many', table_name=table_name, trigger_fields=list(update.keys()))

    ############################

    # send signal: updating data 
    # if resp.modified_count != 0:
    #     data_changed_signal = current_app.data_changed
    #     data_changed_signal.send(current_app._get_current_object(), msg='update_one', modified_id=_id, table_name=table_name, trigger_fields=list(data.keys()))
    #############################

    return resp


def update_detail_table_relation(master_table: dict, old_data: list, updated_data: list):
    """
    Update relation field of detail table after master data updated
    """
    # Check relation to update detail data
    pipeline = [
        {
            '$match': {
                'master_table': master_table['_id'],
            }
        }, {
            '$lookup': {
                'from': 'table_metadata',
                'localField': 'detail_table',
                'foreignField': '_id',
                'as': 'detail_table'
            }
        }, {
            '$unwind': {
                'path': '$detail_table',
                'includeArrayIndex': 'string',
                'preserveNullAndEmptyArrays': True
            }
        }, {
            '$group': {
                '_id': '$detail_table.name',
                'pairs': {
                    '$addToSet': {
                        'master_field': '$master_field',
                        'detail_field': '$detail_field'
                    }
                }
            }
        }
    ]

    detail = list(db.table_relationship.aggregate(pipeline))

    for elm in detail:
        for i in range(len(old_data)):
            table_name = elm['_id']
            fil = {pair['detail_field']: old_data[i][pair['master_field']] for pair in elm['pairs'] if pair['master_field'] in old_data[i]}
            update = {pair['detail_field']: updated_data[i][pair['master_field']] for pair in elm['pairs']}
            # print(fil, update)
            detail_table_resp = db[table_name].update_many(fil, {'$set': update})


def update_many(user_id, table_id, data: list):
    table_id = ObjectId(table_id)
    user_id = ObjectId(user_id)

    for d in data:
        update_data(user_id, table_id, d.pop('_id'), d)


def update_many_data_by_condition(table_id, condition: dict, data: dict, **kwargs):
    # print(condition, kwargs)
    if 'table_name' in kwargs:
        table_name = kwargs['table_name']
    else:
        table_name = db.table_metadata.find_one({'_id': ObjectId(table_id)})['name']

    updated_at = datetime.now()

    # record_ids =
    resp = db[table_name].update_many(condition, {'$set': {**data, 'updated_at': updated_at}})

    # send signal: updating data 
    # if resp.modified_count != 0:
    #     data_changed_signal = current_app.data_changed
    #     data_changed_signal.send(current_app._get_current_object(), msg='update_many', table_name=table_name, trigger_fields=list(data.keys()), update_time=updated_at)
    #############################

    return resp 


def upload_file(file):
    fs = gridfs.GridFS(db._get_current_object())

    file_id = fs.put(file, filename=file.filename)
    file_name = file.filename

    return file_id, file_name


def delete_file(file_id):
    file_id = ObjectId(file_id)
    fs = gridfs.GridFS(db._get_current_object())
    fs.delete(file_id)


def delete_many_files(file_ids):
    file_ids = [ObjectId(_id) for _id in file_ids]
    resp1 = db['fs.files'].delete_many({'_id': {'$in': file_ids}})
    resp2 = db['fs.chunks'].delete_many({'files_id': {'$in': file_ids}})
    return resp1, resp2


def download_file(file_id):
    fs = gridfs.GridFS(db._get_current_object())
    file_id = ObjectId(file_id)
    file = fs.get(file_id)
    return file 


def delete_data(table_id, _id):
    table = get_table(table_id)
    table_name = table['name']
    table_metadata = table['metadata']
    file_type_fields = [field for field in table_metadata if table_metadata[field]['type'] == 'File']

    data = db[table_name].find_one({'_id': ObjectId(_id)})
    file_ids = [data[f]['file_id'] for f in table_metadata if (f in file_type_fields and f in data and 'file_id' in data[f])]

    resp = db[table_name].delete_one({'_id': ObjectId(_id)})
    delete_many_files(file_ids)

    return resp 


def delete_many_data(table_id, user_id, field_name, values, use_xlsx=False):
    if field_name == '_id':
        values = [ObjectId(v) for v in values]

    rg_permission = aggregate_rg_permission(user_id)

    table = get_table(table_id)
    table_name = table['name']
    table_metadata = table['metadata']
    primary_key = table['primary_key']
    pk_name = table_metadata[primary_key]['field_name']
    file_type_fields = [field for field in table_metadata if table_metadata[field]['type'] == 'File']

    if use_xlsx:
        if pk_name != field_name:
            resp = 'Use primary key to delete data'
            return resp

        field_name = primary_key

    data = list(db[table_name].find({field_name: {'$in': values}}))
    file_ids = [record[f]['file_id'] for record in data for f in record if (f in file_type_fields and f in record and 'file_id' in record[f])]

    not_delete = [str(d[primary_key]) for d in data if d['record_group'] not in rg_permission or (d['record_group'] in rg_permission and rg_permission[d['record_group']]['D'] != True)]
    if len(not_delete) != 0:
        resp = 'You can not delete the following records: %s' % ', '.join(not_delete)
        return resp

    resp = db[table_name].delete_many({field_name: {'$in': values}})
    delete_many_files(file_ids)

    return resp 


def bulk_delete_by_xlsx(table_id, user_id, file):
    file_id, file_name = upload_file(file)

    user_id = ObjectId(user_id)
    user = db.pdb_user.find_one({'_id': user_id})
    username = user['username']
    user_full_name = user['name']

    table_id = ObjectId(table_id)
    table = db.table_metadata.find_one({'_id': table_id})
    table_name = table['name']

    df = pd.read_excel(file, dtype=object)
    key = df.columns[0].strip() 
    df.rename({df.columns[0]: key})

    if table['use_default_primary_key'] == True:
        values = list(df[key].apply(pd.to_numeric, errors='coerce'))
    else:
        values = list(df[key].astype(str).str.strip().values)

    resp = delete_many_data(table_id, user_id, key, values, use_xlsx=True)

    if isinstance(resp, DeleteResult):
        records_deleted = resp.deleted_count
        errors = ''
    else:
        records_deleted = 0
        errors = [resp]

    upload_log = {
        'username': username,
        'name': user_full_name,
        'table_name': table_name,
        'time': datetime.now(),
        'file': {
            'file_id': file_id,
            'file_name': file_name
        },
        'result': {
            'records_created': 0,
            'records_updated': 0,
            'records_deleted': records_deleted,
            'errors': errors
        }
    }
    db.data_upload_log.insert_one(upload_log)

    return upload_log


def export_data(user_id, form_id, filter={}):
    user_id = ObjectId(user_id)
    user = db.pdb_user.find_one({'_id': user_id})
    user_roles = user.get('roles', [])
    form = db.form.find_one({'_id': ObjectId(form_id)})
    form_type = form['form_type']

    if form_type == 'Multi Record View':
        table_id = ObjectId(form['table_id'])
        table = get_table(table_id)
        table_metadata = table['metadata']
        form_metadata = form['metadata']

        data, rg_permission, field_permission = get_multi_record_view_form_data(user_id, form_id, filter=filter)
        if len(data) == 0:
            return []

        cols = sorted(form_metadata.keys(), key=lambda x: form_metadata[x]['sort_id'])

        for col in cols:
            field_type = table_metadata[col]['type']
            if field_type == 'File':
                for i in range(len(data)):
                    tmp = data[i].copy()
                    tmp[col] = '=HYPERLINK("https://test.iv-tracker.com/download-file/%s", "%s")' % (tmp[col].get('file_id'), tmp[col].get('file_name')) if col in tmp else '' #replace file data by string
                    data[i] = tmp

        df = pd.DataFrame.from_dict(data)
        for c in cols:
            if c not in df.columns:
                df[c] = ''

            # if table_metadata[c]['type'] == 'Checkbox':
            #     df[col] = df[col].replace('FALSE', '')

        if 'admin' in user_roles:
            cols.insert(0, 'record_group')
            
        df = df[cols]
        for col in df.columns:
            df[col].fillna('', inplace=True)
        
        values = df.values.tolist()
        columns = [table_metadata.get(col, {}).get('field_name', col) for col in df.columns.to_list()]
        values.insert(0, columns)
        return values

    elif form_type == 'Master Detail View':
        master_table_metadata = form['master_table_metadata']
        detail_table_metadata = form['detail_table_metadata']

        data, rg_permission, field_permission = get_master_detail_view_data(user_id, form_id, filter=filter)
        if len(data) == 0:
            return []

        master_cols = sorted(master_table_metadata.keys(), key=lambda x: master_table_metadata[x]['sort_id'])
        detail_cols = sorted(detail_table_metadata.keys(), key=lambda x: detail_table_metadata[x]['sort_id'])

        # Convert for data type File
        for i in range(len(data)):
            for col in master_cols:
                if col in data[i] and isinstance(data[i][col], dict) and 'file_id' in data[i][col] and 'file_name' in data[i][col]:
                    tmp = data[i][col]
                    tmp = '=HYPERLINK("http://localhost:5000/api/pdb/data/download-file/%s", "%s")' % (tmp.get('file_id'), tmp.get('file_name')) # replace file data by string
                    data[i][col] = tmp
            
            for col in detail_cols:
                if col in data[i]['#detail_table_data#'] and isinstance(data[i]['#detail_table_data#'][col], dict) and 'file_id' in data[i]['#detail_table_data#'][col] and 'file_name' in data[i]['#detail_table_data#'][col]:
                    tmp = data[i]['#detail_table_data#'][col]
                    tmp = '=HYPERLINK("http://localhost:5000/api/pdb/data/download-file/%s", "%s")' % (tmp.get('file_id'), tmp.get('file_name')) # replace file data by string
                    data[i]['#detail_table_data#'][col] = tmp

        # Check common column names between master and detail table
        common_col_names = set(master_cols).intersection(set(detail_cols))
        new_detail_cols = [name if name not in common_col_names else '#detail.' + name for name in detail_cols]
        rename = {name: name if name not in common_col_names else '#detail.' + name for name in detail_cols}

        cols = master_cols + new_detail_cols

        extracted_data = []
        for i in range(len(data)):
            detail_data = data[i].pop('#detail_table_data#')
            if len(detail_data) == 0:
                detail_data = [{k: '' for k in detail_cols}]

            for j in range(len(detail_data)):
                tmp = data[i].copy()
                tmp.update(detail_data[j])
                extracted_data.append(tmp)

        df = pd.DataFrame.from_dict(extracted_data)

        for c in cols:
            if c not in df.columns:
                df[c] = ''

        if 'admin' in user_roles:
            cols.insert(0, 'record_group')

        df = df[cols]
        for col in df.columns:
            df[col].fillna('', inplace=True)

        rename_master = {k: master_table_metadata[k]['display_name'] for k in master_table_metadata}
        df.rename(columns=rename_master, inplace=True)
        rename_detail = {k: detail_table_metadata[k]['display_name'] for k in detail_table_metadata}
        df.rename(columns=rename_detail, inplace=True)

        values = df.values.tolist()
        columns = df.columns.to_list()
        values.insert(0, columns)

        return values


def download_many(user_id, form_id, _ids):
    _obids = [ObjectId(_id) for _id in _ids]
    fil = {
        '$and':[
            {'_id': {'$in': _obids}}
        ]
    }
    return export_data(user_id, form_id, fil)


def download_import_template(form_id, user_id):
    user_id = ObjectId(user_id)
    form_id = ObjectId(form_id)
    form = db.form.find_one({'_id': form_id})
    form_metadata = form['metadata']
    table_id = form['table_id']
    form_fields = form['metadata'].keys()
    field_permission = aggregate_field_permission(table_id, user_id) 
    can_update_fields = [f for f in field_permission if field_permission[f]['U'] == True]
    showed_fields = list(set(can_update_fields).intersection(set(form_fields)))
    showed_fields = sorted(showed_fields, key=lambda x: form_metadata[x]['sort_id'])
    if 'row_id' in showed_fields:
        showed_fields.remove('row_id')
    return [showed_fields]


def download_import_template_new(template_id, type):
    template_id = ObjectId(template_id)
    template = db.import_template.find_one({'_id': template_id})
    table_id = template['table_id']
    table = get_table(table_id)
    table_metadata = table['metadata']
    name = template['name']
    fields = [table_metadata[f]['field_name'] for f in template['fields']]

    if 'row_id' in fields:
        if type == 'Create':
            fields.remove('row_id')

    return name, [fields]


def get_import_template_new(user_id, template_id):
    user_id = ObjectId(user_id)
    template_id = ObjectId(template_id)
    user_roles = db.pdb_user.find_one({'_id': user_id})['roles']

    if 'admin' in user_roles:
        return db.import_template.find_one({'_id': template_id})
    else: 
        role_ids = [r['_id'] for r in list(db.role.find({'name': {'$in': user_roles}}, {'_id': 1}))]
        template = db.import_template.find_one({'_id': template_id})

        if template['role_id'] not in role_ids:
            return []
        else:
            return template


def get_all_import_template(user_id):
    user_id = ObjectId(user_id)
    user_roles = db.pdb_user.find_one({'_id': user_id})['roles']
    if 'admin' in user_roles:
        return list(db.import_template.find())
    else: 
        role_ids = [r['_id'] for r in list(db.role.find({'name': {'$in': user_roles}}, {'_id': 1}))]
        return list(db.import_template.find({'role_id': {'$in': role_ids}}))


def search_import_template(user_id, filter):
    user_id = ObjectId(user_id)
    user_roles = db.pdb_user.find_one({'_id': user_id})['roles']

    if 'table_id' in filter:
        table_id = ObjectId(filter['table_id'])
        filter['table_id'] = table_id

    if 'role_id' in filter:
        role_id = ObjectId(filter['role_id'])
        filter['role_id'] = role_id

    if 'admin' in user_roles:
        return list(db.import_template.find(filter))
    else: 
        role_ids = [r['_id'] for r in list(db.role.find({'name': {'$in': user_roles}}, {'_id': 1}))]
        return list(db.import_template.find({**filter, 'role_id': {'$in': role_ids}}))


def create_import_template(name, table_id, role_id, fields: list):
    table_id = ObjectId(table_id)
    role_id = ObjectId(role_id)
    # table_metadata = db.table_metadata.find_one({'_id': table_id})['metadata']
    table_metadata = get_table(table_id)['metadata']

    not_exist_fields = [f for f in fields if f not in table_metadata]
    assert (len(not_exist_fields) == 0), 'Table does not have the following fields: %s' % ', '.join(not_exist_fields)

    resp = db.import_template.insert_one({'name': name, 'table_id': table_id, 'role_id': role_id, 'fields': fields})
    return resp 


def update_import_template(_id, data):
    _id = ObjectId(_id)
    
    if 'table_id' in data:
        table_id = ObjectId(data['table_id'])
        data['table_id'] = table_id

    if 'role_id' in data:
        role_id = ObjectId(data['role_id'])
        data['role_id'] = role_id

    return db.import_template.update_one({'_id': _id}, {'$set': data})


def delete_import_template(_id):
    _id = ObjectId(_id)
    return db.import_template.delete_one({'_id': _id})


def download_delete_template(table_id):
    table_id = ObjectId(table_id)
    table = get_table(table_id)
    pk = table['primary_key']
    pk_name = table['metadata'][pk]['field_name']
    return [[pk_name]]


def get_log(user_id, log_id):
    user_id = ObjectId(user_id)
    user = db.pdb_user.find_one({'_id': user_id})
    roles = user['roles']
    username = user['username']

    log_id = ObjectId(log_id)

    if 'admin' in roles:
        log = db.data_upload_log.find_one({'_id': log_id})
    else:
        log = db.data_upload_log.find_one({'_id': log_id, 'username': username})

    if log is None:
        return {}
    else:
        return log


def get_all_log(user_id):
    user_id = ObjectId(user_id)
    user = db.pdb_user.find_one({'_id': user_id})
    roles = user['roles']

    if 'admin' in roles:
        return list(db.data_upload_log.find({'$query': {}, '$orderby': {'time': -1}}).limit(1000))
    else:
        username = user['username']
        return list(db.data_upload_log.find({'$query': {'username': username}, '$orderby': {'time': -1}}).limit(100))

