from bson.objectid import ObjectId


def convert_filter(filter):
    if len(filter) == 0:
        return {}

    mongodb_filter = []
    for f in filter:
        if isinstance(f, dict) and 'or' in f and isinstance(f['or'], list):
            list_conditions = [to_dict(condition) for condition in f['or']]
            mongodb_filter.append({'$or': list_conditions})
        else:
            mongodb_filter.append(to_dict(f))
    return {'$and': mongodb_filter}


def to_dict(condition):
    field_name = condition.get('field_id', condition.get('field_name', ''))
    operator = condition['operator']
    value = condition['value']

    if 'field_type' in condition and condition['field_type'] == 'object_id':
        if isinstance(value, list):
            value = [ObjectId(v) for v in value]
        else:
            value = ObjectId(value)

    # '$options': 'i' --> ignore case sensitive

    if operator == 'Equal to':
        # cond_to_dict = {field_name: {'$eq': value}}
        cond_to_dict = {'$expr': {'$eq': ['$' + field_name, value]}}
    
    elif operator == 'Not equal to':
        cond_to_dict = {field_name: {'$ne': value}}
    
    elif operator == 'Start with':
        cond_to_dict = {field_name: {'$regex': '^%s' % value}}
    
    elif operator == 'Not start with':
        cond_to_dict = {field_name: {'$not': {'$regex': '^%s' % value}}}
    
    elif operator == 'Contains':
        # cond_to_dict = {field_name: {'$regex': '%s' % value, '$options': 'i'}}
        # cond_to_dict = {field_name: {'$regex': '%s' % value}}
        cond_to_dict = {
            '$expr': {
                '$regexMatch': {
                    'input': {'$toString': '$' + field_name},
                    'regex': str(value)
                }
            }
        }
    elif operator == 'Not contains':
        cond_to_dict = {field_name: {'$not': {'$regex': '%s' % value}}}
    
    elif operator == 'Greater than':
        cond_to_dict = {field_name: {'$gt': value}}
    
    elif operator == 'Smaller than':
        cond_to_dict = {field_name: {'$lt': value}}
    
    elif operator == 'Greater than or equal to':
        cond_to_dict = {field_name: {'$gte': value}}

    elif operator == 'Less than or equal to':
        cond_to_dict = {field_name: {'$lte': value}}

    elif operator == 'In':
        cond_to_dict = {field_name: {'$in': value}}
    
    return cond_to_dict