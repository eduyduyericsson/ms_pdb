from apps.db import db 
from bson.objectid import ObjectId
from pymongo import ReturnDocument, UpdateOne


def get_table(_id):
    pipeline = [
        {
            '$match': {
                '_id': ObjectId(_id)
            }
        }, {
            '$lookup': {
                'from': 'columns_info',
                'localField': '_id',
                'foreignField': 'table_id',
                'as': 'fields'
            }
        }, {
            '$project': {
                'fields.table_id': 0
            }
        }
    ]
    table = list(db.table_metadata.aggregate(pipeline))[0]
    metadata = {d.pop('field_id'): d for d in table['fields']}
    table.pop('fields')
    reverse_mapping = {info['field_name']: field_id for (field_id, info) in metadata.items()}
    table['metadata'] = metadata
    table['reverse_mapping'] = reverse_mapping
    return table


def get_all_table():
    pipeline = [
        {
            '$lookup': {
                'from': 'columns_info',
                'localField': '_id',
                'foreignField': 'table_id',
                'as': 'fields'
            }
        }, {
            '$project': {
                'fields.table_id': 0
            }
        }
    ]
    tables = list(db.table_metadata.aggregate(pipeline))
    converted_tables = []
    for table in tables:
        t = {
            '_id': table['_id'],
            'name': table['name'],
            'primary_key': table['primary_key'],
            # 'use_default_primary_key': table['use_default_primary_key']
        }
        metadata = {d.pop('field_id'): d for d in table['fields']}
        reverse_mapping = {info['field_name']: field_id for (field_id, info) in metadata.items()}
        t['metadata'] = metadata
        t['reverse_mapping'] = reverse_mapping
        converted_tables.append(t)
    return converted_tables


def get_field_id():
    resp = db.sequence.find_one_and_update({'type': 'field_id'}, {'$inc': {'seq': 1}}, upsert=True, return_document=ReturnDocument.AFTER)
    return str(resp['seq'])


def add_table(inp):
    table_name = inp['name']
    if table_name in db.collection_names():
        return 'Table already exist'

    metadata = inp.pop('metadata')

    # create unique index for PK
    pk = inp['primary_key']
    pk_field_id = get_field_id()
    inp['primary_key'] = pk_field_id
    unique_index_name = db[table_name].create_index(pk_field_id, unique=True, partialFilterExpression={pk_field_id: {'$exists': True, '$type': ['int', 'long', 'double', 'decimal', 'string']}})

    resp = db.table_metadata.insert_one(inp)

    # add field to columns_info
    db.columns_info.insert_one({
        'table_id': resp.inserted_id,
        'field_id': pk_field_id,
        'field_name': pk,
        'type': metadata[pk]['type'],
        'is_unique': True,
        'unique_index_name': unique_index_name,
        'is_primary_key': True,
        'sort_id': 0
    })

    # Add field permissions for admin
    admin_id = ObjectId(db.role.find_one({'name': 'admin'})['_id'])

    field_perm = {
        'table_id': ObjectId(resp.inserted_id),
        'role_id': admin_id,
        'field_id': pk_field_id,
        'field_permission': {
            'S': True,
            'U': True
        }
    }
    if pk == 'row_id':
        field_perm['field_permission']['U'] = False

    db.field_permission.insert_one(field_perm)
    #########################

    return resp


def update_table(_id, props):
    resp = db.table_metadata.update_one({'_id': ObjectId(_id)}, {'$set': props})
    return resp


def delete_table(_id):
    _id = ObjectId(_id)
    table_name = db.table_metadata.find_one({'_id': _id})['name']
    # delete meta_data
    resp1 = db.table_metadata.delete_one({'_id': _id})
    # delete field permissions
    resp2 = db.field_permission.delete_many({'table_id': _id})
    # delete related forms
    resp3 = db.form.delete_many({'table_id': _id})
    db.form.delete_many({'master_table_id': _id})
    db.form.delete_many({'detail_table_id': _id})
    # delete table
    resp4 = db.drop_collection(table_name)
    # delete rules
    resp5 = db.rule.delete_many({'target_table': table_name})
    # delete import templates
    resp6 = db.import_template.delete_many({'table_id': _id})
    # delete sequence_id
    db.sequence.delete_many({'table_id': _id})
    # delete columns info
    db.columns_info.delete_many({'table_id': _id})
    return resp1, resp2, resp3, resp4, resp5


def add_field(_id, field_name, field_info):
    _id = ObjectId(_id)

    table = get_table(_id)

    if table is None or table == {}:
        return {'err': 'Table does not exist'}

    if 'is_unique' in field_info and field_info['is_unique'] == True:
        is_unique = True
    else:
        is_unique = False

    table_name = table['name']
    metadata = table['metadata']
    reverse_mapping = table['reverse_mapping']

    if field_name in reverse_mapping:
        return {'err': 'Field already exists'}

    if field_name == 'row_id':
        return {'err': 'Can not use name \'row_id\', choose an other name'}

    field_id = get_field_id()

    # Create unique index
    if is_unique:
        unique_index_name = db[table_name].create_index(
            field_id,
            unique=True,
            partialFilterExpression={
                field_id: {
                    '$exists': True,
                    '$type': ['int', 'long', 'double', 'decimal', 'string']
                }
            }
        )
        field_info['unique_index_name'] = unique_index_name
    #########################

    # add field to columns_info
    resp = db.columns_info.insert_one({
        'table_id': _id,
        'field_id': field_id,
        'field_name': field_name,
        'is_unique': is_unique,
        **field_info
    })

    # Add field permissions for admin
    admin_id = ObjectId(db.role.find_one({'name': 'admin'})['_id'])
    field_perm = {
        'table_id': _id,
        'role_id': admin_id,
        'field_id': field_id,
        'field_permission': {
            'S': True,
            'U': True
        }
    }
    db.field_permission.insert_one(field_perm)
    #########################

    # Check relation ########
    if field_info['type'] == 'Relation':
        master_table_id = ObjectId(field_info['values']['table_id'])
        master_field = field_info['values']['field_id']
        db.table_relationship.insert_one({
            'master_table': master_table_id,
            'detail_table': _id,
            'master_field': master_field,
            'detail_field': field_id
        })
    #########################

    return resp


def add_many_fields_by_xlsx(_id, fields_info):
    _id = ObjectId(_id)

    table = get_table(_id)

    if table is None or table == {}:
        return {'err': 'Table does not exist'}

    table_name = table['name']
    metadata = table['metadata']
    sort_ids = list(f['sort_id'] for f in metadata.values())
    current_max_sort_id = max(sort_ids) if len(sort_ids) != 0 else 0
    data_types = [d['name'] for d in list(db.data_type.find())]

    err_msgs = []
    unique_fields = []
    columns_info = []

    list_fields = []
    
    for id in range(len(fields_info)):
        field_id = get_field_id()

        # Process field name
        field_name = str(fields_info[id][0]).strip()
        if field_name in metadata or field_name in list_fields:
            msg = 'Line %d: field %s duplicated' % (id + 2, field_name)
            err_msgs.append(msg)
            continue

        if field_name == '':
            msg = 'Line %d: field name must not be blank' % (id + 2)
            err_msgs.append(msg)
            continue

        if field_name == 'row_id':
            msg = 'Can not use name \'row_id\'' % (id + 2)
            err_msgs.append(msg)
            continue

        # Process field type
        field_type = str(fields_info[id][1]).strip()
        if field_type not in data_types:
            msg = 'Line %d: Type \'%s\' is not in data types (%s)' % (id + 2, field_type, str(data_types)[1:-1])
            err_msgs.append(msg)
            continue

        list_fields.append(field_name)

        # Process values for Dropdown
        if field_type == 'Dropdown':
            dropdown_list = [s.strip() for s in str(fields_info[id][2]).split('|-|')]
            dropdown_values = {'values': dropdown_list}
        else:
            dropdown_values = {}

        # Check is_unique
        is_unique = True if str(fields_info[id][3]).strip() != '' else False
        if is_unique:
            unique_fields.append(field_id)

        field_info = {
            'table_id': _id,
            'field_id': field_id,
            'field_name': field_name,
            'type': field_type,
            'is_unique': is_unique,
            'sort_id': current_max_sort_id + id + 1,
            **dropdown_values,
        }
        columns_info.append(field_info)

    if len(err_msgs) == 0:
        # create unique indexes
        for field in columns_info:
            if field['field_id'] in unique_fields:
                unique_index_name = db[table_name].create_index(
                    field['field_id'],
                    unique=True,
                    partialFilterExpression={
                        field['field_id']: {
                            '$exists': True,
                            '$type': ['int', 'long', 'double', 'decimal', 'string']
                        }
                    }
                )
                field['unique_index_name'] = unique_index_name
        ########################

        # Add field permissions for admin
        admin_id = ObjectId(db.role.find_one({'name': 'admin'})['_id'])
        field_perm_list = []
        for field in columns_info:
            field_perm = {
                'table_id': _id,
                'role_id': admin_id,
                'field_id': field['field_id'],
                'field_permission': {
                    'S': True,
                    'U': True
                }
            }
            field_perm_list.append(field_perm)
        db.field_permission.insert_many(field_perm_list)
        #########################

        # add field info
        resp = db.columns_info.insert_many(columns_info)

    else:
        resp = None
    return resp, err_msgs


def update_field(_id, field_info: dict):
    _id = ObjectId(_id)
    field = db.columns_info.find_one({'_id': _id})
    field_id = field['field_id']
    table_id = ObjectId(field['table_id'])
    table = db.table_metadata.find_one({'_id': table_id})
    table_name = table['name']

    old_type = field['type']
    new_type = field_info['type']

    new_name = field_info['field_name']
    if new_name == 'row_id':
        return {'err': 'Can not use name\'row_id\', choose an other name'}

    # Check unique
    if 'is_unique' in field_info and field_info['is_unique'] == True:
        is_unique = True
        pipeline = [
            {
                '$group': {
                    '_id': '$%s' % field_id,
                    'count': {'$sum': 1}
                }
            }
        ]
        count_distinct = list(db[table_name].aggregate(pipeline))
        duplicated = [d['_id'] for d in count_distinct if (d['count'] > 1 and d['_id'] is not None)]
        if len(duplicated) != 0:
            return {'err': 'Data is not unique, please delete duplicated data before updating/deleting'}
    else:
        is_unique = False

    # Create/delete unique index
    if is_unique:
        unique_index_name = db[table_name].create_index(
            field_id,
            unique=True,
            partialFilterExpression={field_id: {'$exists': True, '$type': ['int', 'long', 'double', 'decimal', 'string']}},
        )
        field_info['unique_index_name'] = unique_index_name
    else:
        if 'unique_index_name' in field and field['unique_index_name'] == True:
            unique_index_name = field['unique_index_name']
            db[table_name].drop_index(unique_index_name)
    #########################

    # Check table relation
    if old_type == 'Relation':
        db.table_relationship.delete_many({'detail_table': table_id, 'detail_field': field_id})

    if new_type == 'Relation':
        master_table_id = ObjectId(field_info['values']['table_id'])
        master_field = field_info['values']['field_id']
        db.table_relationship.insert_one({
            'master_table': master_table_id,
            'detail_table': table_id,
            'master_field': master_field,
            'detail_field': field_id
        })
    #########################

    # Save to columns info
    resp = db.columns_info.update_one(
        {'_id': _id},
        {'$set': field_info}
    )

    return resp


def update_many_fields(inp):
    """
    Update sort_id of fields in the same table
    """
    data_to_update = []
    for d in inp:
        data_to_update.append(UpdateOne(
            {'_id': ObjectId(d['_id'])},
            {'$set': {'sort_id': d['sort_id']}}
        ))
    resp = db.columns_info.bulk_write(data_to_update, ordered=False)
    return resp


def delete_field(_id):
    _id = ObjectId(_id)

    field = db.columns_info.find_one({'_id': _id})
    field_id = field['field_id']
    table_id = ObjectId(field['table_id'])
    table = db.table_metadata.find_one({'_id': table_id})
    table_name = table['name']

    # Check which forms using field
    forms = list(db.form.find({
        '$or': [
            {'table_id': _id, 'metadata.%s' % field_id: {'$exists': True}},
            {'master_table_id': _id, 'master_table_metadata.%s' % field_id: {'$exists': True}},
            {'detail_table_id': _id, 'detail_table_metadata.%s' % field_id: {'$exists': True}}
        ]
    }))
    if len(forms) != 0:
        return 'Field is being used in the following forms: %s' % ', '.join(f['name'] for f in forms)

    db[table_name].update_many({}, {'$unset': {field_id: ""}})
    db.field_permission.delete_many({'table_id': _id, 'field_id': field_id})

    # delete field permission
    db.field_permission.delete_many({
        'table_id': table_id,
        'field_id': field_id
    })

    # delete relation
    db.table_relationship.delete_many({
        '$or': [
            {'master_table': _id, 'master_field': field_id},
            {'detail_table': _id, 'detail_field': field_id}
        ]
    })
    ################################

    # delete unique index
    if 'is_unique' in field and field['is_unique'] == True:
        unique_index_name = field['unique_index_name']
        db[table_name].drop_index(unique_index_name)
    ######################

    resp = db.columns_info.delete_one({'_id': _id})
    return resp


def delete_many_fields(_id, fields):
    table_id = ObjectId(_id)

    table = db.table_metadata.find_one({'_id': table_id})

    if table is None or table == {}:
        return {'err': 'Table does not exist'}, None

    table_name = table['name']
    metadata = table['metadata']

    metadata_delete_info = {}
    data_delete_info = {}
    err_fields = []

    for f in fields:
        if f not in metadata:
            err_fields.append(f)
        metadata_delete_info['metadata.' + f] =  ""
        data_delete_info[f] = ""

    if len(err_fields) != 0:
        err_msg = 'The following fields do not exist: %s.' % ', '.join(err_fields)
        return {'err': err_msg}, None

    resp1 = db.table_metadata.update_one({'_id': table_id}, {'$unset': metadata_delete_info})
    resp2 = db[table_name].update_many({}, {'$unset': data_delete_info})
    resp3 = db.form.update_many({'table_id': table_id}, {'$unset': metadata_delete_info})
    resp4 = db.field_permission.delete_many({'table_id': table_id, 'field_name': {'$in': fields}})

    # delete relation
    del_master_resp = db.table_relationship.delete_many({'master_table': _id, 'master_field': {'$in': fields}})
    del_detail_resp = db.table_relationship.delete_many({'detail_table': _id, 'detail_field': {'$in': fields}})
    ################################

    # delete unique indexes
    for f in fields:
        if 'is_unique' in metadata[f] and metadata[f]['is_unique'] == True:
            unique_index_name = metadata[f]['unique_index_name']
            db[table_name].drop_index(unique_index_name)
    ########################

    return resp1, resp2


def get_detail_tables_list(master_table_id):
    master_table_id = ObjectId(master_table_id)

    pipeline = [
        {
            '$match': {
                'master_table': master_table_id
            }
        }, {
            '$group': {
                '_id': '$detail_table'
            }
        }, {
            '$lookup': {
                'from': 'table_metadata',
                'localField': '_id',
                'foreignField': '_id',
                'as': 'string'
            }
        }, {
            '$unwind': {
                'path': '$string',
                'preserveNullAndEmptyArrays': True
            }
        }, {
            '$project': {
                'name': '$string.name'
            }
        }
    ]

    detail_table_list = list(db.table_relationship.aggregate(pipeline))
    return detail_table_list


def get_related_tables(table_id):
    """
    Get all tables link to the table which has _id 'table_id'
    """
    table_id = ObjectId(table_id)

    pipeline1 = [
        {
            '$match': {
                'master_table': table_id
            }
        }, {
            '$group': {
                '_id': '$detail_table'
            }
        }, {
            '$lookup': {
                'from': 'table_metadata',
                'localField': '_id',
                'foreignField': '_id',
                'as': 'string'
            }
        }, {
            '$unwind': {
                'path': '$string',
                'preserveNullAndEmptyArrays': True
            }
        }, {
            '$project': {
                'name': '$string.name'
            }
        }
    ]

    pipeline2 = [
        {
            '$match': {
                'detail_table': table_id
            }
        }, {
            '$group': {
                '_id': '$detail_table'
            }
        }, {
            '$lookup': {
                'from': 'table_metadata',
                'localField': '_id',
                'foreignField': '_id',
                'as': 'string'
            }
        }, {
            '$unwind': {
                'path': '$string',
                'preserveNullAndEmptyArrays': True
            }
        }, {
            '$project': {
                'name': '$string.name'
            }
        }
    ]

    detail_table_list = list(db.table_relationship.aggregate(pipeline1))
    master_table_list = list(db.table_relationship.aggregate(pipeline2))
    return {'detail_tables': detail_table_list, 'master_tables': master_table_list}
