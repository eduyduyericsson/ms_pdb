from apps.db import db 
from bson.objectid import ObjectId

def get_form(_id, user_id):
    user_id = ObjectId(user_id)
    roles_name = db.pdb_user.find_one({'_id': user_id})['roles']
    user_roles = db.role.find({'name': {'$in': roles_name}})
    role_ids = [r['_id'] for r in user_roles]

    if 'admin' in roles_name:
        return db.form.find_one({'_id': ObjectId(_id)})
    else:
        pipeline = [
            {
                '$lookup': {
                    'from': 'form_permission', 
                    'localField': '_id', 
                    'foreignField': 'form_id', 
                    'as': 'p'
                }
            }, {
                '$match': {
                    '_id': ObjectId(_id),
                    'p': {
                        '$elemMatch': {
                            'form_permission': True, 
                            'role_id': {'$in': role_ids}
                        }
                    }
                }
            }, {
                '$project': {
                    'p': 0
                }
            }
        ]
        res = list(db.form.aggregate(pipeline))
        if len(res) != 0:
            return res[0]
        else: 
            return {}    

def get_all(user_id):
    user_id = ObjectId(user_id)
    roles_name = db.pdb_user.find_one({'_id': user_id})['roles']
    user_roles = db.role.find({'name': {'$in': roles_name}})
    role_ids = [r['_id'] for r in user_roles]

    if 'admin' in roles_name:
        return list(db.form.find())
    else:
        pipeline = [
            {
                '$lookup': {
                    'from': 'form_permission', 
                    'localField': '_id', 
                    'foreignField': 'form_id', 
                    'as': 'p'
                }
            }, {
                '$match': {
                    'p': {
                        '$elemMatch': {
                            'form_permission': True, 
                            'role_id': {'$in': role_ids}
                        }
                    }
                }
            }, {
                '$project': {
                    'p': 0
                }
            }
        ]
        return list(db.form.aggregate(pipeline))

def facet_search(condition, user_id):
    user_id = ObjectId(user_id)
    roles_name = db.pdb_user.find_one({'_id': user_id})['roles']
    user_roles = db.role.find({'name': {'$in': roles_name}})
    role_ids = [r['_id'] for r in user_roles]

    if 'table_id' in condition: 
        table_id = ObjectId(condition.pop('table_id'))
        condition['table_id'] = table_id

    if 'master_table_id' in condition:
        master_table_id = ObjectId(condition.pop('master_table_id'))
        condition['master_table_id'] = master_table_id

    if 'detail_table_id' in condition:
        detail_table_id = ObjectId(condition.pop('detail_table_id'))
        condition['detail_table_id'] = detail_table_id

    if 'project_id' in condition: 
        project_id = ObjectId(condition.pop('project_id'))
        condition['project_id'] = project_id
    
    if 'admin' in roles_name:
        return list(db.form.find(condition))
    else:
        pipeline = [
            {
                '$lookup': {
                    'from': 'form_permission', 
                    'localField': '_id', 
                    'foreignField': 'form_id', 
                    'as': 'p'
                }
            }, {
                '$match': {
                    **condition,
                    'p': {
                        '$elemMatch': {
                            'form_permission': True, 
                            'role_id': {'$in': role_ids}
                        }
                    }
                }
            }, {
                '$project': {
                    'p': 0
                }
            }
        ]
        return list(db.form.aggregate(pipeline))

def add_form(inp):
    if 'table_id' in inp:
        table_id = ObjectId(inp.pop('table_id'))
        inp['table_id'] = table_id

    if 'master_table_id' in inp:
        master_table_id = ObjectId(inp.pop('master_table_id'))
        inp['master_table_id'] = master_table_id

    if 'detail_table_id' in inp:
        detail_table_id = ObjectId(inp.pop('detail_table_id'))
        inp['detail_table_id'] = detail_table_id

    project_id = ObjectId(inp.pop('project_id'))
    inp['project_id'] = project_id
    
    resp = db.form.insert_one(inp)
    return resp

def update_form(inp):
    if 'table_id' in inp: 
        table_id = ObjectId(inp.pop('table_id'))
        inp['table_id'] = table_id

    if 'master_table_id' in inp:
        master_table_id = ObjectId(inp.pop('master_table_id'))
        inp['master_table_id'] = master_table_id

    if 'detail_table_id' in inp:
        detail_table_id = ObjectId(inp.pop('detail_table_id'))
        inp['detail_table_id'] = detail_table_id

    if 'project_id' in inp: 
        project_id = ObjectId(inp.pop('project_id'))
        inp['project_id'] = project_id
        
    _id = ObjectId(inp.pop('_id')) 
    resp = db.form.update_one({'_id': _id}, {'$set': inp})
    return resp

def delete_form(_id):
    resp = db.form.delete_one({'_id': ObjectId(_id)})
    return resp 