from apps.db import db
from .data import download_file, export_data, get_all_data, aggregate_field_permission

from bson.objectid import ObjectId
from openpyxl import load_workbook, Workbook
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.utils.dataframe import dataframe_to_rows
import pandas as pd


def get_export(_id):
    return db.export.find_one({'_id': ObjectId(_id)})


def get_all():
    return list(db.export.find())


def facet_search(condition):
    if '_id' in condition:
        _id = ObjectId(condition.pop('_id'))
        condition['_id'] = _id

    if 'table_id' in condition:
        table_id = ObjectId(condition.pop('table_id'))
        condition['table_id'] = table_id

    if 'project_id' in condition:
        project_id = ObjectId(condition.pop('project_id'))
        condition['project_id'] = project_id

    if 'form_id' in condition:
        form_id = ObjectId(condition.pop('form_id'))
        condition['form_id'] = form_id

    return list(db.export.find(condition))


def add_export(inp):
    if 'table_id' in inp:
        table_id = ObjectId(inp.pop('table_id'))
        inp['table_id'] = table_id

    if 'role_id' in inp:
        role_id = ObjectId(inp.pop('role_id'))
        inp['role_id'] = role_id

    resp = db.export.insert_one(inp)
    return resp


def update_export(inp):
    if '_id' in inp:
        _id = ObjectId(inp.pop('_id'))
        inp['_id'] = _id

    if 'table_id' in inp:
        table_id = ObjectId(inp.pop('table_id'))
        inp['table_id'] = table_id

    if 'role_id' in inp:
        role_id = ObjectId(inp.pop('role_id'))
        inp['role_id'] = role_id

    if 'form_id' in inp:
        form_id = ObjectId(inp.pop('form_id'))
        inp['form_id'] = form_id

    _id = ObjectId(inp.pop('_id'))
    resp = db.export.update_one({'_id': _id}, {'$set': inp})
    return resp


def delete_export(_id):
    resp = db.export.delete_one({'_id': ObjectId(_id)})
    return resp


def run_export(_id, user_id, use_template=False):
    export_id = ObjectId(_id)

    export = db.export.find_one({'_id': export_id})
    table_id = export['table_id']
    fields = export['fields']

    field_permission = aggregate_field_permission(table_id, user_id)

    table = db.table_metadata.find_one({'_id': ObjectId(table_id)})
    table_metadata = table['metadata']

    data = get_all_data(table_id, user_id)
    for col in fields:
        field_type = table_metadata[col]['type']
        if field_type == 'File':
            for i in range(len(data)):
                tmp = data[i].copy()
                tmp[col] = '=HYPERLINK("https://test.iv-tracker.com/download-file/%s", "%s")' % (tmp[col].get('file_id'), tmp[col].get('file_name')) if col in tmp else ''  # replace file data by string
                data[i] = tmp

    df = pd.DataFrame.from_dict(data)

    for col in fields:
        if (col not in df.columns) or (col in df.columns and col not in field_permission):
            df[col] = ''

    df = df[fields]
    for col in df.columns:
        df[col].fillna('', inplace=True)

    data = dataframe_to_rows(df, index=False, header=True)

    if use_template:
        template_file_id = export['file_template_id']
        file_name = export['file_template_name']
        extension = file_name.split('.')[-1]
        template = download_file(template_file_id)
        if extension == 'xlsm':
            wb = load_workbook(template, keep_vba=True)
        else:
            wb = load_workbook(template, keep_vba=False)
        sheet_name = wb.sheetnames[0]
        ws = wb[sheet_name]
        wb.remove(ws)
        ws = wb.create_sheet(sheet_name, 0)
        for d in data:
            ws.append(d)
        wb.close()

    else:
        wb = Workbook()
        ws = wb.active
        for d in data:
            ws.append(d)
        wb.close()

        file_name = 'export.xlsx'
        extension = 'xlsx'

    # template = download_file('5f493a556da3eafd44363f00')
    # data = export_data(user_id='5ef45d03c946593c169b038b', form_id='5f324c01fcf43e83092747eb')

    return save_virtual_workbook(wb), file_name, extension
